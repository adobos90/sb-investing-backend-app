package springsbinvesting.domain.celkituzes;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QFeladat is a Querydsl query type for Feladat
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QFeladat extends EntityPathBase<Feladat> {

    private static final long serialVersionUID = -828150815L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QFeladat feladat = new QFeladat("feladat");

    public final StringPath besorolas = createString("besorolas");

    public final StringPath color = createString("color");

    public final StringPath end = createString("end");

    public final QCelkituzes feladatCelkituzes;

    public final StringPath hatarido = createString("hatarido");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Integer> sorszam = createNumber("sorszam", Integer.class);

    public final StringPath start = createString("start");

    public final StringPath title = createString("title");

    public final StringPath type = createString("type");

    public final NumberPath<Integer> utemezett = createNumber("utemezett", Integer.class);

    public QFeladat(String variable) {
        this(Feladat.class, forVariable(variable), INITS);
    }

    public QFeladat(Path<? extends Feladat> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QFeladat(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QFeladat(PathMetadata metadata, PathInits inits) {
        this(Feladat.class, metadata, inits);
    }

    public QFeladat(Class<? extends Feladat> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.feladatCelkituzes = inits.isInitialized("feladatCelkituzes") ? new QCelkituzes(forProperty("feladatCelkituzes"), inits.get("feladatCelkituzes")) : null;
    }

}

