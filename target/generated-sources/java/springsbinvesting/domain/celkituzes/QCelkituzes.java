package springsbinvesting.domain.celkituzes;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QCelkituzes is a Querydsl query type for Celkituzes
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QCelkituzes extends EntityPathBase<Celkituzes> {

    private static final long serialVersionUID = -113295775L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QCelkituzes celkituzes = new QCelkituzes("celkituzes");

    public final SetPath<Akadaly, QAkadaly> akadalyok = this.<Akadaly, QAkadaly>createSet("akadalyok", Akadaly.class, QAkadaly.class, PathInits.DIRECT2);

    public final StringPath celIgeny = createString("celIgeny");

    public final springsbinvesting.domain.QUser celkituzesUser;

    public final StringPath celOka = createString("celOka");

    public final DateTimePath<java.util.Date> datum = createDateTime("datum", java.util.Date.class);

    public final StringPath eleresiSzint = createString("eleresiSzint");

    public final StringPath elerhetoE = createString("elerhetoE");

    public final StringPath elokeszuletElso = createString("elokeszuletElso");

    public final StringPath elokeszuletHarmadik = createString("elokeszuletHarmadik");

    public final StringPath elokeszuletMasodik = createString("elokeszuletMasodik");

    public final StringPath elokeszuletNegyedik = createString("elokeszuletNegyedik");

    public final SetPath<Feladat, QFeladat> feladatok = this.<Feladat, QFeladat>createSet("feladatok", Feladat.class, QFeladat.class, PathInits.DIRECT2);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath lepes1 = createString("lepes1");

    public final StringPath lepes10 = createString("lepes10");

    public final StringPath lepes2 = createString("lepes2");

    public final StringPath lepes3 = createString("lepes3");

    public final StringPath lepes4 = createString("lepes4");

    public final StringPath lepes5 = createString("lepes5");

    public final StringPath lepes6 = createString("lepes6");

    public final StringPath lepes7 = createString("lepes7");

    public final StringPath lepes8 = createString("lepes8");

    public final StringPath lepes9 = createString("lepes9");

    public final StringPath megnevezes = createString("megnevezes");

    public final NumberPath<Double> szuksegesOsszeg = createNumber("szuksegesOsszeg", Double.class);

    public QCelkituzes(String variable) {
        this(Celkituzes.class, forVariable(variable), INITS);
    }

    public QCelkituzes(Path<? extends Celkituzes> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QCelkituzes(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QCelkituzes(PathMetadata metadata, PathInits inits) {
        this(Celkituzes.class, metadata, inits);
    }

    public QCelkituzes(Class<? extends Celkituzes> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.celkituzesUser = inits.isInitialized("celkituzesUser") ? new springsbinvesting.domain.QUser(forProperty("celkituzesUser")) : null;
    }

}

