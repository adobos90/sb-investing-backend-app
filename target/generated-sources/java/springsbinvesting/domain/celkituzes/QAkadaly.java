package springsbinvesting.domain.celkituzes;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QAkadaly is a Querydsl query type for Akadaly
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QAkadaly extends EntityPathBase<Akadaly> {

    private static final long serialVersionUID = -808998913L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QAkadaly akadaly = new QAkadaly("akadaly");

    public final QCelkituzes akadalyCelkituzes;

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath title = createString("title");

    public QAkadaly(String variable) {
        this(Akadaly.class, forVariable(variable), INITS);
    }

    public QAkadaly(Path<? extends Akadaly> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QAkadaly(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QAkadaly(PathMetadata metadata, PathInits inits) {
        this(Akadaly.class, metadata, inits);
    }

    public QAkadaly(Class<? extends Akadaly> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.akadalyCelkituzes = inits.isInitialized("akadalyCelkituzes") ? new QCelkituzes(forProperty("akadalyCelkituzes"), inits.get("akadalyCelkituzes")) : null;
    }

}

