package springsbinvesting.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QUser is a Querydsl query type for User
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QUser extends EntityPathBase<User> {

    private static final long serialVersionUID = -90617192L;

    public static final QUser user = new QUser("user");

    public final DateTimePath<java.util.Date> birthDate = createDateTime("birthDate", java.util.Date.class);

    public final SetPath<springsbinvesting.domain.celkituzes.Celkituzes, springsbinvesting.domain.celkituzes.QCelkituzes> celkituzesk = this.<springsbinvesting.domain.celkituzes.Celkituzes, springsbinvesting.domain.celkituzes.QCelkituzes>createSet("celkituzesk", springsbinvesting.domain.celkituzes.Celkituzes.class, springsbinvesting.domain.celkituzes.QCelkituzes.class, PathInits.DIRECT2);

    public final SetPath<springsbinvesting.domain.penzugy.CelokPenzugy, springsbinvesting.domain.penzugy.QCelokPenzugy> celokpenzugyek = this.<springsbinvesting.domain.penzugy.CelokPenzugy, springsbinvesting.domain.penzugy.QCelokPenzugy>createSet("celokpenzugyek", springsbinvesting.domain.penzugy.CelokPenzugy.class, springsbinvesting.domain.penzugy.QCelokPenzugy.class, PathInits.DIRECT2);

    public final StringPath email = createString("email");

    public final StringPath firstName = createString("firstName");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath lastName = createString("lastName");

    public final StringPath passw = createString("passw");

    public final SetPath<springsbinvesting.domain.penzugy.Penzugy, springsbinvesting.domain.penzugy.QPenzugy> penzugyek = this.<springsbinvesting.domain.penzugy.Penzugy, springsbinvesting.domain.penzugy.QPenzugy>createSet("penzugyek", springsbinvesting.domain.penzugy.Penzugy.class, springsbinvesting.domain.penzugy.QPenzugy.class, PathInits.DIRECT2);

    public final ArrayPath<byte[], Byte> photo = createArray("photo", byte[].class);

    public final SetPath<Reszveny, QReszveny> reszvenyek = this.<Reszveny, QReszveny>createSet("reszvenyek", Reszveny.class, QReszveny.class, PathInits.DIRECT2);

    public final SetPath<springsbinvesting.domain.strategia.Strategia, springsbinvesting.domain.strategia.QStrategia> strategiaUser = this.<springsbinvesting.domain.strategia.Strategia, springsbinvesting.domain.strategia.QStrategia>createSet("strategiaUser", springsbinvesting.domain.strategia.Strategia.class, springsbinvesting.domain.strategia.QStrategia.class, PathInits.DIRECT2);

    public final SetPath<springsbinvesting.domain.strategia.StrategiaReszveny, springsbinvesting.domain.strategia.QStrategiaReszveny> stReszvenyUser = this.<springsbinvesting.domain.strategia.StrategiaReszveny, springsbinvesting.domain.strategia.QStrategiaReszveny>createSet("stReszvenyUser", springsbinvesting.domain.strategia.StrategiaReszveny.class, springsbinvesting.domain.strategia.QStrategiaReszveny.class, PathInits.DIRECT2);

    public QUser(String variable) {
        super(User.class, forVariable(variable));
    }

    public QUser(Path<? extends User> path) {
        super(path.getType(), path.getMetadata());
    }

    public QUser(PathMetadata metadata) {
        super(User.class, metadata);
    }

}

