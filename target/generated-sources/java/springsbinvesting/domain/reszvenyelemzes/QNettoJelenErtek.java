package springsbinvesting.domain.reszvenyelemzes;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QNettoJelenErtek is a Querydsl query type for NettoJelenErtek
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QNettoJelenErtek extends EntityPathBase<NettoJelenErtek> {

    private static final long serialVersionUID = -518205387L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QNettoJelenErtek nettoJelenErtek = new QNettoJelenErtek("nettoJelenErtek");

    public final NumberPath<Integer> alj_kimenet = createNumber("alj_kimenet", Integer.class);

    public final NumberPath<Integer> alj_val = createNumber("alj_val", Integer.class);

    public final NumberPath<Integer> arfolyam = createNumber("arfolyam", Integer.class);

    public final NumberPath<Integer> bef_osszeg = createNumber("bef_osszeg", Integer.class);

    public final NumberPath<Integer> celar_fel_kimenet = createNumber("celar_fel_kimenet", Integer.class);

    public final NumberPath<Integer> celar_fel_val = createNumber("celar_fel_val", Integer.class);

    public final NumberPath<Integer> celar_kimenet = createNumber("celar_kimenet", Integer.class);

    public final NumberPath<Integer> celar_val = createNumber("celar_val", Integer.class);

    public final NumberPath<Integer> csod_kimenet = createNumber("csod_kimenet", Integer.class);

    public final NumberPath<Integer> csod_val = createNumber("csod_val", Integer.class);

    public final NumberPath<Integer> csucs_kimenet = createNumber("csucs_kimenet", Integer.class);

    public final NumberPath<Integer> csucs_val = createNumber("csucs_val", Integer.class);

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final NumberPath<Integer> mely_kimenet = createNumber("mely_kimenet", Integer.class);

    public final NumberPath<Integer> mely_val = createNumber("mely_val", Integer.class);

    public final springsbinvesting.domain.QReszveny nettoReszveny;

    public final StringPath penznem = createString("penznem");

    public QNettoJelenErtek(String variable) {
        this(NettoJelenErtek.class, forVariable(variable), INITS);
    }

    public QNettoJelenErtek(Path<? extends NettoJelenErtek> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QNettoJelenErtek(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QNettoJelenErtek(PathMetadata metadata, PathInits inits) {
        this(NettoJelenErtek.class, metadata, inits);
    }

    public QNettoJelenErtek(Class<? extends NettoJelenErtek> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.nettoReszveny = inits.isInitialized("nettoReszveny") ? new springsbinvesting.domain.QReszveny(forProperty("nettoReszveny"), inits.get("nettoReszveny")) : null;
    }

}

