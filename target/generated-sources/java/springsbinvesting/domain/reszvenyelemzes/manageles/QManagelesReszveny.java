package springsbinvesting.domain.reszvenyelemzes.manageles;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QManagelesReszveny is a Querydsl query type for ManagelesReszveny
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QManagelesReszveny extends EntityPathBase<ManagelesReszveny> {

    private static final long serialVersionUID = -113502470L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QManagelesReszveny managelesReszveny = new QManagelesReszveny("managelesReszveny");

    public final StringPath bef_netto_jelen_magyarazat = createString("bef_netto_jelen_magyarazat");

    public final StringPath bef_toke = createString("bef_toke");

    public final StringPath egyeb_magyarazat = createString("egyeb_magyarazat");

    public final StringPath eszkoz = createString("eszkoz");

    public final StringPath gazdasagi_ingadozas_magyarazat = createString("gazdasagi_ingadozas_magyarazat");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath mikor_vasarolni = createString("mikor_vasarolni");

    public final springsbinvesting.domain.reszvenyelemzes.QManageles reszvenymanageles;

    public final StringPath riziko_faktor = createString("riziko_faktor");

    public final StringPath stop_szint = createString("stop_szint");

    public final StringPath valallati_riziko_magyarazat = createString("valallati_riziko_magyarazat");

    public final StringPath vallalat_hosszu_eladosodas_magyarazat = createString("vallalat_hosszu_eladosodas_magyarazat");

    public final StringPath vallalat_magas_reszveny_magyarazat = createString("vallalat_magas_reszveny_magyarazat");

    public final StringPath vallalat_piaci_mut_magyarazat = createString("vallalat_piaci_mut_magyarazat");

    public final StringPath vallalat_rossz_hat_mut_magyarazat = createString("vallalat_rossz_hat_mut_magyarazat");

    public final StringPath vallalat_rovid_eladosodas_magyarazat = createString("vallalat_rovid_eladosodas_magyarazat");

    public final StringPath vallalati_rossz_penz_mutato_magy = createString("vallalati_rossz_penz_mutato_magy");

    public final StringPath vasarlas = createString("vasarlas");

    public QManagelesReszveny(String variable) {
        this(ManagelesReszveny.class, forVariable(variable), INITS);
    }

    public QManagelesReszveny(Path<? extends ManagelesReszveny> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QManagelesReszveny(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QManagelesReszveny(PathMetadata metadata, PathInits inits) {
        this(ManagelesReszveny.class, metadata, inits);
    }

    public QManagelesReszveny(Class<? extends ManagelesReszveny> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.reszvenymanageles = inits.isInitialized("reszvenymanageles") ? new springsbinvesting.domain.reszvenyelemzes.QManageles(forProperty("reszvenymanageles"), inits.get("reszvenymanageles")) : null;
    }

}

