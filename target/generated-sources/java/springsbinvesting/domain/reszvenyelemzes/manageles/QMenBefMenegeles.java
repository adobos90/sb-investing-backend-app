package springsbinvesting.domain.reszvenyelemzes.manageles;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QMenBefMenegeles is a Querydsl query type for MenBefMenegeles
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QMenBefMenegeles extends EntityPathBase<MenBefMenegeles> {

    private static final long serialVersionUID = 82299441L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QMenBefMenegeles menBefMenegeles = new QMenBefMenegeles("menBefMenegeles");

    public final NumberPath<Integer> arfolyam = createNumber("arfolyam", Integer.class);

    public final springsbinvesting.domain.reszvenyelemzes.QManageles befMenmanageles;

    public final StringPath bek_valtozasok = createString("bek_valtozasok");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath intezkedeseim = createString("intezkedeseim");

    public final StringPath kov_datum = createString("kov_datum");

    public QMenBefMenegeles(String variable) {
        this(MenBefMenegeles.class, forVariable(variable), INITS);
    }

    public QMenBefMenegeles(Path<? extends MenBefMenegeles> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QMenBefMenegeles(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QMenBefMenegeles(PathMetadata metadata, PathInits inits) {
        this(MenBefMenegeles.class, metadata, inits);
    }

    public QMenBefMenegeles(Class<? extends MenBefMenegeles> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.befMenmanageles = inits.isInitialized("befMenmanageles") ? new springsbinvesting.domain.reszvenyelemzes.QManageles(forProperty("befMenmanageles"), inits.get("befMenmanageles")) : null;
    }

}

