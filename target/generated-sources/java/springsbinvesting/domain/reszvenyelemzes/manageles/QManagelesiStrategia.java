package springsbinvesting.domain.reszvenyelemzes.manageles;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QManagelesiStrategia is a Querydsl query type for ManagelesiStrategia
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QManagelesiStrategia extends EntityPathBase<ManagelesiStrategia> {

    private static final long serialVersionUID = -1107018565L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QManagelesiStrategia managelesiStrategia = new QManagelesiStrategia("managelesiStrategia");

    public final StringPath arfolyam_celar = createString("arfolyam_celar");

    public final StringPath arfolyam_emelkedes = createString("arfolyam_emelkedes");

    public final StringPath arfolyam_eses = createString("arfolyam_eses");

    public final StringPath gazd_romlott = createString("gazd_romlott");

    public final StringPath hitel_romlott = createString("hitel_romlott");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath jov_romlott = createString("jov_romlott");

    public final StringPath riziko_faktor_nott = createString("riziko_faktor_nott");

    public final springsbinvesting.domain.reszvenyelemzes.QManageles stratmanageles;

    public QManagelesiStrategia(String variable) {
        this(ManagelesiStrategia.class, forVariable(variable), INITS);
    }

    public QManagelesiStrategia(Path<? extends ManagelesiStrategia> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QManagelesiStrategia(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QManagelesiStrategia(PathMetadata metadata, PathInits inits) {
        this(ManagelesiStrategia.class, metadata, inits);
    }

    public QManagelesiStrategia(Class<? extends ManagelesiStrategia> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.stratmanageles = inits.isInitialized("stratmanageles") ? new springsbinvesting.domain.reszvenyelemzes.QManageles(forProperty("stratmanageles"), inits.get("stratmanageles")) : null;
    }

}

