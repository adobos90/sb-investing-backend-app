package springsbinvesting.domain.reszvenyelemzes.manageles;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QManagelesBefAdatok is a Querydsl query type for ManagelesBefAdatok
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QManagelesBefAdatok extends EntityPathBase<ManagelesBefAdatok> {

    private static final long serialVersionUID = -670353489L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QManagelesBefAdatok managelesBefAdatok = new QManagelesBefAdatok("managelesBefAdatok");

    public final springsbinvesting.domain.reszvenyelemzes.QManageles befAdatokmanageles;

    public final NumberPath<Integer> celar_arfolyam = createNumber("celar_arfolyam", Integer.class);

    public final StringPath celar_penznem = createString("celar_penznem");

    public final NumberPath<Integer> elemzesi_arfolyam = createNumber("elemzesi_arfolyam", Integer.class);

    public final StringPath elemzesi_penznem = createString("elemzesi_penznem");

    public final NumberPath<Integer> eves_megtakaritas = createNumber("eves_megtakaritas", Integer.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath menedzselesi_strat = createString("menedzselesi_strat");

    public final StringPath piaci_kapitalizacio = createString("piaci_kapitalizacio");

    public final StringPath szektor = createString("szektor");

    public QManagelesBefAdatok(String variable) {
        this(ManagelesBefAdatok.class, forVariable(variable), INITS);
    }

    public QManagelesBefAdatok(Path<? extends ManagelesBefAdatok> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QManagelesBefAdatok(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QManagelesBefAdatok(PathMetadata metadata, PathInits inits) {
        this(ManagelesBefAdatok.class, metadata, inits);
    }

    public QManagelesBefAdatok(Class<? extends ManagelesBefAdatok> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.befAdatokmanageles = inits.isInitialized("befAdatokmanageles") ? new springsbinvesting.domain.reszvenyelemzes.QManageles(forProperty("befAdatokmanageles"), inits.get("befAdatokmanageles")) : null;
    }

}

