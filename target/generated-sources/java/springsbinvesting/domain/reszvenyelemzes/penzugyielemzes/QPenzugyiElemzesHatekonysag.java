package springsbinvesting.domain.reszvenyelemzes.penzugyielemzes;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QPenzugyiElemzesHatekonysag is a Querydsl query type for PenzugyiElemzesHatekonysag
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QPenzugyiElemzesHatekonysag extends EntityPathBase<PenzugyiElemzesHatekonysag> {

    private static final long serialVersionUID = 873845656L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QPenzugyiElemzesHatekonysag penzugyiElemzesHatekonysag = new QPenzugyiElemzesHatekonysag("penzugyiElemzesHatekonysag");

    public final NumberPath<Integer> elozoev_atlagos_hat = createNumber("elozoev_atlagos_hat", Integer.class);

    public final NumberPath<Integer> elozoev_fore_juto_hat = createNumber("elozoev_fore_juto_hat", Integer.class);

    public final NumberPath<Integer> elozoev_forgo_toke_hat = createNumber("elozoev_forgo_toke_hat", Integer.class);

    public final NumberPath<Integer> elozoev_keszlet_hat = createNumber("elozoev_keszlet_hat", Integer.class);

    public final NumberPath<Integer> elozoev_koltseg_hat = createNumber("elozoev_koltseg_hat", Integer.class);

    public final NumberPath<Integer> elozoev_szallito_hat = createNumber("elozoev_szallito_hat", Integer.class);

    public final NumberPath<Integer> ert_hat = createNumber("ert_hat", Integer.class);

    public final NumberPath<Integer> fore_juto_letszam_hat = createNumber("fore_juto_letszam_hat", Integer.class);

    public final NumberPath<Integer> fore_juto_netto_hat = createNumber("fore_juto_netto_hat", Integer.class);

    public final NumberPath<Integer> forgo_toke_hat_forgo_eszkoz = createNumber("forgo_toke_hat_forgo_eszkoz", Integer.class);

    public final NumberPath<Integer> forgo_toke_hat_netto_arbevetel = createNumber("forgo_toke_hat_netto_arbevetel", Integer.class);

    public final NumberPath<Integer> forgo_toke_hat_rovid_lej_kot = createNumber("forgo_toke_hat_rovid_lej_kot", Integer.class);

    public final NumberPath<Integer> hat_atlagos_koveteles = createNumber("hat_atlagos_koveteles", Integer.class);

    public final NumberPath<Integer> hat_atlagos_netto_arbevetel = createNumber("hat_atlagos_netto_arbevetel", Integer.class);

    public final NumberPath<Integer> hat_egyeb_mukodesi = createNumber("hat_egyeb_mukodesi", Integer.class);

    public final StringPath hat_megjegyzes = createString("hat_megjegyzes");

    public final NumberPath<Integer> hat_netto_arbevetel = createNumber("hat_netto_arbevetel", Integer.class);

    public final NumberPath<Integer> hat_szallito_kotelezettseg = createNumber("hat_szallito_kotelezettseg", Integer.class);

    public final NumberPath<Integer> hat_szallito_netto_arbevetel = createNumber("hat_szallito_netto_arbevetel", Integer.class);

    public final springsbinvesting.domain.reszvenyelemzes.QVallalatPenzugyiElemzes hatPenzElemzes;

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Integer> keszlet_hat = createNumber("keszlet_hat", Integer.class);

    public final NumberPath<Integer> konkurencia_atlagos_hat = createNumber("konkurencia_atlagos_hat", Integer.class);

    public final NumberPath<Integer> konkurencia_fore_juto_hat = createNumber("konkurencia_fore_juto_hat", Integer.class);

    public final NumberPath<Integer> konkurencia_forgo_toke_hat = createNumber("konkurencia_forgo_toke_hat", Integer.class);

    public final NumberPath<Integer> konkurencia_keszlet_hat = createNumber("konkurencia_keszlet_hat", Integer.class);

    public final NumberPath<Integer> konkurencia_koltseg_hat = createNumber("konkurencia_koltseg_hat", Integer.class);

    public final NumberPath<Integer> konkurencia_szallito_hat = createNumber("konkurencia_szallito_hat", Integer.class);

    public final NumberPath<Integer> naptar_hat = createNumber("naptar_hat", Integer.class);

    public final NumberPath<Integer> naptar_hat_szallito = createNumber("naptar_hat_szallito", Integer.class);

    public final NumberPath<Integer> netto_hat = createNumber("netto_hat", Integer.class);

    public final NumberPath<Integer> targyev_atlagos_hat = createNumber("targyev_atlagos_hat", Integer.class);

    public final NumberPath<Integer> targyev_fore_juto_hat = createNumber("targyev_fore_juto_hat", Integer.class);

    public final NumberPath<Integer> targyev_forgo_toke_hat = createNumber("targyev_forgo_toke_hat", Integer.class);

    public final NumberPath<Integer> targyev_keszlet_hat = createNumber("targyev_keszlet_hat", Integer.class);

    public final NumberPath<Integer> targyev_koltseg_hat = createNumber("targyev_koltseg_hat", Integer.class);

    public final NumberPath<Integer> targyev_szallito_hat = createNumber("targyev_szallito_hat", Integer.class);

    public QPenzugyiElemzesHatekonysag(String variable) {
        this(PenzugyiElemzesHatekonysag.class, forVariable(variable), INITS);
    }

    public QPenzugyiElemzesHatekonysag(Path<? extends PenzugyiElemzesHatekonysag> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QPenzugyiElemzesHatekonysag(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QPenzugyiElemzesHatekonysag(PathMetadata metadata, PathInits inits) {
        this(PenzugyiElemzesHatekonysag.class, metadata, inits);
    }

    public QPenzugyiElemzesHatekonysag(Class<? extends PenzugyiElemzesHatekonysag> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.hatPenzElemzes = inits.isInitialized("hatPenzElemzes") ? new springsbinvesting.domain.reszvenyelemzes.QVallalatPenzugyiElemzes(forProperty("hatPenzElemzes"), inits.get("hatPenzElemzes")) : null;
    }

}

