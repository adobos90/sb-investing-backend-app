package springsbinvesting.domain.reszvenyelemzes.penzugyielemzes;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QPenzugyiElemzesJovedelem is a Querydsl query type for PenzugyiElemzesJovedelem
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QPenzugyiElemzesJovedelem extends EntityPathBase<PenzugyiElemzesJovedelem> {

    private static final long serialVersionUID = 967019127L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QPenzugyiElemzesJovedelem penzugyiElemzesJovedelem = new QPenzugyiElemzesJovedelem("penzugyiElemzesJovedelem");

    public final NumberPath<Integer> arbevetel = createNumber("arbevetel", Integer.class);

    public final NumberPath<Integer> brutto_fedezet = createNumber("brutto_fedezet", Integer.class);

    public final NumberPath<Integer> eladott_aruk_ktg = createNumber("eladott_aruk_ktg", Integer.class);

    public final NumberPath<Integer> elozoev_arbevetel = createNumber("elozoev_arbevetel", Integer.class);

    public final NumberPath<Integer> elozoev_eszkoz = createNumber("elozoev_eszkoz", Integer.class);

    public final NumberPath<Integer> elozoev_fedezet = createNumber("elozoev_fedezet", Integer.class);

    public final NumberPath<Integer> elozoev_reszveny = createNumber("elozoev_reszveny", Integer.class);

    public final NumberPath<Integer> elozoev_sajat = createNumber("elozoev_sajat", Integer.class);

    public final NumberPath<Integer> eszkozok_eszkoz = createNumber("eszkozok_eszkoz", Integer.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath jov_megjegyzes = createString("jov_megjegyzes");

    public final springsbinvesting.domain.reszvenyelemzes.QVallalatPenzugyiElemzes jovPenzElemzes;

    public final NumberPath<Integer> konkurencia_arbevetel = createNumber("konkurencia_arbevetel", Integer.class);

    public final NumberPath<Integer> konkurencia_eszkoz = createNumber("konkurencia_eszkoz", Integer.class);

    public final NumberPath<Integer> konkurencia_fedezet = createNumber("konkurencia_fedezet", Integer.class);

    public final NumberPath<Integer> konkurencia_reszveny = createNumber("konkurencia_reszveny", Integer.class);

    public final NumberPath<Integer> konkurencia_sajat = createNumber("konkurencia_sajat", Integer.class);

    public final NumberPath<Integer> netto_arbevetel = createNumber("netto_arbevetel", Integer.class);

    public final NumberPath<Integer> netto_eszkoz = createNumber("netto_eszkoz", Integer.class);

    public final NumberPath<Integer> netto_fedezet = createNumber("netto_fedezet", Integer.class);

    public final NumberPath<Integer> netto_reszveny = createNumber("netto_reszveny", Integer.class);

    public final NumberPath<Integer> netto_sajat = createNumber("netto_sajat", Integer.class);

    public final NumberPath<Integer> ossz_sajat_toke = createNumber("ossz_sajat_toke", Integer.class);

    public final NumberPath<Integer> reszveny_db = createNumber("reszveny_db", Integer.class);

    public final NumberPath<Integer> targyev_arbevetel = createNumber("targyev_arbevetel", Integer.class);

    public final NumberPath<Integer> targyev_eszkoz = createNumber("targyev_eszkoz", Integer.class);

    public final NumberPath<Integer> targyev_fedezet = createNumber("targyev_fedezet", Integer.class);

    public final NumberPath<Integer> targyev_reszveny = createNumber("targyev_reszveny", Integer.class);

    public final NumberPath<Integer> targyev_sajat = createNumber("targyev_sajat", Integer.class);

    public QPenzugyiElemzesJovedelem(String variable) {
        this(PenzugyiElemzesJovedelem.class, forVariable(variable), INITS);
    }

    public QPenzugyiElemzesJovedelem(Path<? extends PenzugyiElemzesJovedelem> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QPenzugyiElemzesJovedelem(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QPenzugyiElemzesJovedelem(PathMetadata metadata, PathInits inits) {
        this(PenzugyiElemzesJovedelem.class, metadata, inits);
    }

    public QPenzugyiElemzesJovedelem(Class<? extends PenzugyiElemzesJovedelem> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.jovPenzElemzes = inits.isInitialized("jovPenzElemzes") ? new springsbinvesting.domain.reszvenyelemzes.QVallalatPenzugyiElemzes(forProperty("jovPenzElemzes"), inits.get("jovPenzElemzes")) : null;
    }

}

