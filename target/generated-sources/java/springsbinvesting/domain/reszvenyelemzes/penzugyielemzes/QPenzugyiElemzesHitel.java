package springsbinvesting.domain.reszvenyelemzes.penzugyielemzes;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QPenzugyiElemzesHitel is a Querydsl query type for PenzugyiElemzesHitel
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QPenzugyiElemzesHitel extends EntityPathBase<PenzugyiElemzesHitel> {

    private static final long serialVersionUID = 599310898L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QPenzugyiElemzesHitel penzugyiElemzesHitel = new QPenzugyiElemzesHitel("penzugyiElemzesHitel");

    public final NumberPath<Integer> elozoev_kamat_hitel = createNumber("elozoev_kamat_hitel", Integer.class);

    public final NumberPath<Integer> elozoev_likv_hitel = createNumber("elozoev_likv_hitel", Integer.class);

    public final NumberPath<Integer> elozoev_rovid_likv_hitel = createNumber("elozoev_rovid_likv_hitel", Integer.class);

    public final NumberPath<Integer> elozoev_sajat_toke_hitel = createNumber("elozoev_sajat_toke_hitel", Integer.class);

    public final NumberPath<Integer> hitel_elszamolt_amortizacio = createNumber("hitel_elszamolt_amortizacio", Integer.class);

    public final NumberPath<Integer> hitel_imm_javak = createNumber("hitel_imm_javak", Integer.class);

    public final NumberPath<Integer> hitel_jovairas = createNumber("hitel_jovairas", Integer.class);

    public final NumberPath<Integer> hitel_kamat = createNumber("hitel_kamat", Integer.class);

    public final NumberPath<Integer> hitel_kamat_jov_ado_elotti_eredmeny = createNumber("hitel_kamat_jov_ado_elotti_eredmeny", Integer.class);

    public final NumberPath<Integer> hitel_kifizetett_osztalek = createNumber("hitel_kifizetett_osztalek", Integer.class);

    public final NumberPath<Integer> hitel_likv_keszletek = createNumber("hitel_likv_keszletek", Integer.class);

    public final NumberPath<Integer> hitel_likv_osszforgo_eszkoz = createNumber("hitel_likv_osszforgo_eszkoz", Integer.class);

    public final NumberPath<Integer> hitel_likv_rovid_lej_kot = createNumber("hitel_likv_rovid_lej_kot", Integer.class);

    public final StringPath hitel_megjegyzes = createString("hitel_megjegyzes");

    public final NumberPath<Integer> hitel_ossz_hossz_lej_kot = createNumber("hitel_ossz_hossz_lej_kot", Integer.class);

    public final NumberPath<Integer> hitel_rovid_likv_kotelezettsegek = createNumber("hitel_rovid_likv_kotelezettsegek", Integer.class);

    public final NumberPath<Integer> hitel_rovid_likv_osszforgo_eszkoz = createNumber("hitel_rovid_likv_osszforgo_eszkoz", Integer.class);

    public final NumberPath<Integer> hitel_sajat_toke = createNumber("hitel_sajat_toke", Integer.class);

    public final springsbinvesting.domain.reszvenyelemzes.QVallalatPenzugyiElemzes hitelPenzElemzes;

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Integer> konkurencia_kamat_hitel = createNumber("konkurencia_kamat_hitel", Integer.class);

    public final NumberPath<Integer> konkurencia_likv_hitel = createNumber("konkurencia_likv_hitel", Integer.class);

    public final NumberPath<Integer> konkurencia_rovid_likv_hitel = createNumber("konkurencia_rovid_likv_hitel", Integer.class);

    public final NumberPath<Integer> konkurencia_sajat_toke_hitel = createNumber("konkurencia_sajat_toke_hitel", Integer.class);

    public final NumberPath<Integer> targyev_kamat_hitel = createNumber("targyev_kamat_hitel", Integer.class);

    public final NumberPath<Integer> targyev_likv_hitel = createNumber("targyev_likv_hitel", Integer.class);

    public final NumberPath<Integer> targyev_rovid_likv_hitel = createNumber("targyev_rovid_likv_hitel", Integer.class);

    public final NumberPath<Integer> targyev_sajat_toke_hitel = createNumber("targyev_sajat_toke_hitel", Integer.class);

    public QPenzugyiElemzesHitel(String variable) {
        this(PenzugyiElemzesHitel.class, forVariable(variable), INITS);
    }

    public QPenzugyiElemzesHitel(Path<? extends PenzugyiElemzesHitel> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QPenzugyiElemzesHitel(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QPenzugyiElemzesHitel(PathMetadata metadata, PathInits inits) {
        this(PenzugyiElemzesHitel.class, metadata, inits);
    }

    public QPenzugyiElemzesHitel(Class<? extends PenzugyiElemzesHitel> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.hitelPenzElemzes = inits.isInitialized("hitelPenzElemzes") ? new springsbinvesting.domain.reszvenyelemzes.QVallalatPenzugyiElemzes(forProperty("hitelPenzElemzes"), inits.get("hitelPenzElemzes")) : null;
    }

}

