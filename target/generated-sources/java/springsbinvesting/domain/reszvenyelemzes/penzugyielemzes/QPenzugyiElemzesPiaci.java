package springsbinvesting.domain.reszvenyelemzes.penzugyielemzes;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QPenzugyiElemzesPiaci is a Querydsl query type for PenzugyiElemzesPiaci
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QPenzugyiElemzesPiaci extends EntityPathBase<PenzugyiElemzesPiaci> {

    private static final long serialVersionUID = 606680742L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QPenzugyiElemzesPiaci penzugyiElemzesPiaci = new QPenzugyiElemzesPiaci("penzugyiElemzesPiaci");

    public final NumberPath<Integer> elozoev_piaci_arfolyam_nyereseg = createNumber("elozoev_piaci_arfolyam_nyereseg", Integer.class);

    public final NumberPath<Integer> elozoev_piaci_bef_kamatlab = createNumber("elozoev_piaci_bef_kamatlab", Integer.class);

    public final NumberPath<Integer> elozoev_piaci_konyv = createNumber("elozoev_piaci_konyv", Integer.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Integer> konkurencia_piaci_arfolyam_nyereseg = createNumber("konkurencia_piaci_arfolyam_nyereseg", Integer.class);

    public final NumberPath<Integer> konkurencia_piaci_bef_kamatlab = createNumber("konkurencia_piaci_bef_kamatlab", Integer.class);

    public final NumberPath<Integer> konkurencia_piaci_konyv = createNumber("konkurencia_piaci_konyv", Integer.class);

    public final NumberPath<Integer> piaci_arfolyam_nyereseg = createNumber("piaci_arfolyam_nyereseg", Integer.class);

    public final NumberPath<Integer> piaci_bef_kam_netto_eredmeny = createNumber("piaci_bef_kam_netto_eredmeny", Integer.class);

    public final NumberPath<Integer> piaci_bef_kam_reszv_db_szama = createNumber("piaci_bef_kam_reszv_db_szama", Integer.class);

    public final NumberPath<Integer> piaci_bef_kam_reszveny_arfolyama = createNumber("piaci_bef_kam_reszveny_arfolyama", Integer.class);

    public final NumberPath<Integer> piaci_konyv_goodwill = createNumber("piaci_konyv_goodwill", Integer.class);

    public final NumberPath<Integer> piaci_konyv_imm_javak = createNumber("piaci_konyv_imm_javak", Integer.class);

    public final NumberPath<Integer> piaci_konyv_reszveny_arfolyam = createNumber("piaci_konyv_reszveny_arfolyam", Integer.class);

    public final NumberPath<Integer> piaci_konyv_reszvenyek_db_szama = createNumber("piaci_konyv_reszvenyek_db_szama", Integer.class);

    public final NumberPath<Integer> piaci_konyv_sajat_toke = createNumber("piaci_konyv_sajat_toke", Integer.class);

    public final StringPath piaci_megjegyzes = createString("piaci_megjegyzes");

    public final NumberPath<Integer> piaci_reszvenyek_db_szama = createNumber("piaci_reszvenyek_db_szama", Integer.class);

    public final NumberPath<Integer> piaci_tars_netto_eredmeny = createNumber("piaci_tars_netto_eredmeny", Integer.class);

    public final springsbinvesting.domain.reszvenyelemzes.QVallalatPenzugyiElemzes piaciPenzElemzes;

    public final NumberPath<Integer> targyev_piaci_arfolyam_nyereseg = createNumber("targyev_piaci_arfolyam_nyereseg", Integer.class);

    public final NumberPath<Integer> targyev_piaci_bef_kamatlab = createNumber("targyev_piaci_bef_kamatlab", Integer.class);

    public final NumberPath<Integer> targyev_piaci_konyv = createNumber("targyev_piaci_konyv", Integer.class);

    public QPenzugyiElemzesPiaci(String variable) {
        this(PenzugyiElemzesPiaci.class, forVariable(variable), INITS);
    }

    public QPenzugyiElemzesPiaci(Path<? extends PenzugyiElemzesPiaci> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QPenzugyiElemzesPiaci(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QPenzugyiElemzesPiaci(PathMetadata metadata, PathInits inits) {
        this(PenzugyiElemzesPiaci.class, metadata, inits);
    }

    public QPenzugyiElemzesPiaci(Class<? extends PenzugyiElemzesPiaci> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.piaciPenzElemzes = inits.isInitialized("piaciPenzElemzes") ? new springsbinvesting.domain.reszvenyelemzes.QVallalatPenzugyiElemzes(forProperty("piaciPenzElemzes"), inits.get("piaciPenzElemzes")) : null;
    }

}

