package springsbinvesting.domain.reszvenyelemzes;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QPenzugyiAdatok is a Querydsl query type for PenzugyiAdatok
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QPenzugyiAdatok extends EntityPathBase<PenzugyiAdatok> {

    private static final long serialVersionUID = -387818517L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QPenzugyiAdatok penzugyiAdatok = new QPenzugyiAdatok("penzugyiAdatok");

    public final NumberPath<Integer> adm_elozo_ev = createNumber("adm_elozo_ev", Integer.class);

    public final NumberPath<Integer> adm_konkurencia = createNumber("adm_konkurencia", Integer.class);

    public final NumberPath<Integer> adm_q2 = createNumber("adm_q2", Integer.class);

    public final NumberPath<Integer> adm_q3 = createNumber("adm_q3", Integer.class);

    public final NumberPath<Integer> adm_targyev = createNumber("adm_targyev", Integer.class);

    public final NumberPath<Integer> alkalmazottak_szama_elozo_ev = createNumber("alkalmazottak_szama_elozo_ev", Integer.class);

    public final NumberPath<Integer> alkalmazottak_szama_konkurencia = createNumber("alkalmazottak_szama_konkurencia", Integer.class);

    public final NumberPath<Integer> alkalmazottak_szama_q2 = createNumber("alkalmazottak_szama_q2", Integer.class);

    public final NumberPath<Integer> alkalmazottak_szama_q3 = createNumber("alkalmazottak_szama_q3", Integer.class);

    public final NumberPath<Integer> alkalmazottak_szama_targyev = createNumber("alkalmazottak_szama_targyev", Integer.class);

    public final NumberPath<Integer> egyeb_elozo_ev = createNumber("egyeb_elozo_ev", Integer.class);

    public final NumberPath<Integer> egyeb_konkurencia = createNumber("egyeb_konkurencia", Integer.class);

    public final NumberPath<Integer> egyeb_q2 = createNumber("egyeb_q2", Integer.class);

    public final NumberPath<Integer> egyeb_q3 = createNumber("egyeb_q3", Integer.class);

    public final NumberPath<Integer> egyeb_targyev = createNumber("egyeb_targyev", Integer.class);

    public final NumberPath<Integer> eladott_aru_elozo_ev = createNumber("eladott_aru_elozo_ev", Integer.class);

    public final NumberPath<Integer> eladott_aru_konkurencia = createNumber("eladott_aru_konkurencia", Integer.class);

    public final NumberPath<Integer> eladott_aru_q2 = createNumber("eladott_aru_q2", Integer.class);

    public final NumberPath<Integer> eladott_aru_q3 = createNumber("eladott_aru_q3", Integer.class);

    public final NumberPath<Integer> eladott_aru_targyev = createNumber("eladott_aru_targyev", Integer.class);

    public final NumberPath<Integer> elszamolt_elozo_ev = createNumber("elszamolt_elozo_ev", Integer.class);

    public final NumberPath<Integer> elszamolt_konkurencia = createNumber("elszamolt_konkurencia", Integer.class);

    public final NumberPath<Integer> elszamolt_q2 = createNumber("elszamolt_q2", Integer.class);

    public final NumberPath<Integer> elszamolt_q3 = createNumber("elszamolt_q3", Integer.class);

    public final NumberPath<Integer> elszamolt_targyev = createNumber("elszamolt_targyev", Integer.class);

    public final NumberPath<Integer> eszkoz_elozo_ev = createNumber("eszkoz_elozo_ev", Integer.class);

    public final NumberPath<Integer> eszkoz_konkurencia = createNumber("eszkoz_konkurencia", Integer.class);

    public final NumberPath<Integer> eszkoz_q2 = createNumber("eszkoz_q2", Integer.class);

    public final NumberPath<Integer> eszkoz_q3 = createNumber("eszkoz_q3", Integer.class);

    public final NumberPath<Integer> eszkoz_targyev = createNumber("eszkoz_targyev", Integer.class);

    public final NumberPath<Integer> fizetett_kamatok_elozo_ev = createNumber("fizetett_kamatok_elozo_ev", Integer.class);

    public final NumberPath<Integer> fizetett_kamatok_konkurencia = createNumber("fizetett_kamatok_konkurencia", Integer.class);

    public final NumberPath<Integer> fizetett_kamatok_q2 = createNumber("fizetett_kamatok_q2", Integer.class);

    public final NumberPath<Integer> fizetett_kamatok_q3 = createNumber("fizetett_kamatok_q3", Integer.class);

    public final NumberPath<Integer> fizetett_kamatok_targyev = createNumber("fizetett_kamatok_targyev", Integer.class);

    public final NumberPath<Integer> forgo_elozo_ev = createNumber("forgo_elozo_ev", Integer.class);

    public final NumberPath<Integer> forgo_konkurencia = createNumber("forgo_konkurencia", Integer.class);

    public final NumberPath<Integer> forgo_q2 = createNumber("forgo_q2", Integer.class);

    public final NumberPath<Integer> forgo_q3 = createNumber("forgo_q3", Integer.class);

    public final NumberPath<Integer> forgo_targyev = createNumber("forgo_targyev", Integer.class);

    public final NumberPath<Integer> hozam_elozo_ev = createNumber("hozam_elozo_ev", Integer.class);

    public final NumberPath<Integer> hozam_konkurencia = createNumber("hozam_konkurencia", Integer.class);

    public final NumberPath<Integer> hozam_q2 = createNumber("hozam_q2", Integer.class);

    public final NumberPath<Integer> hozam_q3 = createNumber("hozam_q3", Integer.class);

    public final NumberPath<Integer> hozam_targyev = createNumber("hozam_targyev", Integer.class);

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final NumberPath<Integer> imm_elozo_ev = createNumber("imm_elozo_ev", Integer.class);

    public final NumberPath<Integer> imm_konkurencia = createNumber("imm_konkurencia", Integer.class);

    public final NumberPath<Integer> imm_q2 = createNumber("imm_q2", Integer.class);

    public final NumberPath<Integer> imm_q3 = createNumber("imm_q3", Integer.class);

    public final NumberPath<Integer> imm_targyev = createNumber("imm_targyev", Integer.class);

    public final NumberPath<Integer> jovairas_elozo_ev = createNumber("jovairas_elozo_ev", Integer.class);

    public final NumberPath<Integer> jovairas_konkurencia = createNumber("jovairas_konkurencia", Integer.class);

    public final NumberPath<Integer> jovairas_q2 = createNumber("jovairas_q2", Integer.class);

    public final NumberPath<Integer> jovairas_q3 = createNumber("jovairas_q3", Integer.class);

    public final NumberPath<Integer> jovairas_targyev = createNumber("jovairas_targyev", Integer.class);

    public final NumberPath<Integer> kamat_elozo_ev = createNumber("kamat_elozo_ev", Integer.class);

    public final NumberPath<Integer> kamat_konkurencia = createNumber("kamat_konkurencia", Integer.class);

    public final NumberPath<Integer> kamat_q2 = createNumber("kamat_q2", Integer.class);

    public final NumberPath<Integer> kamat_q3 = createNumber("kamat_q3", Integer.class);

    public final NumberPath<Integer> kamat_targyev = createNumber("kamat_targyev", Integer.class);

    public final NumberPath<Integer> keszlet_elozo_ev = createNumber("keszlet_elozo_ev", Integer.class);

    public final NumberPath<Integer> keszlet_konkurencia = createNumber("keszlet_konkurencia", Integer.class);

    public final NumberPath<Integer> keszlet_q2 = createNumber("keszlet_q2", Integer.class);

    public final NumberPath<Integer> keszlet_q3 = createNumber("keszlet_q3", Integer.class);

    public final NumberPath<Integer> keszlet_targyev = createNumber("keszlet_targyev", Integer.class);

    public final NumberPath<Integer> keszpenz_elozo_ev = createNumber("keszpenz_elozo_ev", Integer.class);

    public final NumberPath<Integer> keszpenz_konkurencia = createNumber("keszpenz_konkurencia", Integer.class);

    public final NumberPath<Integer> keszpenz_q2 = createNumber("keszpenz_q2", Integer.class);

    public final NumberPath<Integer> keszpenz_q3 = createNumber("keszpenz_q3", Integer.class);

    public final NumberPath<Integer> keszpenz_targyev = createNumber("keszpenz_targyev", Integer.class);

    public final NumberPath<Integer> kot_szallito_elozo_ev = createNumber("kot_szallito_elozo_ev", Integer.class);

    public final NumberPath<Integer> kot_szallito_konkurencia = createNumber("kot_szallito_konkurencia", Integer.class);

    public final NumberPath<Integer> kot_szallito_q2 = createNumber("kot_szallito_q2", Integer.class);

    public final NumberPath<Integer> kot_szallito_q3 = createNumber("kot_szallito_q3", Integer.class);

    public final NumberPath<Integer> kot_szallito_targyev = createNumber("kot_szallito_targyev", Integer.class);

    public final NumberPath<Integer> koveteles_vevo_elozo_ev = createNumber("koveteles_vevo_elozo_ev", Integer.class);

    public final NumberPath<Integer> koveteles_vevo_konkurencia = createNumber("koveteles_vevo_konkurencia", Integer.class);

    public final NumberPath<Integer> koveteles_vevo_q2 = createNumber("koveteles_vevo_q2", Integer.class);

    public final NumberPath<Integer> koveteles_vevo_q3 = createNumber("koveteles_vevo_q3", Integer.class);

    public final NumberPath<Integer> koveteles_vevo_targyev = createNumber("koveteles_vevo_targyev", Integer.class);

    public final NumberPath<Integer> naptar_elozo_ev = createNumber("naptar_elozo_ev", Integer.class);

    public final NumberPath<Integer> naptar_konkurencia = createNumber("naptar_konkurencia", Integer.class);

    public final NumberPath<Integer> naptar_q2 = createNumber("naptar_q2", Integer.class);

    public final NumberPath<Integer> naptar_q3 = createNumber("naptar_q3", Integer.class);

    public final NumberPath<Integer> naptar_targyev = createNumber("naptar_targyev", Integer.class);

    public final NumberPath<Integer> netto_elozo_ev = createNumber("netto_elozo_ev", Integer.class);

    public final NumberPath<Integer> netto_konkurencia = createNumber("netto_konkurencia", Integer.class);

    public final NumberPath<Integer> netto_q2 = createNumber("netto_q2", Integer.class);

    public final NumberPath<Integer> netto_q3 = createNumber("netto_q3", Integer.class);

    public final NumberPath<Integer> netto_targyev = createNumber("netto_targyev", Integer.class);

    public final NumberPath<Integer> ossz_kot_elozo_ev = createNumber("ossz_kot_elozo_ev", Integer.class);

    public final NumberPath<Integer> ossz_kot_konkurencia = createNumber("ossz_kot_konkurencia", Integer.class);

    public final NumberPath<Integer> ossz_kot_q2 = createNumber("ossz_kot_q2", Integer.class);

    public final NumberPath<Integer> ossz_kot_q3 = createNumber("ossz_kot_q3", Integer.class);

    public final NumberPath<Integer> ossz_kot_targyev = createNumber("ossz_kot_targyev", Integer.class);

    public final NumberPath<Integer> osztalek_elozo_ev = createNumber("osztalek_elozo_ev", Integer.class);

    public final NumberPath<Integer> osztalek_konkurencia = createNumber("osztalek_konkurencia", Integer.class);

    public final NumberPath<Integer> osztalek_q2 = createNumber("osztalek_q2", Integer.class);

    public final NumberPath<Integer> osztalek_q3 = createNumber("osztalek_q3", Integer.class);

    public final NumberPath<Integer> osztalek_targyev = createNumber("osztalek_targyev", Integer.class);

    public final springsbinvesting.domain.QReszveny penzAdatokReszveny;

    public final NumberPath<Integer> reszveny_arfolyam_elozo_ev = createNumber("reszveny_arfolyam_elozo_ev", Integer.class);

    public final NumberPath<Integer> reszveny_arfolyam_konkurencia = createNumber("reszveny_arfolyam_konkurencia", Integer.class);

    public final NumberPath<Integer> reszveny_arfolyam_q2 = createNumber("reszveny_arfolyam_q2", Integer.class);

    public final NumberPath<Integer> reszveny_arfolyam_q3 = createNumber("reszveny_arfolyam_q3", Integer.class);

    public final NumberPath<Integer> reszveny_arfolyam_targyev = createNumber("reszveny_arfolyam_targyev", Integer.class);

    public final NumberPath<Integer> reszveny_elozo_ev = createNumber("reszveny_elozo_ev", Integer.class);

    public final NumberPath<Integer> reszveny_konkurencia = createNumber("reszveny_konkurencia", Integer.class);

    public final NumberPath<Integer> reszveny_q2 = createNumber("reszveny_q2", Integer.class);

    public final NumberPath<Integer> reszveny_q3 = createNumber("reszveny_q3", Integer.class);

    public final NumberPath<Integer> reszveny_targyev = createNumber("reszveny_targyev", Integer.class);

    public final NumberPath<Integer> rovid_lejaratu_kot_elozo_ev = createNumber("rovid_lejaratu_kot_elozo_ev", Integer.class);

    public final NumberPath<Integer> rovid_lejaratu_kot_konkurencia = createNumber("rovid_lejaratu_kot_konkurencia", Integer.class);

    public final NumberPath<Integer> rovid_lejaratu_kot_q2 = createNumber("rovid_lejaratu_kot_q2", Integer.class);

    public final NumberPath<Integer> rovid_lejaratu_kot_q3 = createNumber("rovid_lejaratu_kot_q3", Integer.class);

    public final NumberPath<Integer> rovid_lejaratu_kot_targyev = createNumber("rovid_lejaratu_kot_targyev", Integer.class);

    public final NumberPath<Integer> tarsasag_elozo_ev = createNumber("tarsasag_elozo_ev", Integer.class);

    public final NumberPath<Integer> tarsasag_konkurencia = createNumber("tarsasag_konkurencia", Integer.class);

    public final NumberPath<Integer> tarsasag_q2 = createNumber("tarsasag_q2", Integer.class);

    public final NumberPath<Integer> tarsasag_q3 = createNumber("tarsasag_q3", Integer.class);

    public final NumberPath<Integer> tarsasag_targyev = createNumber("tarsasag_targyev", Integer.class);

    public final NumberPath<Integer> toke_elozo_ev = createNumber("toke_elozo_ev", Integer.class);

    public final NumberPath<Integer> toke_konkurencia = createNumber("toke_konkurencia", Integer.class);

    public final NumberPath<Integer> toke_q2 = createNumber("toke_q2", Integer.class);

    public final NumberPath<Integer> toke_q3 = createNumber("toke_q3", Integer.class);

    public final NumberPath<Integer> toke_targyev = createNumber("toke_targyev", Integer.class);

    public QPenzugyiAdatok(String variable) {
        this(PenzugyiAdatok.class, forVariable(variable), INITS);
    }

    public QPenzugyiAdatok(Path<? extends PenzugyiAdatok> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QPenzugyiAdatok(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QPenzugyiAdatok(PathMetadata metadata, PathInits inits) {
        this(PenzugyiAdatok.class, metadata, inits);
    }

    public QPenzugyiAdatok(Class<? extends PenzugyiAdatok> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.penzAdatokReszveny = inits.isInitialized("penzAdatokReszveny") ? new springsbinvesting.domain.QReszveny(forProperty("penzAdatokReszveny"), inits.get("penzAdatokReszveny")) : null;
    }

}

