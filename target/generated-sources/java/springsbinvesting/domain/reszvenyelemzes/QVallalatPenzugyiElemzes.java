package springsbinvesting.domain.reszvenyelemzes;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QVallalatPenzugyiElemzes is a Querydsl query type for VallalatPenzugyiElemzes
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QVallalatPenzugyiElemzes extends EntityPathBase<VallalatPenzugyiElemzes> {

    private static final long serialVersionUID = -495457001L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QVallalatPenzugyiElemzes vallalatPenzugyiElemzes = new QVallalatPenzugyiElemzes("vallalatPenzugyiElemzes");

    public final SetPath<springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.PenzugyiElemzesHatekonysag, springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.QPenzugyiElemzesHatekonysag> hatPenzElemzes = this.<springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.PenzugyiElemzesHatekonysag, springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.QPenzugyiElemzesHatekonysag>createSet("hatPenzElemzes", springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.PenzugyiElemzesHatekonysag.class, springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.QPenzugyiElemzesHatekonysag.class, PathInits.DIRECT2);

    public final SetPath<springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.PenzugyiElemzesHitel, springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.QPenzugyiElemzesHitel> hitelPenzElemzes = this.<springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.PenzugyiElemzesHitel, springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.QPenzugyiElemzesHitel>createSet("hitelPenzElemzes", springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.PenzugyiElemzesHitel.class, springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.QPenzugyiElemzesHitel.class, PathInits.DIRECT2);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final SetPath<springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.PenzugyiElemzesJovedelem, springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.QPenzugyiElemzesJovedelem> jovPenzElemzes = this.<springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.PenzugyiElemzesJovedelem, springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.QPenzugyiElemzesJovedelem>createSet("jovPenzElemzes", springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.PenzugyiElemzesJovedelem.class, springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.QPenzugyiElemzesJovedelem.class, PathInits.DIRECT2);

    public final springsbinvesting.domain.QReszveny penzugyReszveny;

    public final SetPath<springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.PenzugyiElemzesPiaci, springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.QPenzugyiElemzesPiaci> piaciPenzElemzes = this.<springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.PenzugyiElemzesPiaci, springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.QPenzugyiElemzesPiaci>createSet("piaciPenzElemzes", springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.PenzugyiElemzesPiaci.class, springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.QPenzugyiElemzesPiaci.class, PathInits.DIRECT2);

    public QVallalatPenzugyiElemzes(String variable) {
        this(VallalatPenzugyiElemzes.class, forVariable(variable), INITS);
    }

    public QVallalatPenzugyiElemzes(Path<? extends VallalatPenzugyiElemzes> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QVallalatPenzugyiElemzes(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QVallalatPenzugyiElemzes(PathMetadata metadata, PathInits inits) {
        this(VallalatPenzugyiElemzes.class, metadata, inits);
    }

    public QVallalatPenzugyiElemzes(Class<? extends VallalatPenzugyiElemzes> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.penzugyReszveny = inits.isInitialized("penzugyReszveny") ? new springsbinvesting.domain.QReszveny(forProperty("penzugyReszveny"), inits.get("penzugyReszveny")) : null;
    }

}

