package springsbinvesting.domain.reszvenyelemzes;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QCelar is a Querydsl query type for Celar
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QCelar extends EntityPathBase<Celar> {

    private static final long serialVersionUID = -768383419L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QCelar celar = new QCelar("celar");

    public final springsbinvesting.domain.QReszveny celarReszveny;

    public final NumberPath<Integer> elvart_hozam = createNumber("elvart_hozam", Integer.class);

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final NumberPath<Integer> konkurencia_mutato = createNumber("konkurencia_mutato", Integer.class);

    public final NumberPath<Integer> konyv_szerinti_ertek = createNumber("konyv_szerinti_ertek", Integer.class);

    public QCelar(String variable) {
        this(Celar.class, forVariable(variable), INITS);
    }

    public QCelar(Path<? extends Celar> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QCelar(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QCelar(PathMetadata metadata, PathInits inits) {
        this(Celar.class, metadata, inits);
    }

    public QCelar(Class<? extends Celar> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.celarReszveny = inits.isInitialized("celarReszveny") ? new springsbinvesting.domain.QReszveny(forProperty("celarReszveny"), inits.get("celarReszveny")) : null;
    }

}

