package springsbinvesting.domain.reszvenyelemzes;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QManageles is a Querydsl query type for Manageles
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QManageles extends EntityPathBase<Manageles> {

    private static final long serialVersionUID = -1234870817L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QManageles manageles = new QManageles("manageles");

    public final SetPath<springsbinvesting.domain.reszvenyelemzes.manageles.ManagelesBefAdatok, springsbinvesting.domain.reszvenyelemzes.manageles.QManagelesBefAdatok> befAdatokmanageles = this.<springsbinvesting.domain.reszvenyelemzes.manageles.ManagelesBefAdatok, springsbinvesting.domain.reszvenyelemzes.manageles.QManagelesBefAdatok>createSet("befAdatokmanageles", springsbinvesting.domain.reszvenyelemzes.manageles.ManagelesBefAdatok.class, springsbinvesting.domain.reszvenyelemzes.manageles.QManagelesBefAdatok.class, PathInits.DIRECT2);

    public final SetPath<springsbinvesting.domain.reszvenyelemzes.manageles.MenBefMenegeles, springsbinvesting.domain.reszvenyelemzes.manageles.QMenBefMenegeles> befMenmanageles = this.<springsbinvesting.domain.reszvenyelemzes.manageles.MenBefMenegeles, springsbinvesting.domain.reszvenyelemzes.manageles.QMenBefMenegeles>createSet("befMenmanageles", springsbinvesting.domain.reszvenyelemzes.manageles.MenBefMenegeles.class, springsbinvesting.domain.reszvenyelemzes.manageles.QMenBefMenegeles.class, PathInits.DIRECT2);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final springsbinvesting.domain.QReszveny managelesReszveny;

    public final SetPath<springsbinvesting.domain.reszvenyelemzes.manageles.ManagelesReszveny, springsbinvesting.domain.reszvenyelemzes.manageles.QManagelesReszveny> reszvenymanageles = this.<springsbinvesting.domain.reszvenyelemzes.manageles.ManagelesReszveny, springsbinvesting.domain.reszvenyelemzes.manageles.QManagelesReszveny>createSet("reszvenymanageles", springsbinvesting.domain.reszvenyelemzes.manageles.ManagelesReszveny.class, springsbinvesting.domain.reszvenyelemzes.manageles.QManagelesReszveny.class, PathInits.DIRECT2);

    public final SetPath<springsbinvesting.domain.reszvenyelemzes.manageles.ManagelesiStrategia, springsbinvesting.domain.reszvenyelemzes.manageles.QManagelesiStrategia> stratmanageles = this.<springsbinvesting.domain.reszvenyelemzes.manageles.ManagelesiStrategia, springsbinvesting.domain.reszvenyelemzes.manageles.QManagelesiStrategia>createSet("stratmanageles", springsbinvesting.domain.reszvenyelemzes.manageles.ManagelesiStrategia.class, springsbinvesting.domain.reszvenyelemzes.manageles.QManagelesiStrategia.class, PathInits.DIRECT2);

    public QManageles(String variable) {
        this(Manageles.class, forVariable(variable), INITS);
    }

    public QManageles(Path<? extends Manageles> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QManageles(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QManageles(PathMetadata metadata, PathInits inits) {
        this(Manageles.class, metadata, inits);
    }

    public QManageles(Class<? extends Manageles> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.managelesReszveny = inits.isInitialized("managelesReszveny") ? new springsbinvesting.domain.QReszveny(forProperty("managelesReszveny"), inits.get("managelesReszveny")) : null;
    }

}

