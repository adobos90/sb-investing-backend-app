package springsbinvesting.domain.reszvenyelemzes;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QVallalatKockElemzes is a Querydsl query type for VallalatKockElemzes
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QVallalatKockElemzes extends EntityPathBase<VallalatKockElemzes> {

    private static final long serialVersionUID = 2126204878L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QVallalatKockElemzes vallalatKockElemzes = new QVallalatKockElemzes("vallalatKockElemzes");

    public final StringPath beta_eredmeny = createString("beta_eredmeny");

    public final NumberPath<Integer> beta_riziko = createNumber("beta_riziko", Integer.class);

    public final StringPath ceg_info_eredmeny = createString("ceg_info_eredmeny");

    public final NumberPath<Integer> ceg_info_riziko = createNumber("ceg_info_riziko", Integer.class);

    public final StringPath egyeb_eredmeny = createString("egyeb_eredmeny");

    public final NumberPath<Integer> egyeb_riziko = createNumber("egyeb_riziko", Integer.class);

    public final StringPath elerheto_info_eredmeny = createString("elerheto_info_eredmeny");

    public final NumberPath<Integer> elerheto_info_riziko = createNumber("elerheto_info_riziko", Integer.class);

    public final StringPath eps_eredmeny = createString("eps_eredmeny");

    public final NumberPath<Integer> eps_riziko = createNumber("eps_riziko", Integer.class);

    public final StringPath fontos_tech_eredmeny = createString("fontos_tech_eredmeny");

    public final NumberPath<Integer> fontos_tech_riziko = createNumber("fontos_tech_riziko", Integer.class);

    public final StringPath fuzio_eredmeny = createString("fuzio_eredmeny");

    public final NumberPath<Integer> fuzio_riziko = createNumber("fuzio_riziko", Integer.class);

    public final StringPath hitel_besorolas_eredmeny = createString("hitel_besorolas_eredmeny");

    public final NumberPath<Integer> hitel_besorolas_riziko = createNumber("hitel_besorolas_riziko", Integer.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath immaterialis_eredmeny = createString("immaterialis_eredmeny");

    public final NumberPath<Integer> immaterialis_riziko = createNumber("immaterialis_riziko", Integer.class);

    public final StringPath konyv_eredmeny = createString("konyv_eredmeny");

    public final NumberPath<Integer> konyv_riziko = createNumber("konyv_riziko", Integer.class);

    public final StringPath kutatas_fejlesztes_eredmeny = createString("kutatas_fejlesztes_eredmeny");

    public final NumberPath<Integer> kutatas_fejlesztes_riziko = createNumber("kutatas_fejlesztes_riziko", Integer.class);

    public final StringPath osztalek_eredmeny = createString("osztalek_eredmeny");

    public final NumberPath<Integer> osztalek_riziko = createNumber("osztalek_riziko", Integer.class);

    public final StringPath piaci_kap_eredmeny = createString("piaci_kap_eredmeny");

    public final NumberPath<Integer> piaci_kap_riziko = createNumber("piaci_kap_riziko", Integer.class);

    public final StringPath reszveny_eredmeny = createString("reszveny_eredmeny");

    public final NumberPath<Integer> reszveny_riziko = createNumber("reszveny_riziko", Integer.class);

    public final StringPath szukseges_allomany_eredmeny = createString("szukseges_allomany_eredmeny");

    public final NumberPath<Integer> szukseges_allomany_riziko = createNumber("szukseges_allomany_riziko", Integer.class);

    public final StringPath termek_info_eredmeny = createString("termek_info_eredmeny");

    public final NumberPath<Integer> termek_info_riziko = createNumber("termek_info_riziko", Integer.class);

    public final StringPath tobbsegi_tulaj_eredmeny = createString("tobbsegi_tulaj_eredmeny");

    public final NumberPath<Integer> tobbsegi_tulaj_riziko = createNumber("tobbsegi_tulaj_riziko", Integer.class);

    public final StringPath uj_reszveny_ki_eredmeny = createString("uj_reszveny_ki_eredmeny");

    public final NumberPath<Integer> uj_reszveny_ki_riziko = createNumber("uj_reszveny_ki_riziko", Integer.class);

    public final StringPath vallalat_tozsde_eredmeny = createString("vallalat_tozsde_eredmeny");

    public final NumberPath<Integer> vallalat_tozsde_riziko = createNumber("vallalat_tozsde_riziko", Integer.class);

    public final springsbinvesting.domain.QReszveny vallalatKockElemzesReszveny;

    public final StringPath vez_me_eredmeny = createString("vez_me_eredmeny");

    public final NumberPath<Integer> vez_me_riziko = createNumber("vez_me_riziko", Integer.class);

    public final StringPath volt_csucson_eredmeny = createString("volt_csucson_eredmeny");

    public final NumberPath<Integer> volt_csucson_riziko = createNumber("volt_csucson_riziko", Integer.class);

    public QVallalatKockElemzes(String variable) {
        this(VallalatKockElemzes.class, forVariable(variable), INITS);
    }

    public QVallalatKockElemzes(Path<? extends VallalatKockElemzes> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QVallalatKockElemzes(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QVallalatKockElemzes(PathMetadata metadata, PathInits inits) {
        this(VallalatKockElemzes.class, metadata, inits);
    }

    public QVallalatKockElemzes(Class<? extends VallalatKockElemzes> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.vallalatKockElemzesReszveny = inits.isInitialized("vallalatKockElemzesReszveny") ? new springsbinvesting.domain.QReszveny(forProperty("vallalatKockElemzesReszveny"), inits.get("vallalatKockElemzesReszveny")) : null;
    }

}

