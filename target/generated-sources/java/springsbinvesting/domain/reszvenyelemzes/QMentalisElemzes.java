package springsbinvesting.domain.reszvenyelemzes;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QMentalisElemzes is a Querydsl query type for MentalisElemzes
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QMentalisElemzes extends EntityPathBase<MentalisElemzes> {

    private static final long serialVersionUID = -952624112L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QMentalisElemzes mentalisElemzes = new QMentalisElemzes("mentalisElemzes");

    public final StringPath ellensegesseg = createString("ellensegesseg");

    public final StringPath ellensegesseg_elv = createString("ellensegesseg_elv");

    public final StringPath felelem = createString("felelem");

    public final StringPath felelem_elv = createString("felelem_elv");

    public final StringPath fokozott = createString("fokozott");

    public final StringPath fokozott_elv = createString("fokozott_elv");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath indokolatlan = createString("indokolatlan");

    public final StringPath indokolatlan_elv = createString("indokolatlan_elv");

    public final StringPath introvertalt = createString("introvertalt");

    public final StringPath introvertalt_elv = createString("introvertalt_elv");

    public final StringPath kavargo = createString("kavargo");

    public final StringPath kavargo_elv = createString("kavargo_elv");

    public final StringPath kellemetlen = createString("kellemetlen");

    public final StringPath kellemetlen_elv = createString("kellemetlen_elv");

    public final StringPath kimerultseg = createString("kimerultseg");

    public final StringPath kimerultseg_elv = createString("kimerultseg_elv");

    public final springsbinvesting.domain.QReszveny mentalisReszveny;

    public final StringPath onbizalom = createString("onbizalom");

    public final StringPath onbizalom_elv = createString("onbizalom_elv");

    public final StringPath szomorusag = createString("szomorusag");

    public final StringPath szomorusag_elv = createString("szomorusag_elv");

    public QMentalisElemzes(String variable) {
        this(MentalisElemzes.class, forVariable(variable), INITS);
    }

    public QMentalisElemzes(Path<? extends MentalisElemzes> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QMentalisElemzes(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QMentalisElemzes(PathMetadata metadata, PathInits inits) {
        this(MentalisElemzes.class, metadata, inits);
    }

    public QMentalisElemzes(Class<? extends MentalisElemzes> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.mentalisReszveny = inits.isInitialized("mentalisReszveny") ? new springsbinvesting.domain.QReszveny(forProperty("mentalisReszveny"), inits.get("mentalisReszveny")) : null;
    }

}

