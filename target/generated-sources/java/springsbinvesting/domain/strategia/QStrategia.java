package springsbinvesting.domain.strategia;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QStrategia is a Querydsl query type for Strategia
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QStrategia extends EntityPathBase<Strategia> {

    private static final long serialVersionUID = 380710497L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QStrategia strategia = new QStrategia("strategia");

    public final StringPath datum = createString("datum");

    public final NumberPath<Integer> eredmeny = createNumber("eredmeny", Integer.class);

    public final StringPath flag = createString("flag");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Integer> pb = createNumber("pb", Integer.class);

    public final NumberPath<Integer> pe = createNumber("pe", Integer.class);

    public final springsbinvesting.domain.QUser strategiaUser;

    public final NumberPath<Integer> teljesitmeny = createNumber("teljesitmeny", Integer.class);

    public QStrategia(String variable) {
        this(Strategia.class, forVariable(variable), INITS);
    }

    public QStrategia(Path<? extends Strategia> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QStrategia(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QStrategia(PathMetadata metadata, PathInits inits) {
        this(Strategia.class, metadata, inits);
    }

    public QStrategia(Class<? extends Strategia> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.strategiaUser = inits.isInitialized("strategiaUser") ? new springsbinvesting.domain.QUser(forProperty("strategiaUser")) : null;
    }

}

