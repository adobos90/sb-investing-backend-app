package springsbinvesting.domain.strategia;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QStrategiaReszveny is a Querydsl query type for StrategiaReszveny
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QStrategiaReszveny extends EntityPathBase<StrategiaReszveny> {

    private static final long serialVersionUID = 1068674325L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QStrategiaReszveny strategiaReszveny = new QStrategiaReszveny("strategiaReszveny");

    public final StringPath agazat = createString("agazat");

    public final NumberPath<Integer> eps = createNumber("eps", Integer.class);

    public final NumberPath<Integer> hozam = createNumber("hozam", Integer.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath ipar = createString("ipar");

    public final StringPath kapitalizacio = createString("kapitalizacio");

    public final StringPath orszag = createString("orszag");

    public final NumberPath<Integer> ready = createNumber("ready", Integer.class);

    public final NumberPath<Integer> reszveny = createNumber("reszveny", Integer.class);

    public final springsbinvesting.domain.QUser stReszvenyUser;

    public final StringPath ticker = createString("ticker");

    public final StringPath vallalat = createString("vallalat");

    public QStrategiaReszveny(String variable) {
        this(StrategiaReszveny.class, forVariable(variable), INITS);
    }

    public QStrategiaReszveny(Path<? extends StrategiaReszveny> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QStrategiaReszveny(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QStrategiaReszveny(PathMetadata metadata, PathInits inits) {
        this(StrategiaReszveny.class, metadata, inits);
    }

    public QStrategiaReszveny(Class<? extends StrategiaReszveny> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.stReszvenyUser = inits.isInitialized("stReszvenyUser") ? new springsbinvesting.domain.QUser(forProperty("stReszvenyUser")) : null;
    }

}

