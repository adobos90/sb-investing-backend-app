package springsbinvesting.domain.penzugy;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QBevetelPenzugy is a Querydsl query type for BevetelPenzugy
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QBevetelPenzugy extends EntityPathBase<BevetelPenzugy> {

    private static final long serialVersionUID = 1086251922L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QBevetelPenzugy bevetelPenzugy = new QBevetelPenzugy("bevetelPenzugy");

    public final NumberPath<Long> bevetel = createNumber("bevetel", Long.class);

    public final QHonap bevetelHonap;

    public final StringPath ev = createString("ev");

    public final NumberPath<Integer> honap = createNumber("honap", Integer.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public QBevetelPenzugy(String variable) {
        this(BevetelPenzugy.class, forVariable(variable), INITS);
    }

    public QBevetelPenzugy(Path<? extends BevetelPenzugy> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QBevetelPenzugy(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QBevetelPenzugy(PathMetadata metadata, PathInits inits) {
        this(BevetelPenzugy.class, metadata, inits);
    }

    public QBevetelPenzugy(Class<? extends BevetelPenzugy> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.bevetelHonap = inits.isInitialized("bevetelHonap") ? new QHonap(forProperty("bevetelHonap"), inits.get("bevetelHonap")) : null;
    }

}

