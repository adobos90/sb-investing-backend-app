package springsbinvesting.domain.penzugy;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QHonap is a Querydsl query type for Honap
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QHonap extends EntityPathBase<Honap> {

    private static final long serialVersionUID = -1532950367L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QHonap honap1 = new QHonap("honap1");

    public final SetPath<BevetelPenzugy, QBevetelPenzugy> bevetelHonap = this.<BevetelPenzugy, QBevetelPenzugy>createSet("bevetelHonap", BevetelPenzugy.class, QBevetelPenzugy.class, PathInits.DIRECT2);

    public final NumberPath<Integer> honap = createNumber("honap", Integer.class);

    public final QPenzugy honapPenzugy;

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final SetPath<KiadasPenzugy, QKiadasPenzugy> kiadHonap = this.<KiadasPenzugy, QKiadasPenzugy>createSet("kiadHonap", KiadasPenzugy.class, QKiadasPenzugy.class, PathInits.DIRECT2);

    public final NumberPath<Integer> zarva = createNumber("zarva", Integer.class);

    public QHonap(String variable) {
        this(Honap.class, forVariable(variable), INITS);
    }

    public QHonap(Path<? extends Honap> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QHonap(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QHonap(PathMetadata metadata, PathInits inits) {
        this(Honap.class, metadata, inits);
    }

    public QHonap(Class<? extends Honap> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.honapPenzugy = inits.isInitialized("honapPenzugy") ? new QPenzugy(forProperty("honapPenzugy"), inits.get("honapPenzugy")) : null;
    }

}

