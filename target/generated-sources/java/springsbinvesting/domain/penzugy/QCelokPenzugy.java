package springsbinvesting.domain.penzugy;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QCelokPenzugy is a Querydsl query type for CelokPenzugy
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QCelokPenzugy extends EntityPathBase<CelokPenzugy> {

    private static final long serialVersionUID = -516647115L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QCelokPenzugy celokPenzugy = new QCelokPenzugy("celokPenzugy");

    public final springsbinvesting.domain.QUser celokPenzugyUser;

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Integer> idotartam = createNumber("idotartam", Integer.class);

    public final StringPath kezdet = createString("kezdet");

    public final StringPath megnevezes = createString("megnevezes");

    public final NumberPath<Long> osszeg = createNumber("osszeg", Long.class);

    public QCelokPenzugy(String variable) {
        this(CelokPenzugy.class, forVariable(variable), INITS);
    }

    public QCelokPenzugy(Path<? extends CelokPenzugy> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QCelokPenzugy(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QCelokPenzugy(PathMetadata metadata, PathInits inits) {
        this(CelokPenzugy.class, metadata, inits);
    }

    public QCelokPenzugy(Class<? extends CelokPenzugy> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.celokPenzugyUser = inits.isInitialized("celokPenzugyUser") ? new springsbinvesting.domain.QUser(forProperty("celokPenzugyUser")) : null;
    }

}

