package springsbinvesting.domain.penzugy;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QPenzugy is a Querydsl query type for Penzugy
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QPenzugy extends EntityPathBase<Penzugy> {

    private static final long serialVersionUID = -1766963919L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QPenzugy penzugy = new QPenzugy("penzugy");

    public final StringPath ev = createString("ev");

    public final SetPath<Honap, QHonap> honapPenzugy = this.<Honap, QHonap>createSet("honapPenzugy", Honap.class, QHonap.class, PathInits.DIRECT2);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final springsbinvesting.domain.QUser penzugyUser;

    public QPenzugy(String variable) {
        this(Penzugy.class, forVariable(variable), INITS);
    }

    public QPenzugy(Path<? extends Penzugy> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QPenzugy(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QPenzugy(PathMetadata metadata, PathInits inits) {
        this(Penzugy.class, metadata, inits);
    }

    public QPenzugy(Class<? extends Penzugy> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.penzugyUser = inits.isInitialized("penzugyUser") ? new springsbinvesting.domain.QUser(forProperty("penzugyUser")) : null;
    }

}

