package springsbinvesting.domain.penzugy;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QKiadasPenzugy is a Querydsl query type for KiadasPenzugy
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QKiadasPenzugy extends EntityPathBase<KiadasPenzugy> {

    private static final long serialVersionUID = -176603266L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QKiadasPenzugy kiadasPenzugy = new QKiadasPenzugy("kiadasPenzugy");

    public final NumberPath<Long> egyeb = createNumber("egyeb", Long.class);

    public final NumberPath<Long> elvezeti = createNumber("elvezeti", Long.class);

    public final StringPath ev = createString("ev");

    public final NumberPath<Long> fogy = createNumber("fogy", Long.class);

    public final NumberPath<Integer> ho = createNumber("ho", Integer.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final QHonap kiadHonap;

    public final NumberPath<Long> megtakaritas = createNumber("megtakaritas", Long.class);

    public final NumberPath<Integer> nap = createNumber("nap", Integer.class);

    public final NumberPath<Long> rezsi = createNumber("rezsi", Long.class);

    public final NumberPath<Long> ruhazkodas = createNumber("ruhazkodas", Long.class);

    public final NumberPath<Long> uti = createNumber("uti", Long.class);

    public QKiadasPenzugy(String variable) {
        this(KiadasPenzugy.class, forVariable(variable), INITS);
    }

    public QKiadasPenzugy(Path<? extends KiadasPenzugy> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QKiadasPenzugy(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QKiadasPenzugy(PathMetadata metadata, PathInits inits) {
        this(KiadasPenzugy.class, metadata, inits);
    }

    public QKiadasPenzugy(Class<? extends KiadasPenzugy> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.kiadHonap = inits.isInitialized("kiadHonap") ? new QHonap(forProperty("kiadHonap"), inits.get("kiadHonap")) : null;
    }

}

