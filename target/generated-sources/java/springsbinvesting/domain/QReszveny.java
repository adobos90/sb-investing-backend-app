package springsbinvesting.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.processing.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QReszveny is a Querydsl query type for Reszveny
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QReszveny extends EntityPathBase<Reszveny> {

    private static final long serialVersionUID = 496188385L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QReszveny reszveny = new QReszveny("reszveny");

    public final StringPath agazat = createString("agazat");

    public final SetPath<springsbinvesting.domain.reszvenyelemzes.Celar, springsbinvesting.domain.reszvenyelemzes.QCelar> celar = this.<springsbinvesting.domain.reszvenyelemzes.Celar, springsbinvesting.domain.reszvenyelemzes.QCelar>createSet("celar", springsbinvesting.domain.reszvenyelemzes.Celar.class, springsbinvesting.domain.reszvenyelemzes.QCelar.class, PathInits.DIRECT2);

    public final DateTimePath<java.util.Date> datum = createDateTime("datum", java.util.Date.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Integer> kitolt = createNumber("kitolt", Integer.class);

    public final SetPath<springsbinvesting.domain.reszvenyelemzes.Manageles, springsbinvesting.domain.reszvenyelemzes.QManageles> managelesReszveny = this.<springsbinvesting.domain.reszvenyelemzes.Manageles, springsbinvesting.domain.reszvenyelemzes.QManageles>createSet("managelesReszveny", springsbinvesting.domain.reszvenyelemzes.Manageles.class, springsbinvesting.domain.reszvenyelemzes.QManageles.class, PathInits.DIRECT2);

    public final SetPath<springsbinvesting.domain.reszvenyelemzes.MentalisElemzes, springsbinvesting.domain.reszvenyelemzes.QMentalisElemzes> mentalisElemzes = this.<springsbinvesting.domain.reszvenyelemzes.MentalisElemzes, springsbinvesting.domain.reszvenyelemzes.QMentalisElemzes>createSet("mentalisElemzes", springsbinvesting.domain.reszvenyelemzes.MentalisElemzes.class, springsbinvesting.domain.reszvenyelemzes.QMentalisElemzes.class, PathInits.DIRECT2);

    public final SetPath<springsbinvesting.domain.reszvenyelemzes.NettoJelenErtek, springsbinvesting.domain.reszvenyelemzes.QNettoJelenErtek> nettoJelenertek = this.<springsbinvesting.domain.reszvenyelemzes.NettoJelenErtek, springsbinvesting.domain.reszvenyelemzes.QNettoJelenErtek>createSet("nettoJelenertek", springsbinvesting.domain.reszvenyelemzes.NettoJelenErtek.class, springsbinvesting.domain.reszvenyelemzes.QNettoJelenErtek.class, PathInits.DIRECT2);

    public final SetPath<springsbinvesting.domain.reszvenyelemzes.PenzugyiAdatok, springsbinvesting.domain.reszvenyelemzes.QPenzugyiAdatok> penzAdatokReszveny = this.<springsbinvesting.domain.reszvenyelemzes.PenzugyiAdatok, springsbinvesting.domain.reszvenyelemzes.QPenzugyiAdatok>createSet("penzAdatokReszveny", springsbinvesting.domain.reszvenyelemzes.PenzugyiAdatok.class, springsbinvesting.domain.reszvenyelemzes.QPenzugyiAdatok.class, PathInits.DIRECT2);

    public final SetPath<springsbinvesting.domain.reszvenyelemzes.VallalatPenzugyiElemzes, springsbinvesting.domain.reszvenyelemzes.QVallalatPenzugyiElemzes> penzugyReszveny = this.<springsbinvesting.domain.reszvenyelemzes.VallalatPenzugyiElemzes, springsbinvesting.domain.reszvenyelemzes.QVallalatPenzugyiElemzes>createSet("penzugyReszveny", springsbinvesting.domain.reszvenyelemzes.VallalatPenzugyiElemzes.class, springsbinvesting.domain.reszvenyelemzes.QVallalatPenzugyiElemzes.class, PathInits.DIRECT2);

    public final QUser reszvenyUser;

    public final StringPath status = createString("status");

    public final StringPath strategia = createString("strategia");

    public final StringPath ticker = createString("ticker");

    public final StringPath timestamp = createString("timestamp");

    public final StringPath vallalat = createString("vallalat");

    public final SetPath<springsbinvesting.domain.reszvenyelemzes.VallalatKockElemzes, springsbinvesting.domain.reszvenyelemzes.QVallalatKockElemzes> vallalatKockElemzes = this.<springsbinvesting.domain.reszvenyelemzes.VallalatKockElemzes, springsbinvesting.domain.reszvenyelemzes.QVallalatKockElemzes>createSet("vallalatKockElemzes", springsbinvesting.domain.reszvenyelemzes.VallalatKockElemzes.class, springsbinvesting.domain.reszvenyelemzes.QVallalatKockElemzes.class, PathInits.DIRECT2);

    public QReszveny(String variable) {
        this(Reszveny.class, forVariable(variable), INITS);
    }

    public QReszveny(Path<? extends Reszveny> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QReszveny(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QReszveny(PathMetadata metadata, PathInits inits) {
        this(Reszveny.class, metadata, inits);
    }

    public QReszveny(Class<? extends Reszveny> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.reszvenyUser = inits.isInitialized("reszvenyUser") ? new QUser(forProperty("reszvenyUser")) : null;
    }

}

