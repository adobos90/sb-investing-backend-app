create table user(
id int primary key auto_increment not null,
firstname varchar(100) not null,
lastname varchar(100) not null,
email varchar(100) not null,
passw varchar(255) not null,
birthdate date not null
);