package springsbinvesting.model.repositories.api;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import springsbinvesting.domain.User;
import springsbinvesting.domain.celkituzes.Celkituzes;

public interface ICelkituzesRepository {
   Long findByUserId(Long id);
   
   Celkituzes findById(Long userId);
}
