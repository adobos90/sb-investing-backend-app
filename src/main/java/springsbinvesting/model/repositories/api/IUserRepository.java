package springsbinvesting.model.repositories.api;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import springsbinvesting.domain.Reszveny;
import springsbinvesting.domain.User;
import springsbinvesting.domain.celkituzes.Akadaly;
import springsbinvesting.domain.celkituzes.Celkituzes;
import springsbinvesting.domain.celkituzes.Feladat;
import springsbinvesting.domain.reszvenyelemzes.Celar;
import springsbinvesting.domain.reszvenyelemzes.Manageles;
import springsbinvesting.domain.reszvenyelemzes.MentalisElemzes;
import springsbinvesting.domain.reszvenyelemzes.NettoJelenErtek;
import springsbinvesting.domain.reszvenyelemzes.PenzugyiAdatok;
import springsbinvesting.domain.reszvenyelemzes.VallalatKockElemzes;
import springsbinvesting.domain.reszvenyelemzes.VallalatPenzugyiElemzes;
import springsbinvesting.domain.reszvenyelemzes.manageles.ManagelesBefAdatok;
import springsbinvesting.domain.reszvenyelemzes.manageles.ManagelesReszveny;
import springsbinvesting.domain.reszvenyelemzes.manageles.ManagelesiStrategia;
import springsbinvesting.domain.reszvenyelemzes.manageles.MenBefMenegeles;
import springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.PenzugyiElemzesHatekonysag;
import springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.PenzugyiElemzesHitel;
import springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.PenzugyiElemzesJovedelem;
import springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.PenzugyiElemzesPiaci;

public interface IUserRepository {

	@Query(value = "select *  from user inner join reszveny on reszveny.user_id=user.id inner join mentalis on mentalis.reszveny_id=reszveny.id", nativeQuery = true)
	public List<User> findAllUserWithReszvenyek();

	public User findUserWithReszvenyekByEmail(String email);

	public User findUserWithReszvenyekById(Long id);

	public void addUser(User user);

	public User updateUser(Long userId, User user);

	public Long getReszvenyId(Long userId);


	// **************** RÉSZVÉNYEK  **************
	// *******************************************


	// **************  GET REQUESTS ***************

	public List<Reszveny> getReszvenyek(Long userId);

	public Reszveny getReszvenyById(Long reszvenyId);

	public MentalisElemzes getMentalisByReszvenyId(Long reszvenyId);

	public VallalatKockElemzes getVallKockElemzes(Long reszvenyId);

	public PenzugyiAdatok getPenzugyiAdatok(Long reszvenyId);

	public VallalatPenzugyiElemzes getVallPenzElemzes(Long reszvenyId);

	public Celar getCelar(Long reszvenyId);

	public NettoJelenErtek getNettoJelenertek(Long reszvenyId);

	public Manageles getManageles(Long reszvenyId);


	// **************** POST REQUESTS ***************

	public Reszveny addReszveny(Long id, Reszveny reszveny);

	public void addMentalisElemzes(Long reszvenyId, MentalisElemzes mentalis);

	public void addVallalatKockElemzes(Long reszvenyId, VallalatKockElemzes vallKockElemzes);

	public void addCelar(Long reszvenyId, Celar celar);

	public void addNettoJelenErtek(Long reszvenyId, NettoJelenErtek netto);

	public void addManageles(Long reszvenyId, Manageles manageles);

	public void addBefAdatokToManageles(Long managelesId, ManagelesBefAdatok menBefadatok);

	public void addReszvenyToManageles(Long managelesId, ManagelesReszveny menReszveny);

	public void addStrategiaToManageles(Long managelesId, ManagelesiStrategia menStrat);

	public void addMenBefMenToManageles(Long managelesId, MenBefMenegeles menBefMen);

	public void addPenzugyiElemzes(Long reszvenyId, VallalatPenzugyiElemzes penzugyiElemzes);

	public void addJovToPenzugyiElemzes(Long penzugyId, PenzugyiElemzesJovedelem jovPenzugyElemzes);

	public void addHatToPenzugyiElemzes(Long penzugyId, PenzugyiElemzesHatekonysag hatPenzugyElemzes);

	public void addHitelToPenzugyiElemzes(Long penzugyId, PenzugyiElemzesHitel hitelPenzugyElemzes);

	public void addPiaciToPenzugyiElemzes(Long penzugyId, PenzugyiElemzesPiaci piaciPenzugyElemzes);

	public void addPenzugyiAdatok(Long reszvenyId, PenzugyiAdatok penzugyiAdatok);




	// **************** PUT REQUESTS ***************

	public Reszveny updateReszveny(Long reszvenyId, Reszveny newReszveny);

	public MentalisElemzes updateMentalisElemzes(Long reszvenyId, MentalisElemzes mentalisElemzes);

	public VallalatKockElemzes updateVallKockElemzes(Long reszvenyId, VallalatKockElemzes kockElemzes);

	public PenzugyiAdatok updatePenzugyiAdatok(Long reszvenyId, PenzugyiAdatok penzugyiAdatok);

	public VallalatPenzugyiElemzes updatePenzugyielemzes(Long reszvenyId, VallalatPenzugyiElemzes vallalatPenzugyiElemzes);

	public void updateJovToPenzugyiElemzes(Long penzelemzesId, PenzugyiElemzesJovedelem jovPenzugyElemzes);

	public void updateHitelToPenzugyiElemzes(Long penzelemzesId, PenzugyiElemzesHitel hitelPenzugyElemzes);

	public void updateHatToPenzugyiElemzes(Long penzelemzesId, PenzugyiElemzesHatekonysag hatPenzugyElemzes);

	public void updatePiaciToPenzugyiElemzes(Long penzelemzesId, PenzugyiElemzesPiaci piaciPenzugyElemzes);

	public Celar updateCelar(Long reszvenyId, Celar celar);

	public NettoJelenErtek updateNettoJelenertek(Long reszvenyId, NettoJelenErtek nettoJelenErtek);

	public ManagelesBefAdatok updateManagelesAdatok(Long managelesId, ManagelesBefAdatok managelesBefAdatok);

	public ManagelesReszveny updateManagelesReszveny(Long managelesId, ManagelesReszveny managelesReszveny);

	public ManagelesiStrategia updateManagelesStrat(Long managelesId, ManagelesiStrategia managelesiStrategia);

	public MenBefMenegeles updateBefMenManageles(Long managelesId,   Long id, MenBefMenegeles menBefMenegeles);


	// ************* DELETE REQUESTS ***************

	public void deleteReszveny(Long id);

	public void deleteMenBefMen(Long id, Long managelesId);


	// ************** CÉLKITŰZÉS *************************
	// ***************************************************
	public void addCelkituzes(Long id, Celkituzes celkituzes);
	
	public void addFeladatToCelkituzes(Long celkituzesId, Feladat feladat);
	
	public void addAkadalyToCelkituzes(Long celkituzesId, Akadaly akadaly);
	
	public void updateCelkituzes(Long userId, Celkituzes celkituzes);
	
	public void updateFeladat(Long celkituzesId, Feladat feladat);
	
	public void deleteCelkituzes(Long userId);
	
	public List<Feladat> getFeladatok(Long celkituzesId);
	
	public int deleteFeladat(Long feladatId);
	
	
	
	

}
