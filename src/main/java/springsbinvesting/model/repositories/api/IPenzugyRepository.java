package springsbinvesting.model.repositories.api;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import org.springframework.web.bind.annotation.PathVariable;
import springsbinvesting.domain.penzugy.*;

public interface IPenzugyRepository {
	
	List<Penzugy> findAllPenzugy(String userId);
	
	Long findPenzugyIdByEv(String ev);
	
	
	Long findBevKiadIbByPenzugyId(Long penzugyId, String honap);
	
	Long addPenzugy(Long userId,Penzugy penzugy);

	void addHonap(Long penzugyId, Honap honap);

	public List<Honap> findHonapByPenzugyiEv(String penzugyiEv);
	
	void addBevetelekToHonap(Long honapId, BevetelPenzugy bevKiadasPenzugy);
	
	void addKiadasokToHonap(Long honapId, KiadasPenzugy kiadasPenzugy);
	
	public void deleteBevetelInPenzugy(Long honapId);
	
	public void deleteKiadasInPenzugy(Long honapId, int nap);
	
	public void updateBevetelInPenzugy(Long honapId, Long bevetel);

	public void updateHonapZarasa(Long yearId, int honap,  int zarva);

	public BevetelPenzugy findBevetelekByHonapId(String year, String honap);

	public List<KiadasPenzugy> findKiadasokByYearsAndMonths(String year, String honap);

	public KiadasPenzugy findKiadasokByHonapIdandNap(Long honapId, int nap);

	public List<BevetelPenzugy> getOsszesBevetel();

	public List<KiadasPenzugy> getOsszesKiadas();

	// GET cél by pénzügy
	public CelokPenzugy findCelokPenzugyById(Long userId);

	// ADD cél by pénzügy
	public void addCelToPenzugy(Long userId, CelokPenzugy celokPenzugy);

	// DELETE cél by pénzügy
	public void deleteCelokPenzugy(Long userId);

}
