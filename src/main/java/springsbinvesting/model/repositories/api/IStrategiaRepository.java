package springsbinvesting.model.repositories.api;

import springsbinvesting.domain.strategia.Strategia;
import springsbinvesting.domain.strategia.StrategiaReszveny;

import java.util.List;

public interface IStrategiaRepository {

    public void addStrategiaItem(Long userId, Strategia strategia);

    public List<Strategia> findAllStrategiaItemByFlag(Long userId, String flag);

    public void addStrategiaReszveny(Long userId, StrategiaReszveny strategiaReszveny);

    public List<StrategiaReszveny> getAllStratReszveny(Long userId);
}
