package springsbinvesting.model.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.jpa.impl.JPAQuery;

import springsbinvesting.domain.QReszveny;
import springsbinvesting.domain.QUser;
import springsbinvesting.domain.User;
import springsbinvesting.domain.penzugy.*;
import springsbinvesting.domain.reszvenyelemzes.QMentalisElemzes;
import springsbinvesting.model.repositories.api.IPenzugyRepository;


@Repository
public class PenzugyRepositoryImpl implements IPenzugyRepository {
	
	@PersistenceContext
	public EntityManager entityManager;
	

	@Override
	public List<Penzugy> findAllPenzugy(String userId) {
		
		String query = "SELECT * FROM `penzugy` WHERE user_id=?";

		return entityManager.createNativeQuery(query)
				.setParameter(1, userId).getResultList();
		
	}

	@Override
	@Transactional
	public Long addPenzugy(Long id,Penzugy penzugy) {
		String postPenzugyQuery = "INSERT INTO penzugy (ev, user_id) VALUES (?,?)";
		entityManager.createNativeQuery(postPenzugyQuery)
		.setParameter(1, penzugy.getEv())
		.setParameter(2, id).executeUpdate();
		
		return penzugy.getId();
	}

	@Override
	public Long findPenzugyIdByEv(String ev) {
		final JPAQuery<Penzugy> jpaQuery = new JPAQuery<Penzugy>(entityManager);
		QPenzugy penzugy = QPenzugy.penzugy;
		
		return jpaQuery.from(penzugy).select(penzugy.id).where(penzugy.ev.eq(ev)).fetchOne();

	}

	@Override
	public Long findBevKiadIbByPenzugyId(Long penzugyId, String honap) {
		return null;
	}

	/*@Override
	public Long findBevKiadIbByPenzugyId(Long penzugyId, String honap) {
		final JPAQuery<BevetelKiadasPenzugy> jpaQuery = new JPAQuery<BevetelKiadasPenzugy>(entityManager);
		
		QPenzugy penzugy = QPenzugy.penzugy;
		QBevetelKiadasPenzugy bevKiad = QBevetelKiadasPenzugy.bevetelKiadasPenzugy;
		
		return jpaQuery.from(bevKiad)
				.innerJoin(bevKiad.bevkiadPenzugy, penzugy)
				.select(bevKiad.id)
				.where(bevKiad.honap.eq(honap).and(penzugy.id.eq(penzugyId))).fetchFirst();
	}*/

	@Override
	@Transactional
	public void deleteBevetelInPenzugy(Long honapid) {
		
		String deleteBevetelQuery = "DELETE FROM bevetelpenzugy where honapid=?";
		entityManager.createNativeQuery(deleteBevetelQuery)
			.setParameter(1, honapid)
			.executeUpdate();
		
	}

	@Override
	@Transactional
	public void deleteKiadasInPenzugy(Long honapId, int nap) {
		String deleteBevetelQuery = "DELETE FROM kiadaspenzugy where honapId=? and nap=?";
		entityManager.createNativeQuery(deleteBevetelQuery)
			.setParameter(1, honapId)
				.setParameter(2, nap)
				.executeUpdate();
	}

	@Override
	@Transactional
	public void updateBevetelInPenzugy(Long honapId, Long bevetel) {
		String updateBevetelQuery = "UPDATE bevetelpenzugy set bevetel=? where honapid=?";
		
		entityManager.createNativeQuery(updateBevetelQuery)
		.setParameter(1, bevetel)
		.setParameter(2, honapId)
		.executeUpdate();
		
	}

	@Override
	@Transactional
	public void updateHonapZarasa(Long yearId, int honap, int zarva) {
		String updateHoZarasQuery = "UPDATE honappenzugy set zarva=? where penzugyid=? and honap=?";

		entityManager.createNativeQuery(updateHoZarasQuery)
				.setParameter(1, zarva)
				.setParameter(2, yearId)
				.setParameter(3, honap)
				.executeUpdate();
	}


	@Override
	@Transactional
	public void addHonap(Long penzugyId, Honap honap) {
		String postHonapToPenzugyQuery = "INSERT INTO honappenzugy(honap, zarva, penzugyid) values(?,?,?)";
		entityManager.createNativeQuery(postHonapToPenzugyQuery)
		.setParameter(1, honap.getHonap())
		.setParameter(2, honap.getZarva())
		.setParameter(3, penzugyId).executeUpdate();
		
	}

	@Override
	public List<Honap> findHonapByPenzugyiEv(String penzugyiEv) {
		final JPAQuery<Honap> jpaQuery = new JPAQuery<Honap>(entityManager);
		QHonap ho = QHonap.honap1;
		QPenzugy penzugy = QPenzugy.penzugy;
		
		return jpaQuery.select(ho).from(ho).innerJoin(ho.honapPenzugy,penzugy).where(penzugy.ev.eq(penzugyiEv)).fetch();
	
	}

	@Override
	@Transactional
	public void addBevetelekToHonap(Long honapId, BevetelPenzugy bevKiadasPenzugy) {
		String postBevetelekToHonapQuery = "INSERT INTO bevetelpenzugy (bevetel, ev, ho,  honapid) VALUES(?,?, ?, ?)";
		entityManager.createNativeQuery(postBevetelekToHonapQuery)
				.setParameter(1, bevKiadasPenzugy.getBevetel())
				.setParameter(2, bevKiadasPenzugy.getEv())
				.setParameter(3, bevKiadasPenzugy.getHonap())
				.setParameter(4, honapId).executeUpdate();
		
	}

	@Override
	@Transactional
	public void addKiadasokToHonap(Long honapId, KiadasPenzugy kiadasPenzugy) {
		String postKaidasToBevPenzugyQuery = "INSERT INTO kiadaspenzugy (nap, rezsi, megtakaritas, elvezeti, uti, fogy, ruhazkodas, egyeb, ev, ho,  honapid) VALUES(?,?,?,?,?,?,?,?,?,?,?)";
		entityManager.createNativeQuery(postKaidasToBevPenzugyQuery)
				.setParameter(1, kiadasPenzugy.getNap())
				.setParameter(2, kiadasPenzugy.getRezsi())
				.setParameter(3, kiadasPenzugy.getMegtakaritas())
				.setParameter(4, kiadasPenzugy.getElvezeti())
				.setParameter(5, kiadasPenzugy.getUti())
				.setParameter(6, kiadasPenzugy.getFogy())
				.setParameter(7, kiadasPenzugy.getRuhazkodas())
				.setParameter(8, kiadasPenzugy.getEgyeb())
				.setParameter(9, kiadasPenzugy.getEv())
				.setParameter(10, kiadasPenzugy.getHo())
				.setParameter(11, honapId)
				.executeUpdate();

		System.out.println(postKaidasToBevPenzugyQuery);

	}


	@Override
	public BevetelPenzugy findBevetelekByHonapId(String year, String honap) {
		final JPAQuery<BevetelPenzugy> jpaQuery = new JPAQuery<BevetelPenzugy>(entityManager);

		QBevetelPenzugy bevetelPenzugy = QBevetelPenzugy.bevetelPenzugy;
		QPenzugy penzugy = QPenzugy.penzugy;
		QHonap ho = QHonap.honap1;

	return  jpaQuery.from(bevetelPenzugy).select(bevetelPenzugy)
			.innerJoin (bevetelPenzugy.bevetelHonap,ho)
			.innerJoin( ho.honapPenzugy, penzugy)
			.where(penzugy.ev.eq(year).and(ho.honap.eq(Integer.parseInt(honap)))).fetchOne();
	}

	@Override
	public List<KiadasPenzugy> findKiadasokByYearsAndMonths(String year, String honap) {
		final JPAQuery<KiadasPenzugy> jpaQuery = new JPAQuery<KiadasPenzugy>(entityManager);

		QKiadasPenzugy kiadasPenzugy = QKiadasPenzugy.kiadasPenzugy;
		QPenzugy penzugy = QPenzugy.penzugy;
		QHonap ho = QHonap.honap1;

		return  jpaQuery.from(kiadasPenzugy).select(kiadasPenzugy)
				.innerJoin (kiadasPenzugy.kiadHonap,ho)
				.innerJoin( ho.honapPenzugy, penzugy)
				.where(penzugy.ev.eq(year).and(ho.honap.eq(Integer.parseInt(honap)))).fetch();
	}

	@Override
	public KiadasPenzugy findKiadasokByHonapIdandNap(Long honapId, int nap) {

		final JPAQuery<BevetelPenzugy> jpaQuery = new JPAQuery<BevetelPenzugy>(entityManager);

		QKiadasPenzugy kiadasPenzugy = QKiadasPenzugy.kiadasPenzugy;
		QHonap ho = QHonap.honap1;

		return jpaQuery.from(kiadasPenzugy).select(kiadasPenzugy)
				.innerJoin(kiadasPenzugy.kiadHonap, ho)
				.where(kiadasPenzugy.nap.eq(nap).and(ho.id.eq(honapId))).fetchOne();

	}

	@Override
	public List<BevetelPenzugy> getOsszesBevetel() {
		final JPAQuery<BevetelPenzugy> jpaQuery = new JPAQuery<BevetelPenzugy>(entityManager);

		QBevetelPenzugy bevetelPenzugy = QBevetelPenzugy.bevetelPenzugy;
		QHonap ho = QHonap.honap1;

		return jpaQuery.from(bevetelPenzugy).select(bevetelPenzugy).fetch();
	}

	@Override
	public List<KiadasPenzugy> getOsszesKiadas() {
		final JPAQuery<BevetelPenzugy> jpaQuery = new JPAQuery<BevetelPenzugy>(entityManager);

		QKiadasPenzugy kiadasPenzugy = QKiadasPenzugy.kiadasPenzugy;
		QHonap ho = QHonap.honap1;

		return jpaQuery.from(kiadasPenzugy).select(kiadasPenzugy).fetch();
	}

	@Override
	public CelokPenzugy findCelokPenzugyById(Long userId) {

	QCelokPenzugy celokPenzugy = QCelokPenzugy.celokPenzugy;

		final JPAQuery<CelokPenzugy> jpaQuery = new JPAQuery<CelokPenzugy>(entityManager);

		return jpaQuery.from(celokPenzugy).select(celokPenzugy)
				.where(celokPenzugy.celokPenzugyUser.id.eq(userId))
				.fetchOne();

	}

	@Override
	@Transactional
	public void addCelToPenzugy(Long userId, CelokPenzugy celokPenzugy) {
		String postPenzugyQuery = "INSERT INTO celokpenzugy (megnevezes, osszeg, idotartam, kezdet, user_id) VALUES (?,?,?,?,?)";
		entityManager.createNativeQuery(postPenzugyQuery)
				.setParameter(1, celokPenzugy.getMegnevezes())
				.setParameter(2, celokPenzugy.getOsszeg())
				.setParameter(3, celokPenzugy.getIdotartam())
				.setParameter(4, celokPenzugy.getKezdet())
				.setParameter(5,  userId)
				.executeUpdate();
	}

	@Override
	@Transactional
	public void deleteCelokPenzugy(Long userId) {
		String deleteQuery = "DELETE FROM celokpenzugy WHERE user_id=?";

		entityManager.createNativeQuery(deleteQuery)
				.setParameter(1, userId)
				.executeUpdate();
	}


}
