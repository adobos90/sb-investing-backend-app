package springsbinvesting.model.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.querydsl.jpa.impl.JPAQuery;

import springsbinvesting.domain.QUser;
import springsbinvesting.domain.User;
import springsbinvesting.domain.celkituzes.Celkituzes;
import springsbinvesting.domain.celkituzes.QCelkituzes;
import springsbinvesting.model.repositories.api.ICelkituzesRepository;

@Repository
public class CelkituzesRepositoryImpl implements ICelkituzesRepository {

	@PersistenceContext
	public EntityManager entityManager;

	@Override
	public Long findByUserId(Long id) {
		final JPAQuery<Celkituzes> jpaQuery = new JPAQuery<Celkituzes>(entityManager);
		QCelkituzes celkituzes = QCelkituzes.celkituzes;
		
		return jpaQuery.from(celkituzes).select(celkituzes.id).where(celkituzes.celkituzesUser.id.eq(id)).fetchOne();
	}

	@Override
	public Celkituzes findById(Long userId) {
		final JPAQuery<Celkituzes> jpaQuery = new JPAQuery<Celkituzes>(entityManager);
		QCelkituzes celkituzes = QCelkituzes.celkituzes;
		
		return jpaQuery.from(celkituzes).select(celkituzes).where(celkituzes.celkituzesUser.id.eq(userId)).fetchOne();
	}
	
	
	
}
