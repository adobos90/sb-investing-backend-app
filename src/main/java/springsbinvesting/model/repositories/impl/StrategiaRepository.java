package springsbinvesting.model.repositories.impl;

import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import springsbinvesting.domain.QUser;
import springsbinvesting.domain.penzugy.Penzugy;
import springsbinvesting.domain.strategia.QStrategia;
import springsbinvesting.domain.strategia.QStrategiaReszveny;
import springsbinvesting.domain.strategia.Strategia;
import springsbinvesting.domain.strategia.StrategiaReszveny;
import springsbinvesting.model.repositories.api.IStrategiaRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class StrategiaRepository implements IStrategiaRepository {

    @PersistenceContext
    public EntityManager entityManager;

    @Override
    @Transactional
    public void addStrategiaItem(Long userId, Strategia strategia) {
        String addStrategiaQuery = "INSERT INTO strategia(eredmeny, teljesitmeny, pe, pb, datum, flag, user_id) values(?,?,?,?,?,?,?)";

        entityManager.createNativeQuery(addStrategiaQuery)
                .setParameter(1, strategia.getEredmeny())
                .setParameter(2, strategia.getTeljesitmeny())
                .setParameter(3, strategia.getPe())
                .setParameter(4, strategia.getPb())
                .setParameter(5, strategia.getDatum())
                .setParameter(6, strategia.getFlag())
                .setParameter(7, userId)
                .executeUpdate();
    }

    @Override
    public List<Strategia> findAllStrategiaItemByFlag(Long userId, String flag) {

         final JPAQuery<Penzugy> jpaQuery = new JPAQuery<Penzugy>(entityManager);

        QStrategia strategia = QStrategia.strategia;

        return jpaQuery.from(strategia).select(strategia).where(strategia.strategiaUser.id.eq(userId).and(strategia.flag.eq(flag))).fetch();

    }

    @Override
    @Transactional
    public void addStrategiaReszveny(Long userId, StrategiaReszveny strategiaReszveny) {
        String addStratReszvenyQuery = "INSERT INTO streszveny(vallalat, ticker, hozam, reszveny, eps, agazat, ipar, kapitalizacio, orszag, ready, user_id) values(?,?,?,?,?,?,?,?,?,?,?)";

        entityManager.createNativeQuery(addStratReszvenyQuery)
                .setParameter(1, strategiaReszveny.getVallalat())
                .setParameter(2, strategiaReszveny.getTicker())
                .setParameter(3, strategiaReszveny.getHozam())
                .setParameter(4,strategiaReszveny.getReszveny())
                .setParameter(5, strategiaReszveny.getEps())
                .setParameter(6, strategiaReszveny.getAgazat())
                .setParameter(7, strategiaReszveny.getIpar())
                .setParameter(8, strategiaReszveny.getKapitalizacio())
                .setParameter(9, strategiaReszveny.getOrszag())
                .setParameter(10, strategiaReszveny.getReady())
                .setParameter(11, userId)
                .executeUpdate();

    }

    @Override
    public List<StrategiaReszveny> getAllStratReszveny(Long userId) {
        final JPAQuery<Penzugy> jpaQuery = new JPAQuery<Penzugy>(entityManager);

        QStrategiaReszveny strategiaReszveny = QStrategiaReszveny.strategiaReszveny;

        return jpaQuery.from(strategiaReszveny).select(strategiaReszveny).where(strategiaReszveny.stReszvenyUser.id.eq(userId)).fetch();

    }
}
