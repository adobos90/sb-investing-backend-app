package springsbinvesting.model.repositories.impl;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.logging.Logger;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.jpa.repository.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.jpa.impl.JPAQuery;

import springfox.documentation.spring.web.json.Json;
import springsbinvesting.domain.QReszveny;
import springsbinvesting.domain.QUser;
import springsbinvesting.domain.Reszveny;
import springsbinvesting.domain.User;
import springsbinvesting.domain.celkituzes.Akadaly;
import springsbinvesting.domain.celkituzes.Celkituzes;
import springsbinvesting.domain.celkituzes.Feladat;
import springsbinvesting.domain.celkituzes.QFeladat;
import springsbinvesting.domain.reszvenyelemzes.*;
import springsbinvesting.domain.reszvenyelemzes.manageles.ManagelesBefAdatok;
import springsbinvesting.domain.reszvenyelemzes.manageles.ManagelesReszveny;
import springsbinvesting.domain.reszvenyelemzes.manageles.ManagelesiStrategia;
import springsbinvesting.domain.reszvenyelemzes.manageles.MenBefMenegeles;
import springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.PenzugyiElemzesHatekonysag;
import springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.PenzugyiElemzesHitel;
import springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.PenzugyiElemzesJovedelem;
import springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.PenzugyiElemzesPiaci;
import springsbinvesting.model.repositories.api.IUserRepository;

@Repository
public class UserRepository implements IUserRepository {

	@PersistenceContext
	public EntityManager entityManager;
	public static final Logger LOGGER = Logger.getLogger(UserRepository.class);

	@Override
	public List<User> findAllUserWithReszvenyek() {

		/*
		 * entityManager.
		 * createQuery("select firstname, lastname,vallalat_neve, agazat, mentalis.*  from user\r\n"
		 * + "inner join reszveny on reszveny.user_id=user.id \r\n" +
		 * "inner join mentalis on mentalis.reszveny_id=reszveny.id",
		 * User.class).getResultList(); final JPAQuery<User> jpaQuery = new
		 * JPAQuery<User>(entityManager);
		 */
		final JPAQuery<User> jpaQuery = new JPAQuery<User>(entityManager);
		QUser user = QUser.user;
		//QReszveny reszveny = QReszveny.reszveny;
		//QMentalisElemzes mentalis = QMentalisElemzes.mentalisElemzes;
		return jpaQuery.from(user).fetch();
	}

	@Override
	public User findUserWithReszvenyekByEmail(String email) {
		final JPAQuery<User> jpaQuery = new JPAQuery<User>(entityManager);
		QUser user = QUser.user;

		return jpaQuery.from(user).select(user).where(user.email.eq(email)).fetchOne();
	}

	@Override
	public User findUserWithReszvenyekById(Long id) {
		final JPAQuery<User> jpaQuery = new JPAQuery<User>(entityManager);
		QUser user = QUser.user;

		return jpaQuery.from(user).select(user).where(user.id.eq(id)).fetchOne();
	}

	@Override
	@Transactional
	public void addUser(User user) {
		String postUserQuery = "INSERT INTO user (firstname, lastname, email, passw, birthdate) VALUES (?,?,?,?,?)";
		entityManager.createNativeQuery(postUserQuery).setParameter(1, user.getFirstName()) // firstname
				.setParameter(2, user.getLastName()) // lastname
				.setParameter(3, user.getEmail()) // email
				.setParameter(4, user.getPassw()) // passw
				.setParameter(5, user.getBirthDate()) // birthdate
				.executeUpdate();
	}

	@Override
	@Transactional
	public User updateUser(Long userId, User user) {
		String query = "UPDATE `user` SET `firstname`=?,`lastname`=?,`birthdate`=?,`photo`=? WHERE id=?";

		entityManager.createNativeQuery(query)
				.setParameter(1, user.getFirstName())
				.setParameter(2, user.getLastName())
				.setParameter(3, user.getBirthDate())
				.setParameter(4, user.getPhoto())
				.setParameter(5, userId)
				.executeUpdate();

		return user;
	}

	@Override
	public List<Reszveny> getReszvenyek(Long userId) {
		final JPAQuery<Reszveny> jpaQuery = new JPAQuery<Reszveny>(entityManager);
		QReszveny reszveny = QReszveny.reszveny;
		return jpaQuery.from(reszveny).where(reszveny.reszvenyUser.id.eq(userId)).fetch();
	}

	@Override
	public Reszveny getReszvenyById(Long reszvenyId) {
		final JPAQuery<User> jpaQuery = new JPAQuery<User>(entityManager);
		QUser user = QUser.user;
		QReszveny reszveny = QReszveny.reszveny;

		return jpaQuery.from(reszveny).select(reszveny).where(reszveny.id.eq(reszvenyId)).fetchOne();
	}


	public Reszveny getReszvenyByTimeStamp(String timeStamp) {
		final JPAQuery<User> jpaQuery = new JPAQuery<User>(entityManager);
		QUser user = QUser.user;
		QReszveny reszveny = QReszveny.reszveny;

		return jpaQuery.from(reszveny).select(reszveny).where(reszveny.timestamp.eq(timeStamp)).fetchOne();
	}

	@Override
	public MentalisElemzes getMentalisByReszvenyId(Long reszvenyId) {
		final JPAQuery<User> jpaQuery = new JPAQuery<User>(entityManager);
		QMentalisElemzes mentalisElemzes = QMentalisElemzes.mentalisElemzes;

		return jpaQuery.from(mentalisElemzes).select(mentalisElemzes).where(mentalisElemzes.mentalisReszveny.id.eq(reszvenyId)).fetchOne();
	}

	@Override
	public VallalatKockElemzes getVallKockElemzes(Long reszvenyId) {
		final JPAQuery<User> jpaQuery = new JPAQuery<User>(entityManager);
		QVallalatKockElemzes kockElemzes = QVallalatKockElemzes.vallalatKockElemzes;

		return jpaQuery.from(kockElemzes).select(kockElemzes).where(kockElemzes.vallalatKockElemzesReszveny.id.eq(reszvenyId)).fetchOne();
	}

	@Override
	public PenzugyiAdatok getPenzugyiAdatok(Long reszvenyId) {
		final JPAQuery<User> jpaQuery = new JPAQuery<User>(entityManager);

		QPenzugyiAdatok penzugyiAdatok = QPenzugyiAdatok.penzugyiAdatok;

		return jpaQuery.from(penzugyiAdatok).select(penzugyiAdatok).where(penzugyiAdatok.penzAdatokReszveny.id.eq(reszvenyId)).fetchOne();
	}

	@Override
	public VallalatPenzugyiElemzes getVallPenzElemzes(Long reszvenyId) {
		final JPAQuery<User> jpaQuery = new JPAQuery<User>(entityManager);

		QVallalatPenzugyiElemzes penzugyiElemzes = QVallalatPenzugyiElemzes.vallalatPenzugyiElemzes;

		return jpaQuery.from(penzugyiElemzes).select(penzugyiElemzes).where(penzugyiElemzes.penzugyReszveny.id.eq(reszvenyId)).fetchOne();
	}

	@Override
	public Celar getCelar(Long reszvenyId) {
		final JPAQuery<User> jpaQuery = new JPAQuery<User>(entityManager);

		QCelar celar = QCelar.celar;

		return jpaQuery.from(celar).select(celar).where(celar.celarReszveny.id.eq(reszvenyId)).fetchOne();
	}

	@Override
	public NettoJelenErtek getNettoJelenertek(Long reszvenyId) {
		final JPAQuery<User> jpaQuery = new JPAQuery<User>(entityManager);

		QNettoJelenErtek nettoJelenErtek = QNettoJelenErtek.nettoJelenErtek;

		return jpaQuery.from(nettoJelenErtek).select(nettoJelenErtek).where(nettoJelenErtek.nettoReszveny.id.eq(reszvenyId)).fetchOne();
	}

	@Override
	public Manageles getManageles(Long reszvenyId) {
		final JPAQuery<User> jpaQuery = new JPAQuery<User>(entityManager);

		QManageles manageles = QManageles.manageles;

		return jpaQuery.from(manageles).select(manageles).where(manageles.managelesReszveny.id.eq(reszvenyId)).fetchOne();
	}

	@Override
	@Transactional
	public Reszveny addReszveny(Long id, Reszveny reszveny) {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());

		String postReszvenyQuery = "INSERT INTO reszveny (vallalat, ticker, datum, agazat, strategia, status, userid, kitolt, timestamp) VALUES (?,?,?,?,?,?,?,?, ?)";
		entityManager.createNativeQuery(postReszvenyQuery).setParameter(1, reszveny.getVallalat())
				.setParameter(2, reszveny.getTicker()).setParameter(3, reszveny.getDatum())
				.setParameter(4, reszveny.getAgazat()).setParameter(5, reszveny.getStrategia())
				.setParameter(6, reszveny.getStatus()).setParameter(7, id)
				.setParameter(8, reszveny.getKitolt())
				.setParameter(9, timestamp.toString())
				.executeUpdate();



		Reszveny newReszveny = getReszvenyByTimeStamp(timestamp.toString());

		return newReszveny;
	}

	@Override
	@Transactional
	// OK
	public Reszveny updateReszveny(Long reszvenyId, Reszveny newReszveny) {
		String updateReszvenyQuery = "UPDATE `reszveny` SET `vallalat`=?,`agazat`=?,`datum`=?,`status`=?,`strategia`=?,`ticker`=?,`kitolt`=? WHERE id=?";

		entityManager.createNativeQuery(updateReszvenyQuery).setParameter(1, newReszveny.getVallalat())
				.setParameter(2, newReszveny.getAgazat()).setParameter(3, newReszveny.getDatum())
				.setParameter(4, newReszveny.getStatus()).setParameter(5, newReszveny.getStrategia())
				.setParameter(6, newReszveny.getTicker()).setParameter(7, newReszveny.getKitolt())
				.setParameter(8, reszvenyId).executeUpdate();

	 return newReszveny;
	}

	@Override
	@Transactional
	// OK
	public MentalisElemzes updateMentalisElemzes(Long reszvenyId, MentalisElemzes mentalisElemzes) {
		String updateMentalisQuery = "UPDATE `mentalis` SET `ellensegesseg`=?,`ellensegesseg_elv`=?,`felelem`=?,`felelem_elv`=?,`fokozott`=?,`fokozott_elv`=?,`indokolatlan`=?,`indokolatlan_elv`=?,`introvertalt`=?,`introvertalt_elv`=?,`kavargo`=?,`kavargo_elv`=?,`kellemetlen`=?,`kellemetlen_elv`=?,`kimerultseg`=?,`kimerultseg_elv`=?,`onbizalom`=?,`onbizalom_elv`=?,`szomorusag`=?,`szomorusag_elv`=?  WHERE `reszveny_id`=? ";

		entityManager.createNativeQuery(updateMentalisQuery)
				.setParameter(1, mentalisElemzes.getEllensegesseg())
				.setParameter(2, mentalisElemzes.getEllensegesseg_elv())
				.setParameter(3, mentalisElemzes.getFelelem())
				.setParameter(4, mentalisElemzes.getFelelem_elv())
				.setParameter(5, mentalisElemzes.getFokozott())
				.setParameter(6, mentalisElemzes.getFokozott_elv())
				.setParameter(7, mentalisElemzes.getIndokolatlan())
				.setParameter(8, mentalisElemzes.getIndokolatlan_elv())
				.setParameter(9, mentalisElemzes.getIntrovertalt())
				.setParameter(10, mentalisElemzes.getIntrovertalt_elv())
				.setParameter(11, mentalisElemzes.getKavargo())
				.setParameter(12, mentalisElemzes.getKavargo_elv())
				.setParameter(13, mentalisElemzes.getKellemetlen())
				.setParameter(14, mentalisElemzes.getKellemetlen_elv())
				.setParameter(15, mentalisElemzes.getKimerultseg())
				.setParameter(16, mentalisElemzes.getKimerultseg_elv())
				.setParameter(17, mentalisElemzes.getOnbizalom())
				.setParameter(18, mentalisElemzes.getOnbizalom_elv())
				.setParameter(19, mentalisElemzes.getSzomorusag())
				.setParameter(20, mentalisElemzes.getSzomorusag_elv())
				.setParameter(21, reszvenyId).executeUpdate();

		return mentalisElemzes;
	}

	@Override
	@Transactional
	// OK
	public VallalatKockElemzes updateVallKockElemzes(Long reszvenyId, VallalatKockElemzes kockElemzes) {
		String updateVallKockElemzesQuery = "UPDATE `vallalatkockazatelemzes` SET `reszveny_eredmeny`=?,`reszveny_riziko`=?,`konyv_eredmeny`=?,`konyv_riziko`=?,`eps_eredmeny`=?,`eps_riziko`=?,`piaci_kap_eredmeny`=?,`piaci_kap_riziko`=?,`hitel_besorolas_eredmeny`=?,`hitel_besorolas_riziko`=?,`tobbsegi_tulaj_eredmeny`=?,`tobbsegi_tulaj_riziko`=?,`ceg_info_eredmeny`=?,`ceg_info_riziko`=?,`termek_info_eredmeny`=?,`termek_info_riziko`=?,`kutatas_fejlesztes_eredmeny`=?,`kutatas_fejlesztes_riziko`=?,`immaterialis_eredmeny`=?,`immaterialis_riziko`=?,`egyeb_eredmeny`=?,`egyeb_riziko`=?,`szukseges_allomany_eredmeny`=?,`szukseges_allomany_riziko`=?,`elerheto_info_eredmeny`=?,`elerheto_info_riziko`=?,`osztalek_eredmeny`=?,`osztalek_riziko`=?,`vallalat_tozsde_eredmeny`=?,`vallalat_tozsde_riziko`=?,`fontos_tech_eredmeny`=?,`fontos_tech_riziko`=?,`volt_csucson_eredmeny`=?,`volt_csucson_riziko`=?,`fuzio_eredmeny`=?,`fuzio_riziko`=?,`beta_eredmeny`=?,`beta_riziko`=?,`vez_me_eredmeny`=?,`vez_me_riziko`=?,`uj_reszveny_ki_eredmeny`=?,`uj_reszveny_ki_riziko`=? WHERE `reszveny_id`=?";

		entityManager.createNativeQuery(updateVallKockElemzesQuery)
				.setParameter(1, kockElemzes.getReszveny_eredmeny())
				.setParameter(2, kockElemzes.getReszveny_riziko())
				.setParameter(3, kockElemzes.getKonyv_eredmeny())
				.setParameter(4, kockElemzes.getKonyv_riziko())
				.setParameter(5, kockElemzes.getEps_eredmeny())
				.setParameter(6, kockElemzes.getEps_riziko())
				.setParameter(7, kockElemzes.getPiaci_kap_eredmeny())
				.setParameter(8, kockElemzes.getPiaci_kap_riziko())
				.setParameter(9, kockElemzes.getHitel_besorolas_eredmeny())
				.setParameter(10, kockElemzes.getHitel_besorolas_riziko())
				.setParameter(11, kockElemzes.getTobbsegi_tulaj_eredmeny())
				.setParameter(12, kockElemzes.getTobbsegi_tulaj_riziko())
				.setParameter(13, kockElemzes.getCeg_info_eredmeny())
				.setParameter(14, kockElemzes.getCeg_info_riziko())
				.setParameter(15, kockElemzes.getTermek_info_eredmeny())
				.setParameter(16, kockElemzes.getTermek_info_riziko())
				.setParameter(17, kockElemzes.getKutatas_fejlesztes_eredmeny())
				.setParameter(18, kockElemzes.getKutatas_fejlesztes_riziko())
				.setParameter(19, kockElemzes.getImmaterialis_eredmeny())
				.setParameter(20, kockElemzes.getImmaterialis_riziko())
				.setParameter(21, kockElemzes.getEgyeb_eredmeny())
				.setParameter(22, kockElemzes.getEgyeb_riziko())
				.setParameter(23, kockElemzes.getSzukseges_allomany_eredmeny())
				.setParameter(24, kockElemzes.getSzukseges_allomany_riziko())
				.setParameter(25, kockElemzes.getElerheto_info_eredmeny())
				.setParameter(26, kockElemzes.getElerheto_info_riziko())
				.setParameter(27, kockElemzes.getOsztalek_eredmeny())
				.setParameter(28, kockElemzes.getOsztalek_riziko())
				.setParameter(29, kockElemzes.getVallalat_tozsde_eredmeny())
				.setParameter(30, kockElemzes.getVallalat_tozsde_riziko())
				.setParameter(31, kockElemzes.getFontos_tech_eredmeny())
				.setParameter(32, kockElemzes.getFontos_tech_riziko())
				.setParameter(33, kockElemzes.getVolt_csucson_eredmeny())
				.setParameter(34, kockElemzes.getVolt_csucson_riziko())
				.setParameter(35, kockElemzes.getFuzio_eredmeny())
				.setParameter(36, kockElemzes.getFuzio_riziko())
				.setParameter(37, kockElemzes.getBeta_eredmeny())
				.setParameter(38, kockElemzes.getBeta_riziko())
				.setParameter(39, kockElemzes.getVez_me_eredmeny())
				.setParameter(40, kockElemzes.getVez_me_riziko())
				.setParameter(41, kockElemzes.getUj_reszveny_ki_eredmeny())
				.setParameter(42, kockElemzes.getUj_reszveny_ki_riziko())
				.setParameter(43, reszvenyId).executeUpdate();

		return kockElemzes;
	}

	@Override
	@Transactional
	public PenzugyiAdatok updatePenzugyiAdatok(Long reszvenyId, PenzugyiAdatok penzugyiAdatok) {

	String updatePenzugyiAdatokQuery = "UPDATE `penzugyi_adatok` SET `netto_targyev`=?,`netto_elozo_ev`=?,`netto_konkurencia`=?,`netto_q2`=?,`netto_q3`=?,`eladott_aru_targyev`=?,`eladott_aru_elozo_ev`=?,`eladott_aru_konkurencia`=?,`eladott_aru_q2`=?,`eladott_aru_q3`=?,`tarsasag_targyev`=?,`tarsasag_elozo_ev`=?,`tarsasag_konkurencia`=?,`tarsasag_q2`=?,`tarsasag_q3`=?,`kamat_targyev`=?,`kamat_elozo_ev`=?,`kamat_konkurencia`=?,`kamat_q2`=?,`kamat_q3`=?,`fizetett_kamatok_targyev`=?,`fizetett_kamatok_elozo_ev`=?,`fizetett_kamatok_konkurencia`=?,`fizetett_kamatok_q2`=?,`fizetett_kamatok_q3`=?,`adm_targyev`=?,`adm_elozo_ev`=?,`adm_konkurencia`=?,`adm_q2`=?,`adm_q3`=?,`egyeb_targyev`=?,`egyeb_elozo_ev`=?,`egyeb_konkurencia`=?,`egyeb_q2`=?,`egyeb_q3`=?,`reszveny_targyev`=?,`reszveny_elozo_ev`=?,`reszveny_konkurencia`=?,`reszveny_q2`=?,`reszveny_q3`=?,`osztalek_targyev`=?,`osztalek_elozo_ev`=?,`osztalek_konkurencia`=?,`osztalek_q2`=?,`osztalek_q3`=?,`elszamolt_targyev`=?,`elszamolt_elozo_ev`=?,`elszamolt_konkurencia`=?,`elszamolt_q2`=?,`elszamolt_q3`=?,`keszpenz_targyev`=?,`keszpenz_elozo_ev`=?,`keszpenz_konkurencia`=?,`keszpenz_q2`=?,`keszpenz_q3`=?,`koveteles_vevo_targyev`=?,`koveteles_vevo_elozo_ev`=?,`koveteles_vevo_konkurencia`=?,`koveteles_vevo_q2`=?,`koveteles_vevo_q3`=?,`keszlet_targyev`=?,`keszlet_elozo_ev`=?,`keszlet_konkurencia`=?,`keszlet_q2`=?,`keszlet_q3`=?,`jovairas_targyev`=?,`jovairas_elozo_ev`=?,`jovairas_konkurencia`=?,`jovairas_q2`=?,`jovairas_q3`=?,`imm_targyev`=?,`imm_elozo_ev`=?,`imm_konkurencia`=?,`imm_q2`=?,`imm_q3`=?,`forgo_targyev`=?,`forgo_elozo_ev`=?,`forgo_konkurencia`=?,`forgo_q2`=?,`forgo_q3`=?,`eszkoz_targyev`=?,`eszkoz_elozo_ev`=?,`eszkoz_konkurencia`=?,`eszkoz_q2`=?,`eszkoz_q3`=?,`kot_szallito_targyev`=?,`kot_szallito_elozo_ev`=?,`kot_szallito_konkurencia`=?,`kot_szallito_q2`=?,`kot_szallito_q3`=?,`toke_targyev`=?,`toke_elozo_ev`=?,`toke_konkurencia`=?,`toke_q2`=?,`toke_q3`=?,`rovid_lejaratu_kot_targyev`=?,`rovid_lejaratu_kot_elozo_ev`=?,`rovid_lejaratu_kot_konkurencia`=?,`rovid_lejaratu_kot_q2`=?,`rovid_lejaratu_kot_q3`=?,`ossz_kot_targyev`=?,`ossz_kot_elozo_ev`=?,`ossz_kot_konkurencia`=?,`ossz_kot_q2`=?,`ossz_kot_q3`=?,`alkalmazottak_szama_targyev`=?,`alkalmazottak_szama_elozo_ev`=?,`alkalmazottak_szama_konkurencia`=?,`alkalmazottak_szama_q2`=?,`alkalmazottak_szama_q3`=?,`reszveny_arfolyam_targyev`=?,`reszveny_arfolyam_elozo_ev`=?,`reszveny_arfolyam_konkurencia`=?,`reszveny_arfolyam_q2`=?,`reszveny_arfolyam_q3`=?,`naptar_targyev`=?,`naptar_elozo_ev`=?,`naptar_konkurencia`=?,`naptar_q2`=?,`naptar_q3`=?,`hozam_targyev`=?,`hozam_elozo_ev`=?,`hozam_konkurencia`=?,`hozam_q2`=?,`hozam_q3`=?  WHERE `reszveny_id`=?";
		entityManager.createNativeQuery(updatePenzugyiAdatokQuery).setParameter(1, penzugyiAdatok.getNetto_targyev())
				.setParameter(2, penzugyiAdatok.getNetto_elozo_ev()).setParameter(3, penzugyiAdatok.getNetto_konkurencia())
				.setParameter(4, penzugyiAdatok.getNetto_q2()).setParameter(5, penzugyiAdatok.getNetto_q3())
				.setParameter(6, penzugyiAdatok.getEladott_aru_targyev())
				.setParameter(7, penzugyiAdatok.getEladott_aru_elozo_ev())
				.setParameter(8, penzugyiAdatok.getEladott_aru_konkurencia())
				.setParameter(9, penzugyiAdatok.getEladott_aru_q2()).setParameter(10, penzugyiAdatok.getEladott_aru_q3())

				.setParameter(11, penzugyiAdatok.getTarsasag_targyev())
				.setParameter(12, penzugyiAdatok.getTarsasag_elozo_ev())
				.setParameter(13, penzugyiAdatok.getTarsasag_konkurencia())
				.setParameter(14, penzugyiAdatok.getTarsasag_q2()).setParameter(15, penzugyiAdatok.getTarsasag_q3())

				.setParameter(16, penzugyiAdatok.getKamat_targyev()).setParameter(17, penzugyiAdatok.getKamat_elozo_ev())
				.setParameter(18, penzugyiAdatok.getKamat_konkurencia()).setParameter(19, penzugyiAdatok.getKamat_q2())
				.setParameter(20, penzugyiAdatok.getKamat_q3())

				.setParameter(21, penzugyiAdatok.getFizetett_kamatok_targyev())
				.setParameter(22, penzugyiAdatok.getFizetett_kamatok_elozo_ev())
				.setParameter(23, penzugyiAdatok.getFizetett_kamatok_konkurencia())
				.setParameter(24, penzugyiAdatok.getFizetett_kamatok_q2())
				.setParameter(25, penzugyiAdatok.getFizetett_kamatok_q3())

				.setParameter(26, penzugyiAdatok.getAdm_targyev()).setParameter(27, penzugyiAdatok.getAdm_elozo_ev())
				.setParameter(28, penzugyiAdatok.getAdm_konkurencia()).setParameter(29, penzugyiAdatok.getAdm_q2())
				.setParameter(30, penzugyiAdatok.getAdm_q3())

				.setParameter(31, penzugyiAdatok.getEgyeb_targyev()).setParameter(32, penzugyiAdatok.getEgyeb_elozo_ev())
				.setParameter(33, penzugyiAdatok.getEgyeb_konkurencia()).setParameter(34, penzugyiAdatok.getEgyeb_q2())
				.setParameter(35, penzugyiAdatok.getEgyeb_q3())

				.setParameter(36, penzugyiAdatok.getReszveny_targyev())
				.setParameter(37, penzugyiAdatok.getReszveny_elozo_ev())
				.setParameter(38, penzugyiAdatok.getReszveny_konkurencia())
				.setParameter(39, penzugyiAdatok.getReszveny_q2()).setParameter(40, penzugyiAdatok.getReszveny_q3())

				.setParameter(41, penzugyiAdatok.getOsztalek_targyev())
				.setParameter(42, penzugyiAdatok.getOsztalek_elozo_ev())
				.setParameter(43, penzugyiAdatok.getOsztalek_konkurencia())
				.setParameter(44, penzugyiAdatok.getOsztalek_q2()).setParameter(45, penzugyiAdatok.getOsztalek_q3())

				.setParameter(46, penzugyiAdatok.getElszamolt_targyev())
				.setParameter(47, penzugyiAdatok.getElszamolt_elozo_ev())
				.setParameter(48, penzugyiAdatok.getElszamolt_konkurencia())
				.setParameter(49, penzugyiAdatok.getElszamolt_q2()).setParameter(50, penzugyiAdatok.getElszamolt_q3())

				.setParameter(51, penzugyiAdatok.getKeszpenz_targyev())
				.setParameter(52, penzugyiAdatok.getKeszpenz_elozo_ev())
				.setParameter(53, penzugyiAdatok.getKeszpenz_konkurencia())
				.setParameter(54, penzugyiAdatok.getKeszpenz_q2()).setParameter(55, penzugyiAdatok.getKeszpenz_q3())

				.setParameter(56, penzugyiAdatok.getKoveteles_vevo_targyev())
				.setParameter(57, penzugyiAdatok.getKoveteles_vevo_elozo_ev())
				.setParameter(58, penzugyiAdatok.getKoveteles_vevo_konkurencia())
				.setParameter(59, penzugyiAdatok.getKoveteles_vevo_q2())
				.setParameter(60, penzugyiAdatok.getKoveteles_vevo_q3())

				.setParameter(61, penzugyiAdatok.getKeszlet_targyev())
				.setParameter(62, penzugyiAdatok.getKeszlet_elozo_ev())
				.setParameter(63, penzugyiAdatok.getKeszlet_konkurencia())
				.setParameter(64, penzugyiAdatok.getKeszlet_q2()).setParameter(65, penzugyiAdatok.getKeszlet_q3())

				.setParameter(66, penzugyiAdatok.getJovairas_targyev())
				.setParameter(67, penzugyiAdatok.getJovairas_elozo_ev())
				.setParameter(68, penzugyiAdatok.getJovairas_konkurencia())
				.setParameter(69, penzugyiAdatok.getJovairas_q2()).setParameter(70, penzugyiAdatok.getJovairas_q3())

				.setParameter(71, penzugyiAdatok.getImm_targyev()).setParameter(72, penzugyiAdatok.getImm_elozo_ev())
				.setParameter(73, penzugyiAdatok.getImm_konkurencia()).setParameter(74, penzugyiAdatok.getImm_q2())
				.setParameter(75, penzugyiAdatok.getImm_q3())

				.setParameter(76, penzugyiAdatok.getForgo_targyev()).setParameter(77, penzugyiAdatok.getForgo_elozo_ev())
				.setParameter(78, penzugyiAdatok.getForgo_konkurencia()).setParameter(79, penzugyiAdatok.getForgo_q2())
				.setParameter(80, penzugyiAdatok.getForgo_q3())

				.setParameter(81, penzugyiAdatok.getEszkoz_targyev()).setParameter(82, penzugyiAdatok.getEszkoz_elozo_ev())
				.setParameter(83, penzugyiAdatok.getEszkoz_konkurencia()).setParameter(84, penzugyiAdatok.getEszkoz_q2())
				.setParameter(85, penzugyiAdatok.getEszkoz_q3())

				.setParameter(86, penzugyiAdatok.getKot_szallito_targyev())
				.setParameter(87, penzugyiAdatok.getKot_szallito_elozo_ev())
				.setParameter(88, penzugyiAdatok.getKot_szallito_konkurencia())
				.setParameter(89, penzugyiAdatok.getKot_szallito_q2()).setParameter(90, penzugyiAdatok.getKot_szallito_q3())

				.setParameter(91, penzugyiAdatok.getToke_targyev()).setParameter(92, penzugyiAdatok.getToke_elozo_ev())
				.setParameter(93, penzugyiAdatok.getToke_konkurencia()).setParameter(94, penzugyiAdatok.getToke_q2())
				.setParameter(95, penzugyiAdatok.getToke_q3())

				.setParameter(96, penzugyiAdatok.getRovid_lejaratu_kot_targyev())
				.setParameter(97, penzugyiAdatok.getRovid_lejaratu_kot_elozo_ev())
				.setParameter(98, penzugyiAdatok.getRovid_lejaratu_kot_konkurencia())
				.setParameter(99, penzugyiAdatok.getRovid_lejaratu_kot_q2())
				.setParameter(100, penzugyiAdatok.getRovid_lejaratu_kot_q3())

				.setParameter(101, penzugyiAdatok.getOssz_kot_targyev())
				.setParameter(102, penzugyiAdatok.getOssz_kot_elozo_ev())
				.setParameter(103, penzugyiAdatok.getOssz_kot_konkurencia())
				.setParameter(104, penzugyiAdatok.getOssz_kot_q2()).setParameter(105, penzugyiAdatok.getOssz_kot_q3())

				.setParameter(106, penzugyiAdatok.getAlkalmazottak_szama_targyev())
				.setParameter(107, penzugyiAdatok.getAlkalmazottak_szama_elozo_ev())
				.setParameter(108, penzugyiAdatok.getAlkalmazottak_szama_konkurencia())
				.setParameter(109, penzugyiAdatok.getAlkalmazottak_szama_q2())
				.setParameter(110, penzugyiAdatok.getAlkalmazottak_szama_q3())

				.setParameter(111, penzugyiAdatok.getReszveny_arfolyam_targyev())
				.setParameter(112, penzugyiAdatok.getReszveny_arfolyam_elozo_ev())
				.setParameter(113, penzugyiAdatok.getReszveny_arfolyam_konkurencia())
				.setParameter(114, penzugyiAdatok.getReszveny_arfolyam_q2())
				.setParameter(115, penzugyiAdatok.getReszveny_arfolyam_q3())

				.setParameter(116, penzugyiAdatok.getNaptar_targyev())
				.setParameter(117, penzugyiAdatok.getNaptar_elozo_ev())
				.setParameter(118, penzugyiAdatok.getNaptar_konkurencia())
				.setParameter(119, penzugyiAdatok.getNaptar_q2()).setParameter(120, penzugyiAdatok.getNaptar_q3())

				.setParameter(121, penzugyiAdatok.getHozam_targyev()).setParameter(122, penzugyiAdatok.getHozam_elozo_ev())
				.setParameter(123, penzugyiAdatok.getHozam_konkurencia()).setParameter(124, penzugyiAdatok.getHozam_q2())
				.setParameter(125, penzugyiAdatok.getHozam_q3()).setParameter(126, reszvenyId).executeUpdate();

		return penzugyiAdatok;
	}

	@Override
	@Transactional
	public VallalatPenzugyiElemzes updatePenzugyielemzes(Long reszvenyId, VallalatPenzugyiElemzes vallalatPenzugyiElemzes) {

		String updatePenzugyiElemzesQuery = "UPDATE `vallalatpenzugyielemzes`  WHERE `reszveny_id`=?";

		entityManager.createNativeQuery(updatePenzugyiElemzesQuery)
				.setParameter(1, reszvenyId).executeUpdate();

		return vallalatPenzugyiElemzes;
	}

	@Override
	@Transactional
	public void updateJovToPenzugyiElemzes(Long penzelemzesId, PenzugyiElemzesJovedelem jovPenzugyElemzes) {
		String updateJovPenzElemzesQuery = "UPDATE `vallalatpenzugyjov` SET `jov_megjegyzes`=?,`netto_eszkoz`=?,`eszkozok_eszkoz`=?,`targyev_eszkoz`=?,`elozoev_eszkoz`=?,`konkurencia_eszkoz`=?,`netto_sajat`=?,`ossz_sajat_toke`=?,`targyev_sajat`=?,`elozoev_sajat`=?,`konkurencia_sajat`=?,`netto_reszveny`=?,`reszveny_db`=?,`targyev_reszveny`=?,`elozoev_reszveny`=?,`konkurencia_reszveny`=?,`netto_arbevetel`=?,`arbevetel`=?,`targyev_arbevetel`=?,`elozoev_arbevetel`=?,`konkurencia_arbevetel`=?,`brutto_fedezet`=?,`eladott_aruk_ktg`=?,`netto_fedezet`=?,`targyev_fedezet`=?,`elozoev_fedezet`=?,`konkurencia_fedezet`=? WHERE `penzelemzes_id`=? ";


		entityManager.createNativeQuery(updateJovPenzElemzesQuery)
				.setParameter(1, jovPenzugyElemzes.getJov_megjegyzes())
				.setParameter(2, jovPenzugyElemzes.getNetto_eszkoz())
				.setParameter(3, jovPenzugyElemzes.getEszkozok_eszkoz())
				.setParameter(4, jovPenzugyElemzes.getTargyev_eszkoz())
				.setParameter(5, jovPenzugyElemzes.getElozoev_eszkoz())
				.setParameter(6, jovPenzugyElemzes.getKonkurencia_eszkoz())
				.setParameter(7, jovPenzugyElemzes.getNetto_sajat())
				.setParameter(8, jovPenzugyElemzes.getOssz_sajat_toke())
				.setParameter(9, jovPenzugyElemzes.getTargyev_sajat())
				.setParameter(10, jovPenzugyElemzes.getElozoev_sajat())
				.setParameter(11, jovPenzugyElemzes.getKonkurencia_sajat())
				.setParameter(12, jovPenzugyElemzes.getNetto_reszveny())
				.setParameter(13, jovPenzugyElemzes.getReszveny_db())
				.setParameter(14, jovPenzugyElemzes.getTargyev_reszveny())
				.setParameter(15, jovPenzugyElemzes.getElozoev_reszveny())
				.setParameter(16, jovPenzugyElemzes.getKonkurencia_reszveny())
				.setParameter(17, jovPenzugyElemzes.getNetto_arbevetel())
				.setParameter(18, jovPenzugyElemzes.getArbevetel())
				.setParameter(19, jovPenzugyElemzes.getTargyev_arbevetel())
				.setParameter(20, jovPenzugyElemzes.getElozoev_arbevetel())
				.setParameter(21, jovPenzugyElemzes.getKonkurencia_arbevetel())
				.setParameter(22, jovPenzugyElemzes.getBrutto_fedezet())
				.setParameter(23, jovPenzugyElemzes.getEladott_aruk_ktg())
				.setParameter(24, jovPenzugyElemzes.getNetto_fedezet())
				.setParameter(25, jovPenzugyElemzes.getTargyev_fedezet())
				.setParameter(26, jovPenzugyElemzes.getElozoev_fedezet())
				.setParameter(27, jovPenzugyElemzes.getKonkurencia_fedezet()).setParameter(28, penzelemzesId).executeUpdate();


	}

	@Override
	@Transactional
	public void updateHitelToPenzugyiElemzes(Long penzelemzesId, PenzugyiElemzesHitel hitelPenzugyElemzes ) {
		String updateHitelPenzElemzesQuery = "UPDATE `vallalatpenzugyhitel` SET `hitel_megjegyzes`=?,`hitel_sajat_toke`=?,`hitel_imm_javak`=?,`hitel_jovairas`=?,`hitel_ossz_hossz_lej_kot`=?,`targyev_sajat_toke_hitel`=?,`elozoev_sajat_toke_hitel`=?,`konkurencia_sajat_toke_hitel`=?,`hitel_kamat_jov_ado_elotti_eredmeny`=?,`hitel_elszamolt_amortizacio`=?,`hitel_kamat`=?,`hitel_kifizetett_osztalek`=?,`targyev_kamat_hitel`=?,`elozoev_kamat_hitel`=?,`konkurencia_kamat_hitel`=?,`hitel_rovid_likv_osszforgo_eszkoz`=?,`hitel_rovid_likv_kotelezettsegek`=?,`targyev_rovid_likv_hitel`=?,`elozoev_rovid_likv_hitel`=?,`konkurencia_rovid_likv_hitel`=?,`hitel_likv_osszforgo_eszkoz`=?,`hitel_likv_keszletek`=?,`hitel_likv_rovid_lej_kot`=?,`targyev_likv_hitel`=?,`elozoev_likv_hitel`=?,`konkurencia_likv_hitel`=? WHERE `penzelemzes_id`=?";

		entityManager.createNativeQuery(updateHitelPenzElemzesQuery)
				.setParameter(1, hitelPenzugyElemzes.getHitel_megjegyzes())
				.setParameter(2, hitelPenzugyElemzes.getHitel_sajat_toke())
				.setParameter(3, hitelPenzugyElemzes.getHitel_imm_javak())
				.setParameter(4, hitelPenzugyElemzes.getHitel_jovairas())
				.setParameter(5, hitelPenzugyElemzes.getHitel_ossz_hossz_lej_kot())
				.setParameter(6, hitelPenzugyElemzes.getTargyev_sajat_toke_hitel())
				.setParameter(7, hitelPenzugyElemzes.getElozoev_sajat_toke_hitel())
				.setParameter(8, hitelPenzugyElemzes.getKonkurencia_sajat_toke_hitel())
				.setParameter(9, hitelPenzugyElemzes.getHitel_kamat_jov_ado_elotti_eredmeny())
				.setParameter(10, hitelPenzugyElemzes.getHitel_elszamolt_amortizacio())
				.setParameter(11, hitelPenzugyElemzes.getHitel_kamat())
				.setParameter(12, hitelPenzugyElemzes.getHitel_kifizetett_osztalek())
				.setParameter(13, hitelPenzugyElemzes.getTargyev_kamat_hitel())
				.setParameter(14, hitelPenzugyElemzes.getElozoev_kamat_hitel())
				.setParameter(15, hitelPenzugyElemzes.getKonkurencia_kamat_hitel())
				.setParameter(16, hitelPenzugyElemzes.getHitel_rovid_likv_osszforgo_eszkoz())
				.setParameter(17, hitelPenzugyElemzes.getHitel_rovid_likv_kotelezettsegek())
				.setParameter(18, hitelPenzugyElemzes.getTargyev_rovid_likv_hitel())
				.setParameter(19, hitelPenzugyElemzes.getElozoev_rovid_likv_hitel())
				.setParameter(20, hitelPenzugyElemzes.getKonkurencia_rovid_likv_hitel())
				.setParameter(21, hitelPenzugyElemzes.getHitel_likv_osszforgo_eszkoz())
				.setParameter(22, hitelPenzugyElemzes.getHitel_likv_keszletek())
				.setParameter(23, hitelPenzugyElemzes.getHitel_likv_rovid_lej_kot())
				.setParameter(24, hitelPenzugyElemzes.getTargyev_likv_hitel())
				.setParameter(25, hitelPenzugyElemzes.getElozoev_likv_hitel())
				.setParameter(26, hitelPenzugyElemzes.getKonkurencia_likv_hitel()).setParameter(27, penzelemzesId).executeUpdate();
	}

	@Override
	@Transactional
	public void updateHatToPenzugyiElemzes(Long penzelemzesId, PenzugyiElemzesHatekonysag hatPenzugyElemzes) {
        String updateHatekonysagPenzElemzesQuery = "UPDATE `vallalatpenzugyhat` SET `hat_megjegyzes`=?,`netto_hat`=?,`keszlet_hat`=?,`targyev_keszlet_hat`=?,`elozoev_keszlet_hat`=?,`konkurencia_keszlet_hat`=?,`ert_hat`=?,`hat_egyeb_mukodesi`=?,`hat_netto_arbevetel`=?,`targyev_koltseg_hat`=?,`elozoev_koltseg_hat`=?,`konkurencia_koltseg_hat`=?,`naptar_hat`=?,`hat_atlagos_netto_arbevetel`=?,`hat_atlagos_koveteles`=?,`targyev_atlagos_hat`=?,`elozoev_atlagos_hat`=?,`konkurencia_atlagos_hat`=?,`naptar_hat_szallito`=?,`hat_szallito_netto_arbevetel`=?,`hat_szallito_kotelezettseg`=?,`targyev_szallito_hat`=?,`elozoev_szallito_hat`=?,`konkurencia_szallito_hat`=?,`fore_juto_netto_hat`=?,`fore_juto_letszam_hat`=?,`targyev_fore_juto_hat`=?,`elozoev_fore_juto_hat`=?,`konkurencia_fore_juto_hat`=?,`forgo_toke_hat_netto_arbevetel`=?,`forgo_toke_hat_forgo_eszkoz`=?,`forgo_toke_hat_rovid_lej_kot`=?,`targyev_forgo_toke_hat`=?,`elozoev_forgo_toke_hat`=?,`konkurencia_forgo_toke_hat`=? WHERE `penzelemzes_id`=?";

		entityManager.createNativeQuery(updateHatekonysagPenzElemzesQuery).setParameter(1, hatPenzugyElemzes.getHat_megjegyzes())
				.setParameter(2, hatPenzugyElemzes.getNetto_hat()).setParameter(3, hatPenzugyElemzes.getKeszlet_hat())
				.setParameter(4, hatPenzugyElemzes.getTargyev_keszlet_hat())
				.setParameter(5, hatPenzugyElemzes.getElozoev_keszlet_hat())
				.setParameter(6, hatPenzugyElemzes.getKonkurencia_keszlet_hat())
				.setParameter(7, hatPenzugyElemzes.getErt_hat()).setParameter(8, hatPenzugyElemzes.getHat_egyeb_mukodesi())
				.setParameter(9, hatPenzugyElemzes.getHat_netto_arbevetel())
				.setParameter(10, hatPenzugyElemzes.getTargyev_koltseg_hat())
				.setParameter(11, hatPenzugyElemzes.getElozoev_koltseg_hat())
				.setParameter(12, hatPenzugyElemzes.getKonkurencia_koltseg_hat())
				.setParameter(13, hatPenzugyElemzes.getNaptar_hat())
				.setParameter(14, hatPenzugyElemzes.getHat_atlagos_netto_arbevetel())
				.setParameter(15, hatPenzugyElemzes.getHat_atlagos_koveteles())
				.setParameter(16, hatPenzugyElemzes.getTargyev_atlagos_hat())
				.setParameter(17, hatPenzugyElemzes.getElozoev_atlagos_hat())
				.setParameter(18, hatPenzugyElemzes.getKonkurencia_atlagos_hat())
				.setParameter(19, hatPenzugyElemzes.getNaptar_hat_szallito())
				.setParameter(20, hatPenzugyElemzes.getHat_szallito_netto_arbevetel())
				.setParameter(21, hatPenzugyElemzes.getHat_szallito_kotelezettseg())
				.setParameter(22, hatPenzugyElemzes.getTargyev_szallito_hat())
				.setParameter(23, hatPenzugyElemzes.getElozoev_szallito_hat())
				.setParameter(24, hatPenzugyElemzes.getKonkurencia_szallito_hat())
				.setParameter(25, hatPenzugyElemzes.getFore_juto_netto_hat())
				.setParameter(26, hatPenzugyElemzes.getFore_juto_letszam_hat())
				.setParameter(27, hatPenzugyElemzes.getTargyev_fore_juto_hat())
				.setParameter(28, hatPenzugyElemzes.getElozoev_fore_juto_hat())
				.setParameter(29, hatPenzugyElemzes.getKonkurencia_fore_juto_hat())
				.setParameter(30, hatPenzugyElemzes.getForgo_toke_hat_netto_arbevetel())
				.setParameter(31, hatPenzugyElemzes.getForgo_toke_hat_forgo_eszkoz())
				.setParameter(32, hatPenzugyElemzes.getForgo_toke_hat_rovid_lej_kot())
				.setParameter(33, hatPenzugyElemzes.getTargyev_forgo_toke_hat())
				.setParameter(34, hatPenzugyElemzes.getElozoev_forgo_toke_hat())
				.setParameter(35, hatPenzugyElemzes.getKonkurencia_forgo_toke_hat()).setParameter(36, penzelemzesId).executeUpdate();

	}

	@Override
	@Transactional
	public void updatePiaciToPenzugyiElemzes(Long penzelemzesId, PenzugyiElemzesPiaci piaciPenzugyElemzes) {
		String updatePiaciPenzElemzesQuery = "UPDATE `vallalatpenzugypiaci` SET `piaci_megjegyzes`=?,`piaci_arfolyam_nyereseg`=?,`piaci_tars_netto_eredmeny`=?,`piaci_reszvenyek_db_szama`=?,`targyev_piaci_arfolyam_nyereseg`=?,`elozoev_piaci_arfolyam_nyereseg`=?,`konkurencia_piaci_arfolyam_nyereseg`=?,`piaci_bef_kam_netto_eredmeny`=?,`piaci_bef_kam_reszv_db_szama`=?,`piaci_bef_kam_reszveny_arfolyama`=?,`targyev_piaci_bef_kamatlab`=?,`elozoev_piaci_bef_kamatlab`=?,`konkurencia_piaci_bef_kamatlab`=?,`piaci_konyv_reszveny_arfolyam`=?,`piaci_konyv_sajat_toke`=?,`piaci_konyv_goodwill`=?,`piaci_konyv_imm_javak`=?,`piaci_konyv_reszvenyek_db_szama`=?,`targyev_piaci_konyv`=?,`elozoev_piaci_konyv`=?,`konkurencia_piaci_konyv`=? WHERE `penzelemzes_id`=?";

		entityManager.createNativeQuery(updatePiaciPenzElemzesQuery)
				.setParameter(1, piaciPenzugyElemzes.getPiaci_megjegyzes())
				.setParameter(2, piaciPenzugyElemzes.getPiaci_arfolyam_nyereseg())
				.setParameter(3, piaciPenzugyElemzes.getPiaci_tars_netto_eredmeny())
				.setParameter(4, piaciPenzugyElemzes.getPiaci_reszvenyek_db_szama())
				.setParameter(5, piaciPenzugyElemzes.getTargyev_piaci_arfolyam_nyereseg())
				.setParameter(6, piaciPenzugyElemzes.getElozoev_piaci_arfolyam_nyereseg())
				.setParameter(7, piaciPenzugyElemzes.getKonkurencia_piaci_arfolyam_nyereseg())
				.setParameter(8, piaciPenzugyElemzes.getPiaci_bef_kam_netto_eredmeny())
				.setParameter(9, piaciPenzugyElemzes.getPiaci_bef_kam_reszv_db_szama())
				.setParameter(10, piaciPenzugyElemzes.getPiaci_bef_kam_reszveny_arfolyama())
				.setParameter(11, piaciPenzugyElemzes.getTargyev_piaci_bef_kamatlab())
				.setParameter(12, piaciPenzugyElemzes.getElozoev_piaci_bef_kamatlab())
				.setParameter(13, piaciPenzugyElemzes.getKonkurencia_piaci_bef_kamatlab())
				.setParameter(14, piaciPenzugyElemzes.getPiaci_konyv_reszveny_arfolyam())
				.setParameter(15, piaciPenzugyElemzes.getPiaci_konyv_sajat_toke())
				.setParameter(16, piaciPenzugyElemzes.getPiaci_konyv_goodwill())
				.setParameter(17, piaciPenzugyElemzes.getPiaci_konyv_imm_javak())
				.setParameter(18, piaciPenzugyElemzes.getPiaci_konyv_reszvenyek_db_szama())
				.setParameter(19, piaciPenzugyElemzes.getTargyev_piaci_konyv())
				.setParameter(20, piaciPenzugyElemzes.getElozoev_piaci_konyv())
				.setParameter(21, piaciPenzugyElemzes.getKonkurencia_piaci_konyv()).setParameter(22, penzelemzesId).executeUpdate();
	}

	@Override
	@Transactional
	public Celar updateCelar(Long reszvenyId, Celar celar) {


		String updateCelarQuery = "UPDATE `celar` SET `konkurencia_mutato`=?,`konyv_szerinti_ertek`=?,`elvart_hozam`=? WHERE `reszveny_id`=?";

		entityManager.createNativeQuery(updateCelarQuery).setParameter(1, celar.getKonkurencia_mutato())
				.setParameter(2, celar.getKonyv_szerinti_ertek()).setParameter(3, celar.getElvart_hozam())
				.setParameter(4, reszvenyId).executeUpdate();

		return celar;
	}

	@Override
	@Transactional
	public NettoJelenErtek updateNettoJelenertek(Long reszvenyId, NettoJelenErtek netto) {
		String updateNettoJelenErtekQuery = "UPDATE `netto_jelenertek` SET `bef_osszeg`=?,`penznem`=?,`arfolyam`=?,`csucs_kimenet`=?,`csucs_val`=?,`celar_kimenet`=?,`celar_val`=?,`celar_fel_kimenet`=?,`celar_fel_val`=?,`alj_kimenet`=?,`alj_val`=?,`mely_kimenet`=?,`mely_val`=?,`csod_kimenet`=?,`csod_val`=? WHERE `reszveny_id`=?";
		entityManager.createNativeQuery(updateNettoJelenErtekQuery).setParameter(1, netto.getBef_osszeg())
				.setParameter(2, netto.getPenznem()).setParameter(3, netto.getArfolyam())
				.setParameter(4, netto.getCsucs_kimenet()).setParameter(5, netto.getCsucs_val())
				.setParameter(6, netto.getCelar_kimenet()).setParameter(7, netto.getCelar_val())
				.setParameter(8, netto.getCelar_fel_kimenet()).setParameter(9, netto.getCelar_fel_val())
				.setParameter(10, netto.getAlj_kimenet()).setParameter(11, netto.getAlj_val())
				.setParameter(12, netto.getMely_kimenet()).setParameter(13, netto.getMely_val())
				.setParameter(14, netto.getCsod_kimenet()).setParameter(15, netto.getCsod_val())
				.setParameter(16, reszvenyId).executeUpdate();

		return netto;
	}

	// Update Manageles items
	@Override
	@Transactional
	public ManagelesBefAdatok updateManagelesAdatok(Long managelesId, ManagelesBefAdatok managelesBefAdatok) {
		String query = "UPDATE `managelesbef` SET `piaci_kapitalizacio`=?,`menedzselesi_strat`=?,`elemzesi_arfolyam`=?,`elemzesi_penznem`=?,`celar_arfolyam`=?,`celar_penznem`=?,`eves_megtakaritas`=? WHERE `manageles_id`=?";

		entityManager.createNativeQuery(query)
				.setParameter(1, managelesBefAdatok.getPiaci_kapitalizacio())
				.setParameter(2, managelesBefAdatok.getMenedzselesi_strat())
				.setParameter(3, managelesBefAdatok.getElemzesi_arfolyam())
				.setParameter(4, managelesBefAdatok.getElemzesi_penznem())
				.setParameter(5, managelesBefAdatok.getCelar_arfolyam())
				.setParameter(6, managelesBefAdatok.getCelar_penznem())
				.setParameter(7, managelesBefAdatok.getEves_megtakaritas())
				.setParameter(8, managelesId)
				.executeUpdate();

		return managelesBefAdatok;
	}

	@Override
	@Transactional
	public ManagelesReszveny updateManagelesReszveny(Long managelesId, ManagelesReszveny managelesReszveny) {

		String query = "UPDATE `managelesreszveny` SET `vasarlas`=?,`eszkoz`=?,`stop_szint`=?,`riziko_faktor`=?,`bef_toke`=?,`mikor_vasarolni`=?,`gazdasagi_ingadozas_magyarazat`=?,`valallati_riziko_magyarazat`=?,`vallalati_rossz_penz_mutato_magy`=?,`vallalat_rossz_hat_mut_magyarazat`=?,`vallalat_hosszu_eladosodas_magyarazat`=?,`vallalat_rovid_eladosodas_magyarazat`=?,`vallalat_piaci_mut_magyarazat`=?,`vallalat_magas_reszveny_magyarazat`=?,`bef_netto_jelen_magyarazat`=?,`egyeb_magyarazat`=? WHERE `manageles_id`=?";

		entityManager.createNativeQuery(query)
				.setParameter(1, managelesReszveny.getVasarlas())
				.setParameter(2, managelesReszveny.getEszkoz())
				.setParameter(3, managelesReszveny.getStop_szint())
				.setParameter(4, managelesReszveny.getRiziko_faktor())
				.setParameter(5, managelesReszveny.getBef_toke())
				.setParameter(6, managelesReszveny.getMikor_vasarolni())
				.setParameter(7, managelesReszveny.getGazdasagi_ingadozas_magyarazat())
				.setParameter(8, managelesReszveny.getValallati_riziko_magyarazat())
				.setParameter(9, managelesReszveny.getVallalati_rossz_penz_mutato_magy())
				.setParameter(10, managelesReszveny.getVallalat_rossz_hat_mut_magyarazat())
				.setParameter(11, managelesReszveny.getVallalat_hosszu_eladosodas_magyarazat())
				.setParameter(12, managelesReszveny.getVallalat_rovid_eladosodas_magyarazat())
				.setParameter(13, managelesReszveny.getVallalat_piaci_mut_magyarazat())
				.setParameter(14, managelesReszveny.getVallalat_magas_reszveny_magyarazat())
				.setParameter(15, managelesReszveny.getBef_netto_jelen_magyarazat())
				.setParameter(16, managelesReszveny.getEgyeb_magyarazat())
				.setParameter(17, managelesId).executeUpdate();

		return managelesReszveny;
	}

	@Override
	@Transactional
	public ManagelesiStrategia updateManagelesStrat(Long managelesId, ManagelesiStrategia managelesiStrategia) {

		String query = "UPDATE `managelesstrat` SET `arfolyam_eses`=?,`arfolyam_emelkedes`=?,`arfolyam_celar`=?,`riziko_faktor_nott`=?,`jov_romlott`=?,`hitel_romlott`=?,`gazd_romlott`=? WHERE `manageles_id`=?";

		entityManager.createNativeQuery(query)
				.setParameter(1, managelesiStrategia.getArfolyam_eses())
				.setParameter(2, managelesiStrategia.getArfolyam_emelkedes())
				.setParameter(3, managelesiStrategia.getArfolyam_celar())
				.setParameter(4, managelesiStrategia.getRiziko_faktor_nott())
				.setParameter(5, managelesiStrategia.getJov_romlott())
				.setParameter(6, managelesiStrategia.getHitel_romlott())
				.setParameter(7, managelesiStrategia.getGazd_romlott())
				.setParameter(8, managelesId).executeUpdate();

		return managelesiStrategia;
	}

	@Override
	@Transactional
	public MenBefMenegeles updateBefMenManageles(Long managelesId,  Long id, MenBefMenegeles menBefMenegeles) {

		String query = "UPDATE `managelesbefmen` SET `kov_datum`=?,`arfolyam`=?,`bek_valtozasok`=?,`intezkedeseim`=?, `manageles_id`=? WHERE `id`=?";

		entityManager.createNativeQuery(query)
				.setParameter(1, menBefMenegeles.getKov_datum())
				.setParameter(2, menBefMenegeles.getArfolyam())
				.setParameter(3, menBefMenegeles.getBek_valtozasok())
				.setParameter(4, menBefMenegeles.getIntezkedeseim())
				.setParameter(5, managelesId)
				.setParameter(6, id)
				.executeUpdate();
		return menBefMenegeles;
	}


	@Override
	@Transactional
	public void deleteReszveny(Long id) {
		String deleteReszvenyQuery = "DELETE FROM reszveny WHERE id=?";

		entityManager.createNativeQuery(deleteReszvenyQuery)
				.setParameter(1, id).executeUpdate();
	}

	@Override
	@Transactional
	public void deleteMenBefMen(Long id,Long managelesId) {
		String deleteQuery = "DELETE FROM `managelesbefmen` WHERE `id`=? and `manageles_id`=?";

		entityManager.createNativeQuery(deleteQuery)
				.setParameter(1, id)
				.setParameter(2, managelesId)
				.executeUpdate();
	}

	@Override
	@Transactional
	public void addMentalisElemzes(Long reszvenyId, MentalisElemzes mentalis) {

		String postMentalisQuery = "INSERT INTO mentalis(ellensegesseg,\r\n" + "	ellensegesseg_elv,\r\n"
				+ "	felelem,\r\n" + "	felelem_elv,\r\n" + "	fokozott,\r\n" + "	fokozott_elv,\r\n"
				+ "	indokolatlan,\r\n" + "	indokolatlan_elv ,\r\n" + "	introvertalt,\r\n" + "	introvertalt_elv,\r\n"
				+ "	kavargo,\r\n" + "	kavargo_elv,\r\n" + "	kellemetlen,\r\n" + "	kellemetlen_elv,\r\n"
				+ "	kimerultseg,\r\n" + "	kimerultseg_elv,\r\n" + "	onbizalom,\r\n" + "	onbizalom_elv,\r\n"
				+ "	szomorusag,\r\n" + "	szomorusag_elv,\r\n"
				+ "	reszveny_id) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		entityManager.createNativeQuery(postMentalisQuery).setParameter(1, mentalis.getEllensegesseg())
				.setParameter(2, mentalis.getEllensegesseg_elv()).setParameter(3, mentalis.getFelelem())
				.setParameter(4, mentalis.getFelelem_elv()).setParameter(5, mentalis.getFokozott())
				.setParameter(6, mentalis.getFokozott_elv()).setParameter(7, mentalis.getIndokolatlan())
				.setParameter(8, mentalis.getIndokolatlan_elv()).setParameter(9, mentalis.getIntrovertalt())
				.setParameter(10, mentalis.getIntrovertalt_elv()).setParameter(11, mentalis.getKavargo())
				.setParameter(12, mentalis.getKavargo_elv()).setParameter(13, mentalis.getKellemetlen())
				.setParameter(14, mentalis.getKellemetlen_elv()).setParameter(15, mentalis.getKimerultseg())
				.setParameter(16, mentalis.getKimerultseg_elv()).setParameter(17, mentalis.getOnbizalom())
				.setParameter(18, mentalis.getOnbizalom_elv()).setParameter(19, mentalis.getSzomorusag())
				.setParameter(20, mentalis.getSzomorusag_elv()).setParameter(21, reszvenyId).executeUpdate();

	}

	@Override
	@Transactional
	public void addVallalatKockElemzes(Long reszvenyId, VallalatKockElemzes vallKockElemzes) {

		String postVallalatKockElemzesQuery = "INSERT INTO vallalatkockazatelemzes(" + "reszveny_eredmeny,\r\n"
				+ "reszveny_riziko,\r\n" + "konyv_eredmeny,\r\n" + "konyv_riziko,\r\n" + "eps_eredmeny,\r\n"
				+ "eps_riziko,\r\n" + "piaci_kap_eredmeny,\r\n" + "piaci_kap_riziko,\r\n"
				+ "hitel_besorolas_eredmeny,\r\n" + "hitel_besorolas_riziko,\r\n" + "tobbsegi_tulaj_eredmeny,\r\n"
				+ "tobbsegi_tulaj_riziko,\r\n" + "ceg_info_eredmeny,\r\n" + "ceg_info_riziko,\r\n"
				+ "termek_info_eredmeny,\r\n" + "termek_info_riziko,\r\n" + "kutatas_fejlesztes_eredmeny,\r\n"
				+ "kutatas_fejlesztes_riziko,\r\n" + "immaterialis_eredmeny,\r\n" + "immaterialis_riziko,\r\n"
				+ "egyeb_eredmeny,\r\n" + "egyeb_riziko,\r\n" + "szukseges_allomany_eredmeny,\r\n"
				+ "szukseges_allomany_riziko,\r\n" + "elerheto_info_eredmeny,\r\n" + "elerheto_info_riziko,\r\n"
				+ "osztalek_eredmeny,\r\n" + "osztalek_riziko,\r\n" + "vallalat_tozsde_eredmeny,\r\n"
				+ "vallalat_tozsde_riziko,\r\n" + "fontos_tech_eredmeny,\r\n" + "fontos_tech_riziko,\r\n"
				+ "volt_csucson_eredmeny,\r\n" + "volt_csucson_riziko,\r\n" + "fuzio_eredmeny,\r\n"
				+ "fuzio_riziko,\r\n" + "beta_eredmeny,\r\n" + "beta_riziko,\r\n" + "vez_me_eredmeny,\r\n"
				+ "vez_me_riziko,\r\n" + "uj_reszveny_ki_eredmeny,\r\n" + "uj_reszveny_ki_riziko,\r\n" + "reszveny_id) "
				+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		entityManager.createNativeQuery(postVallalatKockElemzesQuery)
				.setParameter(1, vallKockElemzes.getReszveny_eredmeny())
				.setParameter(2, vallKockElemzes.getReszveny_riziko())
				.setParameter(3, vallKockElemzes.getKonyv_eredmeny()).setParameter(4, vallKockElemzes.getKonyv_riziko())
				.setParameter(5, vallKockElemzes.getEps_eredmeny()).setParameter(6, vallKockElemzes.getEps_riziko())
				.setParameter(7, vallKockElemzes.getPiaci_kap_eredmeny())
				.setParameter(8, vallKockElemzes.getPiaci_kap_riziko())
				.setParameter(9, vallKockElemzes.getHitel_besorolas_eredmeny())
				.setParameter(10, vallKockElemzes.getHitel_besorolas_riziko())
				.setParameter(11, vallKockElemzes.getTobbsegi_tulaj_eredmeny())
				.setParameter(12, vallKockElemzes.getTobbsegi_tulaj_riziko())
				.setParameter(13, vallKockElemzes.getCeg_info_eredmeny())
				.setParameter(14, vallKockElemzes.getCeg_info_riziko())
				.setParameter(15, vallKockElemzes.getTermek_info_eredmeny())
				.setParameter(16, vallKockElemzes.getTermek_info_riziko())
				.setParameter(17, vallKockElemzes.getKutatas_fejlesztes_eredmeny())
				.setParameter(18, vallKockElemzes.getKutatas_fejlesztes_riziko())
				.setParameter(19, vallKockElemzes.getImmaterialis_eredmeny())
				.setParameter(20, vallKockElemzes.getImmaterialis_riziko())
				.setParameter(21, vallKockElemzes.getEgyeb_eredmeny())
				.setParameter(22, vallKockElemzes.getEgyeb_riziko())
				.setParameter(23, vallKockElemzes.getSzukseges_allomany_eredmeny())
				.setParameter(24, vallKockElemzes.getSzukseges_allomany_riziko())
				.setParameter(25, vallKockElemzes.getElerheto_info_eredmeny())
				.setParameter(26, vallKockElemzes.getElerheto_info_riziko())
				.setParameter(27, vallKockElemzes.getOsztalek_eredmeny())
				.setParameter(28, vallKockElemzes.getOsztalek_riziko())
				.setParameter(29, vallKockElemzes.getVallalat_tozsde_eredmeny())
				.setParameter(30, vallKockElemzes.getVallalat_tozsde_riziko())
				.setParameter(31, vallKockElemzes.getFontos_tech_eredmeny())
				.setParameter(32, vallKockElemzes.getFontos_tech_riziko())
				.setParameter(33, vallKockElemzes.getVolt_csucson_eredmeny())
				.setParameter(34, vallKockElemzes.getVolt_csucson_riziko())
				.setParameter(35, vallKockElemzes.getFuzio_eredmeny())
				.setParameter(36, vallKockElemzes.getFuzio_riziko())
				.setParameter(37, vallKockElemzes.getBeta_eredmeny()).setParameter(38, vallKockElemzes.getBeta_riziko())
				.setParameter(39, vallKockElemzes.getVez_me_eredmeny())
				.setParameter(40, vallKockElemzes.getVez_me_riziko())
				.setParameter(41, vallKockElemzes.getUj_reszveny_ki_eredmeny())
				.setParameter(42, vallKockElemzes.getUj_reszveny_ki_riziko()).setParameter(43, reszvenyId)
				.executeUpdate();
	}

	@Override
	@Transactional
	public void addCelar(Long reszvenyId, Celar celar) {
		String celarPostQuery = "insert into celar(konkurencia_mutato,konyv_szerinti_ertek,elvart_hozam, reszveny_id)"
				+ "values(?,?,?,?)";

		entityManager.createNativeQuery(celarPostQuery).setParameter(1, celar.getKonkurencia_mutato())
				.setParameter(2, celar.getKonyv_szerinti_ertek()).setParameter(3, celar.getElvart_hozam())
				.setParameter(4, reszvenyId).executeUpdate();
	}

	@Override
	@Transactional
	public void addNettoJelenErtek(Long reszvenyId, NettoJelenErtek netto) {
		String nettoPostQuery = "insert into netto_jelenertek(bef_osszeg,penznem,arfolyam,csucs_kimenet,csucs_val,celar_kimenet,celar_val,celar_fel_kimenet,celar_fel_val,alj_kimenet,alj_val,mely_kimenet,mely_val,csod_kimenet, csod_val ,reszveny_id) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		entityManager.createNativeQuery(nettoPostQuery).setParameter(1, netto.getBef_osszeg())
				.setParameter(2, netto.getPenznem()).setParameter(3, netto.getArfolyam())
				.setParameter(4, netto.getCsucs_kimenet()).setParameter(5, netto.getCsucs_val())
				.setParameter(6, netto.getCelar_kimenet()).setParameter(7, netto.getCelar_val())
				.setParameter(8, netto.getCelar_fel_kimenet()).setParameter(9, netto.getCelar_fel_val())
				.setParameter(10, netto.getAlj_kimenet()).setParameter(11, netto.getAlj_val())
				.setParameter(12, netto.getMely_kimenet()).setParameter(13, netto.getMely_val())
				.setParameter(14, netto.getCsod_kimenet()).setParameter(15, netto.getCsod_val())
				.setParameter(16, reszvenyId).executeUpdate();
	}

	@Override
	@Transactional
	public void addManageles(Long reszvenyId, Manageles manageles) {
		String managelesPostQuery = "insert into manageles(reszveny_id) values(?)";

		entityManager.createNativeQuery(managelesPostQuery).setParameter(1, reszvenyId).executeUpdate();
	}

	@Override
	@Transactional
	public void addBefAdatokToManageles(Long managelesId, ManagelesBefAdatok menBefadatok) {
		String befAdatokMAnagelesPostQuery = "insert into managelesbef(szektor,piaci_kapitalizacio, menedzselesi_strat, elemzesi_arfolyam,elemzesi_penznem, celar_arfolyam, celar_penznem, eves_megtakaritas,manageles_id)"
				+ "values(?,?,?,?,?,?,?,?,?)";

		entityManager.createNativeQuery(befAdatokMAnagelesPostQuery)
				.setParameter(1, menBefadatok.getSzektor())
				.setParameter(2, menBefadatok.getPiaci_kapitalizacio())
				.setParameter(3, menBefadatok.getMenedzselesi_strat())
				.setParameter(4, menBefadatok.getElemzesi_arfolyam())
				.setParameter(5, menBefadatok.getElemzesi_penznem()).setParameter(6, menBefadatok.getCelar_arfolyam())
				.setParameter(7, menBefadatok.getCelar_penznem()).setParameter(8, menBefadatok.getEves_megtakaritas())
				.setParameter(9, managelesId).executeUpdate();

	}

	@Override
	@Transactional
	public void addReszvenyToManageles(Long managelesId, ManagelesReszveny menReszveny) {
		String befReszvenyPostQuery = "INSERT INTO `managelesreszveny`( `vasarlas`, `eszkoz`, `stop_szint`, `riziko_faktor`, `bef_toke`, `mikor_vasarolni`, `gazdasagi_ingadozas_magyarazat`, `valallati_riziko_magyarazat`, `vallalati_rossz_penz_mutato_magy`, `vallalat_rossz_hat_mut_magyarazat`, `vallalat_hosszu_eladosodas_magyarazat`, `vallalat_rovid_eladosodas_magyarazat`, `vallalat_piaci_mut_magyarazat`, `vallalat_magas_reszveny_magyarazat`, `bef_netto_jelen_magyarazat`, `egyeb_magyarazat`, `manageles_id`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		entityManager.createNativeQuery(befReszvenyPostQuery).setParameter(1, menReszveny.getVasarlas())
				.setParameter(2, menReszveny.getEszkoz()).setParameter(3, menReszveny.getStop_szint())
				.setParameter(4, menReszveny.getRiziko_faktor()).setParameter(5, menReszveny.getBef_toke())
				.setParameter(6, menReszveny.getMikor_vasarolni())
				.setParameter(7, menReszveny.getGazdasagi_ingadozas_magyarazat())
				.setParameter(8, menReszveny.getValallati_riziko_magyarazat())
				.setParameter(9, menReszveny.getVallalati_rossz_penz_mutato_magy())
				.setParameter(10, menReszveny.getVallalat_rossz_hat_mut_magyarazat())
				.setParameter(11, menReszveny.getVallalat_hosszu_eladosodas_magyarazat())
				.setParameter(12, menReszveny.getVallalat_rovid_eladosodas_magyarazat())
				.setParameter(13, menReszveny.getVallalat_piaci_mut_magyarazat())
				.setParameter(14, menReszveny.getVallalat_magas_reszveny_magyarazat())
				.setParameter(15, menReszveny.getBef_netto_jelen_magyarazat())
				.setParameter(16, menReszveny.getEgyeb_magyarazat()).setParameter(17, managelesId).executeUpdate();
	}

	@Override
	@Transactional
	public void addStrategiaToManageles(Long managelesId, ManagelesiStrategia menStrat) {
		String stratManagelesPostQuery = "INSERT INTO `managelesstrat`( `arfolyam_eses`, `arfolyam_emelkedes`, `arfolyam_celar`, `riziko_faktor_nott`, `jov_romlott`, `hitel_romlott`, `gazd_romlott`, `manageles_id`) VALUES (?,?,?,?,?,?,?,?)";

		entityManager.createNativeQuery(stratManagelesPostQuery).setParameter(1, menStrat.getArfolyam_eses())
				.setParameter(2, menStrat.getArfolyam_emelkedes()).setParameter(3, menStrat.getArfolyam_celar())
				.setParameter(4, menStrat.getRiziko_faktor_nott()).setParameter(5, menStrat.getJov_romlott())
				.setParameter(6, menStrat.getHitel_romlott()).setParameter(7, menStrat.getGazd_romlott())
				.setParameter(8, managelesId).executeUpdate();

	}

	@Override
	@Transactional
	public void addMenBefMenToManageles(Long managelesId, MenBefMenegeles menBefMen) {
		String menBefMenPostQuery = "INSERT INTO `managelesbefmen`(`kov_datum`, `arfolyam`, `bek_valtozasok`, `intezkedeseim`, `manageles_id`) VALUES (?,?,?,?,?)";

		entityManager.createNativeQuery(menBefMenPostQuery).setParameter(1, menBefMen.getKov_datum())
				.setParameter(2, menBefMen.getArfolyam()).setParameter(3, menBefMen.getBek_valtozasok())
				.setParameter(4, menBefMen.getIntezkedeseim()).setParameter(5, managelesId).executeUpdate();
	}

	@Override
	@Transactional
	public void addPenzugyiElemzes(Long reszvenyId, VallalatPenzugyiElemzes penzugyiElemzes) {
		String penzugyiElemzesPostQuery = "insert into vallalatpenzugyielemzes(reszveny_id) values(?)";

		entityManager.createNativeQuery(penzugyiElemzesPostQuery).setParameter(1, reszvenyId).executeUpdate();

	}

	@Override
	@Transactional
	public void addJovToPenzugyiElemzes(Long penzugyId, PenzugyiElemzesJovedelem jovPenzugyElemzes) {
		String postJovPenzugyiElemzesQuery = "INSERT INTO `vallalatpenzugyjov`( `jov_megjegyzes`, `netto_eszkoz`, `eszkozok_eszkoz`, `targyev_eszkoz`, `elozoev_eszkoz`, `konkurencia_eszkoz`, `netto_sajat`, `ossz_sajat_toke`, `targyev_sajat`, `elozoev_sajat`, `konkurencia_sajat`, `netto_reszveny`, `reszveny_db`, `targyev_reszveny`, `elozoev_reszveny`, `konkurencia_reszveny`, `netto_arbevetel`, `arbevetel`, `targyev_arbevetel`, `elozoev_arbevetel`, `konkurencia_arbevetel`, `brutto_fedezet`, `eladott_aruk_ktg`, `netto_fedezet`, `targyev_fedezet`, `elozoev_fedezet`, `konkurencia_fedezet`, `penzelemzes_id`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";


		entityManager.createNativeQuery(postJovPenzugyiElemzesQuery)
				.setParameter(1, jovPenzugyElemzes.getJov_megjegyzes())
				.setParameter(2, jovPenzugyElemzes.getNetto_eszkoz())
				.setParameter(3, jovPenzugyElemzes.getEszkozok_eszkoz())
				.setParameter(4, jovPenzugyElemzes.getTargyev_eszkoz())
				.setParameter(5, jovPenzugyElemzes.getElozoev_eszkoz())
				.setParameter(6, jovPenzugyElemzes.getKonkurencia_eszkoz())
				.setParameter(7, jovPenzugyElemzes.getNetto_sajat())
				.setParameter(8, jovPenzugyElemzes.getOssz_sajat_toke())
				.setParameter(9, jovPenzugyElemzes.getTargyev_sajat())
				.setParameter(10, jovPenzugyElemzes.getElozoev_sajat())
				.setParameter(11, jovPenzugyElemzes.getKonkurencia_sajat())
				.setParameter(12, jovPenzugyElemzes.getNetto_reszveny())
				.setParameter(13, jovPenzugyElemzes.getReszveny_db())
				.setParameter(14, jovPenzugyElemzes.getTargyev_reszveny())
				.setParameter(15, jovPenzugyElemzes.getElozoev_reszveny())
				.setParameter(16, jovPenzugyElemzes.getKonkurencia_reszveny())
				.setParameter(17, jovPenzugyElemzes.getNetto_arbevetel())
				.setParameter(18, jovPenzugyElemzes.getArbevetel())
				.setParameter(19, jovPenzugyElemzes.getTargyev_arbevetel())
				.setParameter(20, jovPenzugyElemzes.getElozoev_arbevetel())
				.setParameter(21, jovPenzugyElemzes.getKonkurencia_arbevetel())
				.setParameter(22, jovPenzugyElemzes.getBrutto_fedezet())
				.setParameter(23, jovPenzugyElemzes.getEladott_aruk_ktg())
				.setParameter(24, jovPenzugyElemzes.getNetto_fedezet())
				.setParameter(25, jovPenzugyElemzes.getTargyev_fedezet())
				.setParameter(26, jovPenzugyElemzes.getElozoev_fedezet())
				.setParameter(27, jovPenzugyElemzes.getKonkurencia_fedezet()).setParameter(28, penzugyId).executeUpdate();


	}

	@Override
	@Transactional
	public void addHatToPenzugyiElemzes(Long penzugyId, PenzugyiElemzesHatekonysag hatPenzugyElemzes) {
		String hatPenzugyiELemzesQuery = "INSERT INTO `vallalatpenzugyhat`( `hat_megjegyzes`, `netto_hat`, `keszlet_hat`, `targyev_keszlet_hat`, `elozoev_keszlet_hat`, `konkurencia_keszlet_hat`, `ert_hat`, `hat_egyeb_mukodesi`, `hat_netto_arbevetel`, `targyev_koltseg_hat`, `elozoev_koltseg_hat`, `konkurencia_koltseg_hat`, `naptar_hat`, `hat_atlagos_netto_arbevetel`, `hat_atlagos_koveteles`, `targyev_atlagos_hat`, `elozoev_atlagos_hat`, `konkurencia_atlagos_hat`, `naptar_hat_szallito`, `hat_szallito_netto_arbevetel`, `hat_szallito_kotelezettseg`, `targyev_szallito_hat`, `elozoev_szallito_hat`, `konkurencia_szallito_hat`, `fore_juto_netto_hat`, `fore_juto_letszam_hat`, `targyev_fore_juto_hat`, `elozoev_fore_juto_hat`, `konkurencia_fore_juto_hat`, `forgo_toke_hat_netto_arbevetel`, `forgo_toke_hat_forgo_eszkoz`, `forgo_toke_hat_rovid_lej_kot`, `targyev_forgo_toke_hat`, `elozoev_forgo_toke_hat`, `konkurencia_forgo_toke_hat`, `penzelemzes_id`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		entityManager.createNativeQuery(hatPenzugyiELemzesQuery).setParameter(1, hatPenzugyElemzes.getHat_megjegyzes())
				.setParameter(2, hatPenzugyElemzes.getNetto_hat()).setParameter(3, hatPenzugyElemzes.getKeszlet_hat())
				.setParameter(4, hatPenzugyElemzes.getTargyev_keszlet_hat())
				.setParameter(5, hatPenzugyElemzes.getElozoev_keszlet_hat())
				.setParameter(6, hatPenzugyElemzes.getKonkurencia_keszlet_hat())
				.setParameter(7, hatPenzugyElemzes.getErt_hat()).setParameter(8, hatPenzugyElemzes.getHat_egyeb_mukodesi())
				.setParameter(9, hatPenzugyElemzes.getHat_netto_arbevetel())
				.setParameter(10, hatPenzugyElemzes.getTargyev_koltseg_hat())
				.setParameter(11, hatPenzugyElemzes.getElozoev_koltseg_hat())
				.setParameter(12, hatPenzugyElemzes.getKonkurencia_koltseg_hat())
				.setParameter(13, hatPenzugyElemzes.getNaptar_hat())
				.setParameter(14, hatPenzugyElemzes.getHat_atlagos_netto_arbevetel())
				.setParameter(15, hatPenzugyElemzes.getHat_atlagos_koveteles())
				.setParameter(16, hatPenzugyElemzes.getTargyev_atlagos_hat())
				.setParameter(17, hatPenzugyElemzes.getElozoev_atlagos_hat())
				.setParameter(18, hatPenzugyElemzes.getKonkurencia_atlagos_hat())
				.setParameter(19, hatPenzugyElemzes.getNaptar_hat_szallito())
				.setParameter(20, hatPenzugyElemzes.getHat_szallito_netto_arbevetel())
				.setParameter(21, hatPenzugyElemzes.getHat_szallito_kotelezettseg())
				.setParameter(22, hatPenzugyElemzes.getTargyev_szallito_hat())
				.setParameter(23, hatPenzugyElemzes.getElozoev_szallito_hat())
				.setParameter(24, hatPenzugyElemzes.getKonkurencia_szallito_hat())
				.setParameter(25, hatPenzugyElemzes.getFore_juto_netto_hat())
				.setParameter(26, hatPenzugyElemzes.getFore_juto_letszam_hat())
				.setParameter(27, hatPenzugyElemzes.getTargyev_fore_juto_hat())
				.setParameter(28, hatPenzugyElemzes.getElozoev_fore_juto_hat())
				.setParameter(29, hatPenzugyElemzes.getKonkurencia_fore_juto_hat())
				.setParameter(30, hatPenzugyElemzes.getForgo_toke_hat_netto_arbevetel())
				.setParameter(31, hatPenzugyElemzes.getForgo_toke_hat_forgo_eszkoz())
				.setParameter(32, hatPenzugyElemzes.getForgo_toke_hat_rovid_lej_kot())
				.setParameter(33, hatPenzugyElemzes.getTargyev_forgo_toke_hat())
				.setParameter(34, hatPenzugyElemzes.getElozoev_forgo_toke_hat())
				.setParameter(35, hatPenzugyElemzes.getKonkurencia_forgo_toke_hat()).setParameter(36, penzugyId).executeUpdate();

	}

	@Override
	@Transactional
	public void addHitelToPenzugyiElemzes(Long penzugyId, PenzugyiElemzesHitel hitelPenzugyElemzes) {
		String hitelPenzugyiElemzesPostQuery = "INSERT INTO `vallalatpenzugyhitel`( `hitel_megjegyzes`, `hitel_sajat_toke`, `hitel_imm_javak`, `hitel_jovairas`, `hitel_ossz_hossz_lej_kot`, `targyev_sajat_toke_hitel`, `elozoev_sajat_toke_hitel`, `konkurencia_sajat_toke_hitel`, `hitel_kamat_jov_ado_elotti_eredmeny`, `hitel_elszamolt_amortizacio`, `hitel_kamat`, `hitel_kifizetett_osztalek`, `targyev_kamat_hitel`, `elozoev_kamat_hitel`, `konkurencia_kamat_hitel`, `hitel_rovid_likv_osszforgo_eszkoz`, `hitel_rovid_likv_kotelezettsegek`, `targyev_rovid_likv_hitel`, `elozoev_rovid_likv_hitel`, `konkurencia_rovid_likv_hitel`, `hitel_likv_osszforgo_eszkoz`, `hitel_likv_keszletek`, `hitel_likv_rovid_lej_kot`, `targyev_likv_hitel`, `elozoev_likv_hitel`, `konkurencia_likv_hitel`, `penzelemzes_id`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		entityManager.createNativeQuery(hitelPenzugyiElemzesPostQuery)
				.setParameter(1, hitelPenzugyElemzes.getHitel_megjegyzes())
				.setParameter(2, hitelPenzugyElemzes.getHitel_sajat_toke())
				.setParameter(3, hitelPenzugyElemzes.getHitel_imm_javak())
				.setParameter(4, hitelPenzugyElemzes.getHitel_jovairas())
				.setParameter(5, hitelPenzugyElemzes.getHitel_ossz_hossz_lej_kot())
				.setParameter(6, hitelPenzugyElemzes.getTargyev_sajat_toke_hitel())
				.setParameter(7, hitelPenzugyElemzes.getElozoev_sajat_toke_hitel())
				.setParameter(8, hitelPenzugyElemzes.getKonkurencia_sajat_toke_hitel())
				.setParameter(9, hitelPenzugyElemzes.getHitel_kamat_jov_ado_elotti_eredmeny())
				.setParameter(10, hitelPenzugyElemzes.getHitel_elszamolt_amortizacio())
				.setParameter(11, hitelPenzugyElemzes.getHitel_kamat())
				.setParameter(12, hitelPenzugyElemzes.getHitel_kifizetett_osztalek())
				.setParameter(13, hitelPenzugyElemzes.getTargyev_kamat_hitel())
				.setParameter(14, hitelPenzugyElemzes.getElozoev_kamat_hitel())
				.setParameter(15, hitelPenzugyElemzes.getKonkurencia_kamat_hitel())
				.setParameter(16, hitelPenzugyElemzes.getHitel_rovid_likv_osszforgo_eszkoz())
				.setParameter(17, hitelPenzugyElemzes.getHitel_rovid_likv_kotelezettsegek())
				.setParameter(18, hitelPenzugyElemzes.getTargyev_rovid_likv_hitel())
				.setParameter(19, hitelPenzugyElemzes.getElozoev_rovid_likv_hitel())
				.setParameter(20, hitelPenzugyElemzes.getKonkurencia_rovid_likv_hitel())
				.setParameter(21, hitelPenzugyElemzes.getHitel_likv_osszforgo_eszkoz())
				.setParameter(22, hitelPenzugyElemzes.getHitel_likv_keszletek())
				.setParameter(23, hitelPenzugyElemzes.getHitel_likv_rovid_lej_kot())
				.setParameter(24, hitelPenzugyElemzes.getTargyev_likv_hitel())
				.setParameter(25, hitelPenzugyElemzes.getElozoev_likv_hitel())
				.setParameter(26, hitelPenzugyElemzes.getKonkurencia_likv_hitel()).setParameter(27, penzugyId).executeUpdate();

	}

	@Override
	@Transactional
	public void addPiaciToPenzugyiElemzes(Long penzugyId, PenzugyiElemzesPiaci piaciPenzugyElemzes) {
		String piaciPenzugyiElemzesPostQuery = "INSERT INTO `vallalatpenzugypiaci`( `piaci_megjegyzes`, `piaci_arfolyam_nyereseg`, `piaci_tars_netto_eredmeny`, `piaci_reszvenyek_db_szama`, `targyev_piaci_arfolyam_nyereseg`, `elozoev_piaci_arfolyam_nyereseg`, `konkurencia_piaci_arfolyam_nyereseg`, `piaci_bef_kam_netto_eredmeny`, `piaci_bef_kam_reszv_db_szama`, `piaci_bef_kam_reszveny_arfolyama`, `targyev_piaci_bef_kamatlab`, `elozoev_piaci_bef_kamatlab`, `konkurencia_piaci_bef_kamatlab`, `piaci_konyv_reszveny_arfolyam`, `piaci_konyv_sajat_toke`, `piaci_konyv_goodwill`, `piaci_konyv_imm_javak`, `piaci_konyv_reszvenyek_db_szama`, `targyev_piaci_konyv`, `elozoev_piaci_konyv`, `konkurencia_piaci_konyv`, `penzelemzes_id`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		entityManager.createNativeQuery(piaciPenzugyiElemzesPostQuery)
				.setParameter(1, piaciPenzugyElemzes.getPiaci_megjegyzes())
				.setParameter(2, piaciPenzugyElemzes.getPiaci_arfolyam_nyereseg())
				.setParameter(3, piaciPenzugyElemzes.getPiaci_tars_netto_eredmeny())
				.setParameter(4, piaciPenzugyElemzes.getPiaci_reszvenyek_db_szama())
				.setParameter(5, piaciPenzugyElemzes.getTargyev_piaci_arfolyam_nyereseg())
				.setParameter(6, piaciPenzugyElemzes.getElozoev_piaci_arfolyam_nyereseg())
				.setParameter(7, piaciPenzugyElemzes.getKonkurencia_piaci_arfolyam_nyereseg())
				.setParameter(8, piaciPenzugyElemzes.getPiaci_bef_kam_netto_eredmeny())
				.setParameter(9, piaciPenzugyElemzes.getPiaci_bef_kam_reszv_db_szama())
				.setParameter(10, piaciPenzugyElemzes.getPiaci_bef_kam_reszveny_arfolyama())
				.setParameter(11, piaciPenzugyElemzes.getTargyev_piaci_bef_kamatlab())
				.setParameter(12, piaciPenzugyElemzes.getElozoev_piaci_bef_kamatlab())
				.setParameter(13, piaciPenzugyElemzes.getKonkurencia_piaci_bef_kamatlab())
				.setParameter(14, piaciPenzugyElemzes.getPiaci_konyv_reszveny_arfolyam())
				.setParameter(15, piaciPenzugyElemzes.getPiaci_konyv_sajat_toke())
				.setParameter(16, piaciPenzugyElemzes.getPiaci_konyv_goodwill())
				.setParameter(17, piaciPenzugyElemzes.getPiaci_konyv_imm_javak())
				.setParameter(18, piaciPenzugyElemzes.getPiaci_konyv_reszvenyek_db_szama())
				.setParameter(19, piaciPenzugyElemzes.getTargyev_piaci_konyv())
				.setParameter(20, piaciPenzugyElemzes.getElozoev_piaci_konyv())
				.setParameter(21, piaciPenzugyElemzes.getKonkurencia_piaci_konyv()).setParameter(22, penzugyId).executeUpdate();

	}

	@Override
	@Transactional
	public void addPenzugyiAdatok(Long reszvenyId, PenzugyiAdatok penzugyiAdatok) {
       String penzAdatokPostQuery = "INSERT INTO penzugyi_adatok(`netto_targyev`, `netto_elozo_ev`, `netto_konkurencia`, `netto_q2`, `netto_q3`, `eladott_aru_targyev`, `eladott_aru_elozo_ev`, `eladott_aru_konkurencia`, `eladott_aru_q2`, `eladott_aru_q3`, `tarsasag_targyev`, `tarsasag_elozo_ev`, `tarsasag_konkurencia`, `tarsasag_q2`, `tarsasag_q3`, `kamat_targyev`, `kamat_elozo_ev`, `kamat_konkurencia`, `kamat_q2`, `kamat_q3`, `fizetett_kamatok_targyev`, `fizetett_kamatok_elozo_ev`, `fizetett_kamatok_konkurencia`, `fizetett_kamatok_q2`, `fizetett_kamatok_q3`, `adm_targyev`, `adm_elozo_ev`, `adm_konkurencia`, `adm_q2`, `adm_q3`, `egyeb_targyev`, `egyeb_elozo_ev`, `egyeb_konkurencia`, `egyeb_q2`, `egyeb_q3`, `reszveny_targyev`, `reszveny_elozo_ev`, `reszveny_konkurencia`, `reszveny_q2`, `reszveny_q3`, `osztalek_targyev`, `osztalek_elozo_ev`, `osztalek_konkurencia`, `osztalek_q2`, `osztalek_q3`, `elszamolt_targyev`, `elszamolt_elozo_ev`, `elszamolt_konkurencia`, `elszamolt_q2`, `elszamolt_q3`, `keszpenz_targyev`, `keszpenz_elozo_ev`, `keszpenz_konkurencia`, `keszpenz_q2`, `keszpenz_q3`, `koveteles_vevo_targyev`, `koveteles_vevo_elozo_ev`, `koveteles_vevo_konkurencia`, `koveteles_vevo_q2`, `koveteles_vevo_q3`, `keszlet_targyev`, `keszlet_elozo_ev`, `keszlet_konkurencia`, `keszlet_q2`, `keszlet_q3`, `jovairas_targyev`, `jovairas_elozo_ev`, `jovairas_konkurencia`, `jovairas_q2`, `jovairas_q3`, `imm_targyev`, `imm_elozo_ev`, `imm_konkurencia`, `imm_q2`, `imm_q3`, `forgo_targyev`, `forgo_elozo_ev`, `forgo_konkurencia`, `forgo_q2`, `forgo_q3`, `eszkoz_targyev`, `eszkoz_elozo_ev`, `eszkoz_konkurencia`, `eszkoz_q2`, `eszkoz_q3`, `kot_szallito_targyev`, `kot_szallito_elozo_ev`, `kot_szallito_konkurencia`, `kot_szallito_q2`, `kot_szallito_q3`, `toke_targyev`, `toke_elozo_ev`, `toke_konkurencia`, `toke_q2`, `toke_q3`, `rovid_lejaratu_kot_targyev`, `rovid_lejaratu_kot_elozo_ev`, `rovid_lejaratu_kot_konkurencia`, `rovid_lejaratu_kot_q2`, `rovid_lejaratu_kot_q3`, `ossz_kot_targyev`, `ossz_kot_elozo_ev`, `ossz_kot_konkurencia`, `ossz_kot_q2`, `ossz_kot_q3`, `alkalmazottak_szama_targyev`, `alkalmazottak_szama_elozo_ev`, `alkalmazottak_szama_konkurencia`, `alkalmazottak_szama_q2`, `alkalmazottak_szama_q3`, `reszveny_arfolyam_targyev`, `reszveny_arfolyam_elozo_ev`, `reszveny_arfolyam_konkurencia`, `reszveny_arfolyam_q2`, `reszveny_arfolyam_q3`, `naptar_targyev`, `naptar_elozo_ev`, `naptar_konkurencia`, `naptar_q2`, `naptar_q3`, `hozam_targyev`, `hozam_elozo_ev`, `hozam_konkurencia`, `hozam_q2`, `hozam_q3`, `reszveny_id`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		entityManager.createNativeQuery(penzAdatokPostQuery).setParameter(1, penzugyiAdatok.getNetto_targyev())
				.setParameter(2, penzugyiAdatok.getNetto_elozo_ev()).setParameter(3, penzugyiAdatok.getNetto_konkurencia())
				.setParameter(4, penzugyiAdatok.getNetto_q2()).setParameter(5, penzugyiAdatok.getNetto_q3())
				.setParameter(6, penzugyiAdatok.getEladott_aru_targyev())
				.setParameter(7, penzugyiAdatok.getEladott_aru_elozo_ev())
				.setParameter(8, penzugyiAdatok.getEladott_aru_konkurencia())
				.setParameter(9, penzugyiAdatok.getEladott_aru_q2()).setParameter(10, penzugyiAdatok.getEladott_aru_q3())

				.setParameter(11, penzugyiAdatok.getTarsasag_targyev())
				.setParameter(12, penzugyiAdatok.getTarsasag_elozo_ev())
				.setParameter(13, penzugyiAdatok.getTarsasag_konkurencia())
				.setParameter(14, penzugyiAdatok.getTarsasag_q2()).setParameter(15, penzugyiAdatok.getTarsasag_q3())

				.setParameter(16, penzugyiAdatok.getKamat_targyev()).setParameter(17, penzugyiAdatok.getKamat_elozo_ev())
				.setParameter(18, penzugyiAdatok.getKamat_konkurencia()).setParameter(19, penzugyiAdatok.getKamat_q2())
				.setParameter(20, penzugyiAdatok.getKamat_q3())

				.setParameter(21, penzugyiAdatok.getFizetett_kamatok_targyev())
				.setParameter(22, penzugyiAdatok.getFizetett_kamatok_elozo_ev())
				.setParameter(23, penzugyiAdatok.getFizetett_kamatok_konkurencia())
				.setParameter(24, penzugyiAdatok.getFizetett_kamatok_q2())
				.setParameter(25, penzugyiAdatok.getFizetett_kamatok_q3())

				.setParameter(26, penzugyiAdatok.getAdm_targyev()).setParameter(27, penzugyiAdatok.getAdm_elozo_ev())
				.setParameter(28, penzugyiAdatok.getAdm_konkurencia()).setParameter(29, penzugyiAdatok.getAdm_q2())
				.setParameter(30, penzugyiAdatok.getAdm_q3())

				.setParameter(31, penzugyiAdatok.getEgyeb_targyev()).setParameter(32, penzugyiAdatok.getEgyeb_elozo_ev())
				.setParameter(33, penzugyiAdatok.getEgyeb_konkurencia()).setParameter(34, penzugyiAdatok.getEgyeb_q2())
				.setParameter(35, penzugyiAdatok.getEgyeb_q3())

				.setParameter(36, penzugyiAdatok.getReszveny_targyev())
				.setParameter(37, penzugyiAdatok.getReszveny_elozo_ev())
				.setParameter(38, penzugyiAdatok.getReszveny_konkurencia())
				.setParameter(39, penzugyiAdatok.getReszveny_q2()).setParameter(40, penzugyiAdatok.getReszveny_q3())

				.setParameter(41, penzugyiAdatok.getOsztalek_targyev())
				.setParameter(42, penzugyiAdatok.getOsztalek_elozo_ev())
				.setParameter(43, penzugyiAdatok.getOsztalek_konkurencia())
				.setParameter(44, penzugyiAdatok.getOsztalek_q2()).setParameter(45, penzugyiAdatok.getOsztalek_q3())

				.setParameter(46, penzugyiAdatok.getElszamolt_targyev())
				.setParameter(47, penzugyiAdatok.getElszamolt_elozo_ev())
				.setParameter(48, penzugyiAdatok.getElszamolt_konkurencia())
				.setParameter(49, penzugyiAdatok.getElszamolt_q2()).setParameter(50, penzugyiAdatok.getElszamolt_q3())

				.setParameter(51, penzugyiAdatok.getKeszpenz_targyev())
				.setParameter(52, penzugyiAdatok.getKeszpenz_elozo_ev())
				.setParameter(53, penzugyiAdatok.getKeszpenz_konkurencia())
				.setParameter(54, penzugyiAdatok.getKeszpenz_q2()).setParameter(55, penzugyiAdatok.getKeszpenz_q3())

				.setParameter(56, penzugyiAdatok.getKoveteles_vevo_targyev())
				.setParameter(57, penzugyiAdatok.getKoveteles_vevo_elozo_ev())
				.setParameter(58, penzugyiAdatok.getKoveteles_vevo_konkurencia())
				.setParameter(59, penzugyiAdatok.getKoveteles_vevo_q2())
				.setParameter(60, penzugyiAdatok.getKoveteles_vevo_q3())

				.setParameter(61, penzugyiAdatok.getKeszlet_targyev())
				.setParameter(62, penzugyiAdatok.getKeszlet_elozo_ev())
				.setParameter(63, penzugyiAdatok.getKeszlet_konkurencia())
				.setParameter(64, penzugyiAdatok.getKeszlet_q2()).setParameter(65, penzugyiAdatok.getKeszlet_q3())

				.setParameter(66, penzugyiAdatok.getJovairas_targyev())
				.setParameter(67, penzugyiAdatok.getJovairas_elozo_ev())
				.setParameter(68, penzugyiAdatok.getJovairas_konkurencia())
				.setParameter(69, penzugyiAdatok.getJovairas_q2()).setParameter(70, penzugyiAdatok.getJovairas_q3())

				.setParameter(71, penzugyiAdatok.getImm_targyev()).setParameter(72, penzugyiAdatok.getImm_elozo_ev())
				.setParameter(73, penzugyiAdatok.getImm_konkurencia()).setParameter(74, penzugyiAdatok.getImm_q2())
				.setParameter(75, penzugyiAdatok.getImm_q3())

				.setParameter(76, penzugyiAdatok.getForgo_targyev()).setParameter(77, penzugyiAdatok.getForgo_elozo_ev())
				.setParameter(78, penzugyiAdatok.getForgo_konkurencia()).setParameter(79, penzugyiAdatok.getForgo_q2())
				.setParameter(80, penzugyiAdatok.getForgo_q3())

				.setParameter(81, penzugyiAdatok.getEszkoz_targyev()).setParameter(82, penzugyiAdatok.getEszkoz_elozo_ev())
				.setParameter(83, penzugyiAdatok.getEszkoz_konkurencia()).setParameter(84, penzugyiAdatok.getEszkoz_q2())
				.setParameter(85, penzugyiAdatok.getEszkoz_q3())

				.setParameter(86, penzugyiAdatok.getKot_szallito_targyev())
				.setParameter(87, penzugyiAdatok.getKot_szallito_elozo_ev())
				.setParameter(88, penzugyiAdatok.getKot_szallito_konkurencia())
				.setParameter(89, penzugyiAdatok.getKot_szallito_q2()).setParameter(90, penzugyiAdatok.getKot_szallito_q3())

				.setParameter(91, penzugyiAdatok.getToke_targyev()).setParameter(92, penzugyiAdatok.getToke_elozo_ev())
				.setParameter(93, penzugyiAdatok.getToke_konkurencia()).setParameter(94, penzugyiAdatok.getToke_q2())
				.setParameter(95, penzugyiAdatok.getToke_q3())

				.setParameter(96, penzugyiAdatok.getRovid_lejaratu_kot_targyev())
				.setParameter(97, penzugyiAdatok.getRovid_lejaratu_kot_elozo_ev())
				.setParameter(98, penzugyiAdatok.getRovid_lejaratu_kot_konkurencia())
				.setParameter(99, penzugyiAdatok.getRovid_lejaratu_kot_q2())
				.setParameter(100, penzugyiAdatok.getRovid_lejaratu_kot_q3())

				.setParameter(101, penzugyiAdatok.getOssz_kot_targyev())
				.setParameter(102, penzugyiAdatok.getOssz_kot_elozo_ev())
				.setParameter(103, penzugyiAdatok.getOssz_kot_konkurencia())
				.setParameter(104, penzugyiAdatok.getOssz_kot_q2()).setParameter(105, penzugyiAdatok.getOssz_kot_q3())

				.setParameter(106, penzugyiAdatok.getAlkalmazottak_szama_targyev())
				.setParameter(107, penzugyiAdatok.getAlkalmazottak_szama_elozo_ev())
				.setParameter(108, penzugyiAdatok.getAlkalmazottak_szama_konkurencia())
				.setParameter(109, penzugyiAdatok.getAlkalmazottak_szama_q2())
				.setParameter(110, penzugyiAdatok.getAlkalmazottak_szama_q3())

				.setParameter(111, penzugyiAdatok.getReszveny_arfolyam_targyev())
				.setParameter(112, penzugyiAdatok.getReszveny_arfolyam_elozo_ev())
				.setParameter(113, penzugyiAdatok.getReszveny_arfolyam_konkurencia())
				.setParameter(114, penzugyiAdatok.getReszveny_arfolyam_q2())
				.setParameter(115, penzugyiAdatok.getReszveny_arfolyam_q3())

				.setParameter(116, penzugyiAdatok.getNaptar_targyev())
				.setParameter(117, penzugyiAdatok.getNaptar_elozo_ev())
				.setParameter(118, penzugyiAdatok.getNaptar_konkurencia())
				.setParameter(119, penzugyiAdatok.getNaptar_q2()).setParameter(120, penzugyiAdatok.getNaptar_q3())

				.setParameter(121, penzugyiAdatok.getHozam_targyev()).setParameter(122, penzugyiAdatok.getHozam_elozo_ev())
				.setParameter(123, penzugyiAdatok.getHozam_konkurencia()).setParameter(124, penzugyiAdatok.getHozam_q2())
				.setParameter(125, penzugyiAdatok.getHozam_q3()).setParameter(126, reszvenyId)
				.executeUpdate();

	}

	// Celkitűzés

	@Override
	@Transactional
	public void addCelkituzes(Long id, Celkituzes celkituzes) {
		String postCelkituzesQuery = "INSERT INTO `celkituzes`(`megnevezes`, `datum`, `elerhetoe`, `lepes1`, `lepes2`, `lepes3`, `lepes4`, `lepes5`, `lepes6`, `lepes7`, `lepes8`, `lepes9`, `lepes10`, `cel_igeny`, `cel_oka`, `eleresi_szint`, `elokeszulet_elso`, `elokeszulet_masodik`,`elokeszulet_harmadik`,`elokeszulet_negyedik`, `szukseges_osszeg`, `user_id`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?, ?, ?)";

		entityManager.createNativeQuery(postCelkituzesQuery).setParameter(1, celkituzes.getMegnevezes())
				.setParameter(2, celkituzes.getDatum()).setParameter(3, celkituzes.getElerhetoE())
				.setParameter(4, celkituzes.getLepes1()).setParameter(5, celkituzes.getLepes2())
				.setParameter(6, celkituzes.getLepes3()).setParameter(7, celkituzes.getLepes4())
				.setParameter(8, celkituzes.getLepes5()).setParameter(9, celkituzes.getLepes6())
				.setParameter(10, celkituzes.getLepes7()).setParameter(11, celkituzes.getLepes8())
				.setParameter(12, celkituzes.getLepes9()).setParameter(13, celkituzes.getLepes10())
				.setParameter(14, celkituzes.getCelIgeny()).setParameter(15, celkituzes.getCelOka())
				.setParameter(16, celkituzes.getEleresiSzint()).setParameter(17, celkituzes.getElokeszuletElso())
				.setParameter(18, celkituzes.getElokeszuletMasodik())
				.setParameter(19, celkituzes.getElokeszuletHarmadik())
				.setParameter(20, celkituzes.getElokeszuletNegyedik()).setParameter(21, celkituzes.getSzuksegesOsszeg())
				.setParameter(22, id).executeUpdate();
	}

	@Override
	@Transactional
	public void addFeladatToCelkituzes(Long celkituzesId, Feladat feladat) {
		String postFeladatQuery = "INSERT INTO `feladat`( `title`, `besorolas`, `hatarido`, `color`, `feladat_start`, `feladat_end`, `feladat_type`, `sorszam`,  `utemezett`, `celkituzes_id`) VALUES (?,?,?,?,?,?,?,?,?,?)";

		entityManager.createNativeQuery(postFeladatQuery).setParameter(1, feladat.getTitle())
				.setParameter(2, feladat.getBesorolas()).setParameter(3, feladat.getHatarido())
				.setParameter(4, feladat.getColor()).setParameter(5, feladat.getStart())
				.setParameter(6, feladat.getEnd()).setParameter(7, feladat.getType())
				.setParameter(8, feladat.getSorszam()).setParameter(9, feladat.getUtemezett())
				.setParameter(10, celkituzesId).executeUpdate();

	}

	@Override
	@Transactional
	public void addAkadalyToCelkituzes(Long celkituzesId, Akadaly akadaly) {
		String postAkadalyQuery = "INSERT INTO `akadaly`( `title`, `celkituzes_id`) VALUES (?,?)";

		entityManager.createNativeQuery(postAkadalyQuery).setParameter(1, akadaly.getTitle())
				.setParameter(2, celkituzesId).executeUpdate();

	}

	@Override
	@Transactional
	public void updateCelkituzes(Long userId, Celkituzes celkituzes) {

		String updateCelkituzes = "UPDATE `celkituzes` SET `megnevezes`=?,datum=?,`elerhetoe`=?,`lepes1`=?,`lepes2`=?,`lepes3`=?,`lepes4`=?,`lepes5`=?,`lepes6`=?,`lepes7`=?,`lepes8`=?,`lepes9`=?,`lepes10`=?,`user_id`=?,`cel_igeny`=?,`cel_oka`=?,`eleresi_szint`=?,`elokeszulet_elso`=?, `elokeszulet_masodik`=?,`elokeszulet_harmadik`=?,`elokeszulet_negyedik`=?,`szukseges_osszeg`=? WHERE id=?";

		entityManager.createNativeQuery(updateCelkituzes).setParameter(1, celkituzes.getMegnevezes())
				.setParameter(2, celkituzes.getDatum()).setParameter(3, celkituzes.getElerhetoE())
				.setParameter(4, celkituzes.getLepes1()).setParameter(5, celkituzes.getLepes2())
				.setParameter(6, celkituzes.getLepes3()).setParameter(7, celkituzes.getLepes4())
				.setParameter(8, celkituzes.getLepes5()).setParameter(9, celkituzes.getLepes6())
				.setParameter(10, celkituzes.getLepes7()).setParameter(11, celkituzes.getLepes8())
				.setParameter(12, celkituzes.getLepes9()).setParameter(13, celkituzes.getLepes10())
				.setParameter(14, userId).setParameter(15, celkituzes.getCelIgeny())
				.setParameter(16, celkituzes.getCelOka()).setParameter(17, celkituzes.getEleresiSzint())
				.setParameter(18, celkituzes.getElokeszuletElso()).setParameter(19, celkituzes.getElokeszuletMasodik())
				.setParameter(20, celkituzes.getElokeszuletHarmadik())
				.setParameter(21, celkituzes.getElokeszuletNegyedik()).setParameter(22, celkituzes.getSzuksegesOsszeg())
				.setParameter(23, celkituzes.getId()).executeUpdate();

	}

	@Override
	@Transactional
	public void updateFeladat(Long celkituzesId, Feladat feladat) {
		String updateFeladatQuery = "UPDATE `feladat` SET `title`=?,`besorolas`=?,`hatarido`=?,`color`=?,`feladat_start`=?,`feladat_end`=?,`feladat_type`=?,`sorszam`=?,`utemezett`=?,`celkituzes_id`=? WHERE id=?";

		entityManager.createNativeQuery(updateFeladatQuery).setParameter(1, feladat.getTitle())
				.setParameter(2, feladat.getBesorolas()).setParameter(3, feladat.getHatarido())
				.setParameter(4, feladat.getColor()).setParameter(5, feladat.getStart())
				.setParameter(6, feladat.getEnd()).setParameter(7, feladat.getType())
				.setParameter(8, feladat.getSorszam()).setParameter(9, feladat.getUtemezett())
				.setParameter(10, celkituzesId).setParameter(11, feladat.getId()).executeUpdate();
	}

	@Override
	@Transactional
	public void deleteCelkituzes(Long userId) {

		String deleteFeladatQuery = "DELETE FROM feladat";
		String deleteAkadalyQuery = "DELETE FROM akadaly";
		String deleteCelkituzesQuery = "DELETE FROM celkituzes WHERE user_id=?";

		entityManager.createNativeQuery(deleteFeladatQuery).executeUpdate();
		entityManager.createNativeQuery(deleteAkadalyQuery).executeUpdate();
		entityManager.createNativeQuery(deleteCelkituzesQuery).setParameter(1, userId).executeUpdate();

	}

	@Override
	public List<Feladat> getFeladatok(Long celkituzesId) {
		final JPAQuery<Feladat> jpaQuery = new JPAQuery<Feladat>(entityManager);
		QFeladat feladat = QFeladat.feladat;

		return jpaQuery.from(feladat).select(feladat).where(feladat.feladatCelkituzes.id.eq(celkituzesId)).fetch();

	}

	@Override
	@Transactional
	public int deleteFeladat(Long feladatId) {
		String deleteFeladatQuery = "DELETE FROM feladat WHERE id=?";
		int numberOfDeleted = 0;

		numberOfDeleted = entityManager.createNativeQuery(deleteFeladatQuery).setParameter(1, feladatId)
				.executeUpdate();
		if (numberOfDeleted > 0)
			LOGGER.info("Feladat megtalálva");

		return numberOfDeleted;

	}

	@Override
	public Long getReszvenyId(Long userId) {
		final JPAQuery<Reszveny> jpaQuery = new JPAQuery<Reszveny>(entityManager);
		QReszveny reszveny = QReszveny.reszveny;

		return null; //jpaQuery.from(reszveny).select(reszveny).where(reszveny.id.eq(userId)).fetchOne();
	}

}
