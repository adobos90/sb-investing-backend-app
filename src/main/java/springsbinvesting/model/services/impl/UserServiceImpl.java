package springsbinvesting.model.services.impl;

import java.util.List;
import java.util.Optional;

import javax.jdo.annotations.Transactional;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import springsbinvesting.domain.Reszveny;
import springsbinvesting.domain.User;
import springsbinvesting.domain.celkituzes.Akadaly;
import springsbinvesting.domain.celkituzes.Celkituzes;
import springsbinvesting.domain.celkituzes.Feladat;
import springsbinvesting.domain.penzugy.*;
import springsbinvesting.domain.reszvenyelemzes.Celar;
import springsbinvesting.domain.reszvenyelemzes.Manageles;
import springsbinvesting.domain.reszvenyelemzes.MentalisElemzes;
import springsbinvesting.domain.reszvenyelemzes.NettoJelenErtek;
import springsbinvesting.domain.reszvenyelemzes.PenzugyiAdatok;
import springsbinvesting.domain.reszvenyelemzes.VallalatKockElemzes;
import springsbinvesting.domain.reszvenyelemzes.VallalatPenzugyiElemzes;
import springsbinvesting.domain.reszvenyelemzes.manageles.ManagelesBefAdatok;
import springsbinvesting.domain.reszvenyelemzes.manageles.ManagelesReszveny;
import springsbinvesting.domain.reszvenyelemzes.manageles.ManagelesiStrategia;
import springsbinvesting.domain.reszvenyelemzes.manageles.MenBefMenegeles;
import springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.PenzugyiElemzesHatekonysag;
import springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.PenzugyiElemzesHitel;
import springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.PenzugyiElemzesJovedelem;
import springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.PenzugyiElemzesPiaci;
import springsbinvesting.domain.strategia.Strategia;
import springsbinvesting.domain.strategia.StrategiaReszveny;
import springsbinvesting.model.repositories.api.ICelkituzesRepository;
import springsbinvesting.model.repositories.api.IPenzugyRepository;
import springsbinvesting.model.repositories.api.IStrategiaRepository;
import springsbinvesting.model.repositories.impl.UserRepository;
import springsbinvesting.model.services.api.IUserService;

@Service
public class UserServiceImpl implements IUserService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ICelkituzesRepository celkituzesRepository;
	
	@Autowired
	private IPenzugyRepository penzugyRepository;

	@Autowired
	private IStrategiaRepository strategiaRepository;
		
	public UserServiceImpl() {}

	@Override
	public List<User> findAll() {
		
		return userRepository.findAllUserWithReszvenyek();
	}

	@Override
	public User findUserById(Long id) {
		
		return userRepository.findUserWithReszvenyekById(id);
	}

	@Override
	public User findUserByEmail(String email) {
		
		return userRepository.findUserWithReszvenyekByEmail(email);
		}

	@Override
	@Transactional
	public void saveUser(User user) {
		userRepository.addUser(user);
	}

	@Override
	public Reszveny addReszveny(Long id,Reszveny reszveny) {
		return userRepository.addReszveny(id,reszveny);
	}

	@Override
	public User updateUser(Long userId, User user) {
		return userRepository.updateUser(userId, user);
	}

	@Override
	public List<Reszveny> getReszvenyek(Long userId) {
		return userRepository.getReszvenyek(userId);
	}

	@Override
	public Reszveny getReszvenyById(Long reszvenyId) {
		return userRepository.getReszvenyById(reszvenyId);
	}

	@Override
	public MentalisElemzes getMentalisByReszvenyId(Long reszvenyId) {
		return userRepository.getMentalisByReszvenyId(reszvenyId);
	}

	@Override
	public VallalatKockElemzes getVallKockElemzes(Long reszvenyId) {
		return userRepository.getVallKockElemzes(reszvenyId);
	}

	@Override
	public PenzugyiAdatok getPenzugyiAdatok(Long reszvenyId) {
		return userRepository.getPenzugyiAdatok(reszvenyId);
	}

	@Override
	public VallalatPenzugyiElemzes getVallPenzElemzes(Long reszvenyId) {
		return userRepository.getVallPenzElemzes(reszvenyId);
	}

	@Override
	public Celar getCelar(Long reszvenyId) {
		return userRepository.getCelar(reszvenyId);
	}

	@Override
	public NettoJelenErtek getNettoJelenertek(Long reszvenyId) {
		return userRepository.getNettoJelenertek(reszvenyId);
	}

	@Override
	public Manageles getManageles(Long reszvenyId) {
		return userRepository.getManageles(reszvenyId);
	}


	//// ********PUT REQUESTS  ********

	@Override
	public Reszveny updateReszveny(Long reszvenyId, Reszveny newReszveny) {
		return userRepository.updateReszveny(reszvenyId, newReszveny);
	}

	@Override
	public MentalisElemzes updateMentalisElemzes(Long reszvenyId, MentalisElemzes mentalisElemzes) {
		return userRepository.updateMentalisElemzes(reszvenyId, mentalisElemzes);
	}

	@Override
	public VallalatKockElemzes updateVallKockElemzes(Long reszvenyId, VallalatKockElemzes kockElemzes) {
		return userRepository.updateVallKockElemzes(reszvenyId, kockElemzes);
	}

	@Override
	public PenzugyiAdatok updatePenzugyiAdatok(Long reszvenyId, PenzugyiAdatok penzugyiAdatok) {
		return userRepository.updatePenzugyiAdatok(reszvenyId, penzugyiAdatok);
	}

	@Override
	public VallalatPenzugyiElemzes updatePenzugyielemzes(Long reszvenyId, VallalatPenzugyiElemzes vallalatPenzugyiElemzes) {
		return userRepository.updatePenzugyielemzes(reszvenyId, vallalatPenzugyiElemzes);
	}

	@Override
	public void updateJovToPenzugyiElemzes(Long penzelemzesId, PenzugyiElemzesJovedelem jovPenzugyElemzes) {
		userRepository.updateJovToPenzugyiElemzes(penzelemzesId, jovPenzugyElemzes);
	}

	@Override
	public void updateHitelToPenzugyiElemzes(Long penzelemzesId, PenzugyiElemzesHitel hitelPenzugyElemzes) {
		userRepository.updateHitelToPenzugyiElemzes(penzelemzesId, hitelPenzugyElemzes);
	}

	@Override
	public void updateHatToPenzugyiElemzes(Long penzelemzesId, PenzugyiElemzesHatekonysag hatPenzugyElemzes) {
		userRepository.updateHatToPenzugyiElemzes(penzelemzesId, hatPenzugyElemzes);
	}

	@Override
	public void updatePiaciToPenzugyiElemzes(Long penzelemzesId, PenzugyiElemzesPiaci piaciPenzugyElemzes) {
		userRepository.updatePiaciToPenzugyiElemzes(penzelemzesId, piaciPenzugyElemzes);
	}

	@Override
	public Celar updateCelar(Long reszvenyId, Celar celar) {
		return userRepository.updateCelar(reszvenyId, celar);
	}

	@Override
	public NettoJelenErtek updateNettoJelenertek(Long reszvenyId, NettoJelenErtek nettoJelenErtek) {
		return userRepository.updateNettoJelenertek(reszvenyId, nettoJelenErtek);
	}

	@Override
	public ManagelesBefAdatok updateManagelesAdatok(Long managelesId, ManagelesBefAdatok managelesBefAdatok) {
		return userRepository.updateManagelesAdatok(managelesId, managelesBefAdatok);
	}

	@Override
	public ManagelesReszveny updateManagelesReszveny(Long managelesId, ManagelesReszveny managelesReszveny) {
		return userRepository.updateManagelesReszveny(managelesId, managelesReszveny);
	}

	@Override
	public ManagelesiStrategia updateManagelesStrat(Long managelesId, ManagelesiStrategia managelesiStrategia) {
		return userRepository.updateManagelesStrat(managelesId, managelesiStrategia);
	}

	@Override
	public MenBefMenegeles updateBefMenManageles(Long managelesId,   Long id,  MenBefMenegeles menBefMenegeles) {
		return userRepository.updateBefMenManageles(managelesId, id,menBefMenegeles);
	}


	// **** POST REQUESTS  *****

	@Override
	public void addMentalis(Long reszvenyId,MentalisElemzes mentalis) {
		userRepository.addMentalisElemzes(reszvenyId,mentalis);
	}

	@Override
	public void addVallKockElemzes(Long reszvenyId, VallalatKockElemzes vallKockElemzes) {
		userRepository.addVallalatKockElemzes(reszvenyId, vallKockElemzes);
	}

	@Override
	public void addCelar(Long reszvenyId, Celar celar) {
		userRepository.addCelar(reszvenyId, celar);
	}

	@Override
	public void addNettoJelenErtek(Long reszvenyId, NettoJelenErtek netto) {
		userRepository.addNettoJelenErtek(reszvenyId, netto);
	}

	@Override
	public void addManageles(Long reszvenyId, Manageles manageles) {
		userRepository.addManageles(reszvenyId, manageles);
	}

	@Override
	public void addReszvenyToManageles(Long managelesId, ManagelesReszveny menReszveny) {
		userRepository.addReszvenyToManageles(managelesId, menReszveny);
	}

	@Override
	public void addStrategiaToManageles(Long managelesId, ManagelesiStrategia menStrat) {
		 userRepository.addStrategiaToManageles(managelesId, menStrat);
		
	}

	@Override
	public void addMenBefMenToManageles(Long managelesId, MenBefMenegeles menBefMen) {
		userRepository.addMenBefMenToManageles(managelesId, menBefMen);
		
	}

	@Override
	public void addBefAdatokToManageles(Long managelesId, ManagelesBefAdatok menBefadatok) {
		userRepository.addBefAdatokToManageles(managelesId, menBefadatok);
		
	}

	@Override
	public void addPenzugyiElemzes(Long reszvenyId, VallalatPenzugyiElemzes penzugyiElemzes) {
		userRepository.addPenzugyiElemzes(reszvenyId, penzugyiElemzes);
		
	}

	@Override
	public void addJovToPenzugyiElemzes(Long penzugyId, PenzugyiElemzesJovedelem jovPenzugyElemzes) {
		userRepository.addJovToPenzugyiElemzes(penzugyId, jovPenzugyElemzes);
	}

	@Override
	public void addHatToPenzugyiElemzes(Long penzugyId, PenzugyiElemzesHatekonysag hatPenzugyElemzes) {
		userRepository.addHatToPenzugyiElemzes(penzugyId, hatPenzugyElemzes);
	}

	@Override
	public void addHitelToPenzugyiElemzes(Long penzugyId, PenzugyiElemzesHitel hitelPenzugyElemzes) {
		userRepository.addHitelToPenzugyiElemzes(penzugyId, hitelPenzugyElemzes);
	}

	@Override
	public void addPiaciToPenzugyiElemzes(Long penzugyId, PenzugyiElemzesPiaci piaciPenzugyElemzes) {
		userRepository.addPiaciToPenzugyiElemzes(penzugyId, piaciPenzugyElemzes);
	}

	@Override
	public void addPenzugyiAdatok(Long reszvenyId, PenzugyiAdatok penzugyiAdatok) {
		userRepository.addPenzugyiAdatok(reszvenyId, penzugyiAdatok);
		
	}

	@Override
	public void deleteReszveny(Long id) {
		userRepository.deleteReszveny(id);
	}

	@Override
	public void deleteMenBefMen(Long id, Long managelesId) {
		userRepository.deleteMenBefMen(id, managelesId);
	}

	@Override
	public void addCelkituzes(Long id, Celkituzes celkituzes) {
		userRepository.addCelkituzes(id, celkituzes);
		
	}

	@Override
	public void addFeladatToCelkituzes(Long celkituzesId, Feladat feladat) {
		userRepository.addFeladatToCelkituzes(celkituzesId, feladat);
		
	}

	@Override
	public void addAkadalyToCelkituzes(Long celkituzesId, Akadaly akadaly) {
		userRepository.addAkadalyToCelkituzes(celkituzesId, akadaly);
		
	}

	@Override
	public void updateCelkituzes(Long userId, Celkituzes celkituzes) {
		userRepository.updateCelkituzes(userId, celkituzes);
		
	}
	

	@Override
	public Long getCelkituzesId(Long id) {
		return celkituzesRepository.findByUserId(id);
	}

	@Override
	public Celkituzes getCelkituzes(Long userId) {
		return celkituzesRepository.findById(userId);
	}

	@Override
	public void updateFeladat(Long celkituzesId, Feladat feladat) {
		 userRepository.updateFeladat(celkituzesId, feladat);
		
	}

	@Override
	public void deleteCelkituzes(Long userId) {
		userRepository.deleteCelkituzes(userId);
		
	}

	@Override
	public List<Feladat> getFeladatok(Long celkituzesId) {
		
		return userRepository.getFeladatok(celkituzesId);
	}

	@Override
	public int deleteFeladat(Long feladatId) {
		
		return userRepository.deleteFeladat(feladatId);
	}

	@Override
	public List<Penzugy> findAllPenzugy(String userId) {
		return penzugyRepository.findAllPenzugy(userId);
	}



	@Override
	public Long addPenzugy(Long id,Penzugy penzugy) {
		return penzugyRepository.addPenzugy(id, penzugy);
	}

	@Override
	public void addBevetelekToHonap(Long honapId, BevetelPenzugy bevKiadasPenzugy) {
		penzugyRepository.addBevetelekToHonap(honapId, bevKiadasPenzugy);
		
	}

	@Override
	public void addKiadasokToHonap(Long honapId, KiadasPenzugy kiadasPenzugy) {
		penzugyRepository.addKiadasokToHonap(honapId, kiadasPenzugy);
		
	}

	@Override
	public Long findPenzugyIdByEv(String ev) {
		
		return penzugyRepository.findPenzugyIdByEv(ev);
	}

	@Override
	public Long findBevKiadIbByPenzugyId(Long penzugyId, String honap) {
		
		return penzugyRepository.findBevKiadIbByPenzugyId(penzugyId, honap);
	}

	@Override
	public void deleteBevetelInPenzugy(Long honapId) {
		 penzugyRepository.deleteBevetelInPenzugy(honapId);
	}

	

	@Override
	public void deleteKiadasInPenzugy(Long honapId, int nap) {
		penzugyRepository.deleteKiadasInPenzugy(honapId, nap);
		
	}

	@Override
	public void updateBevetelInPenzugy(Long honapId, Long bevetel) {
		penzugyRepository.updateBevetelInPenzugy(honapId, bevetel);
		
	}

	@Override
	public void addHonap(Long penzugyId, Honap honap) {
		penzugyRepository.addHonap(penzugyId, honap);
		
	}

	@Override
	public List<Honap> findHonapByPenzugyiEv(String penzugyiEv) {
		return penzugyRepository.findHonapByPenzugyiEv(penzugyiEv);
	}

	@Override
	public BevetelPenzugy findBevetelekByHonapId(String year, String honap) {
		return penzugyRepository.findBevetelekByHonapId(year, honap);
	}

	@Override
	public List<KiadasPenzugy> findKiadasokByYearsAndMonths(String year, String honap) {
		return penzugyRepository.findKiadasokByYearsAndMonths(year, honap);
	}

	@Override
	public KiadasPenzugy findKiadasokByHonapIdandNap(Long honapId, int nap) {
		return penzugyRepository.findKiadasokByHonapIdandNap(honapId,nap);
	}

	@Override
	public List<BevetelPenzugy> getOsszesBevetel() {
		return penzugyRepository.getOsszesBevetel();
	}

	@Override
	public List<KiadasPenzugy> getOsszesKiadas() {
		return penzugyRepository.getOsszesKiadas();
	}



	@Override
	public void updateHonapZarasa(Long yearId, int honap, int zarva) {
		 penzugyRepository.updateHonapZarasa(yearId, honap,zarva);
	}

	@Override
	public void addCelToPenzugy(Long userId, CelokPenzugy celokPenzugy) {
		penzugyRepository.addCelToPenzugy(userId, celokPenzugy);
	}

	@Override
	public CelokPenzugy findCelokPenzugyById(Long userId) {
		return penzugyRepository.findCelokPenzugyById(userId);
	}

	@Override
	public void deleteCelokPenzugy(Long userId) {
		penzugyRepository.deleteCelokPenzugy(userId);
	}

	@Override
	public void addStrategiaItem(Long userId, Strategia strategia) {
		strategiaRepository.addStrategiaItem(userId, strategia);
	}

	@Override
	public List<Strategia> findAllStrategiaItemByFlag(Long userId, String flag) {
		return strategiaRepository.findAllStrategiaItemByFlag(userId, flag);
	}

	@Override
	public void addStrategiaReszveny(Long userId, StrategiaReszveny strategiaReszveny) {
		strategiaRepository.addStrategiaReszveny(userId, strategiaReszveny);
	}

	@Override
	public List<StrategiaReszveny> getAllStratReszveny(Long userId) {
		return strategiaRepository.getAllStratReszveny(userId);
	}


}
