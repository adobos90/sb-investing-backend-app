package springsbinvesting.domain;

import java.util.Date;
import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import springsbinvesting.domain.celkituzes.Celkituzes;
import springsbinvesting.domain.penzugy.CelokPenzugy;
import springsbinvesting.domain.penzugy.Penzugy;
import springsbinvesting.domain.strategia.Strategia;
import springsbinvesting.domain.strategia.StrategiaReszveny;

@Entity
public class User {

	@Column(name = "id")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "firstname")
	private String firstName;
	
	@Column(name = "lastname")
	private String lastName;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "passw")
	private String passw;
	
	@Column(name = "birthdate")
	private Date birthDate;

	@Lob
	@Column(length=100000)
	private byte[] photo;
	
	@OneToMany(mappedBy = "reszvenyUser", cascade = CascadeType.ALL)
	@Column(nullable = true)
	@JsonManagedReference
	private Set<Reszveny> reszvenyek;
	
	@OneToMany(mappedBy = "celkituzesUser", cascade = CascadeType.ALL)
	@Column(nullable = true)
	@JsonManagedReference
	private Set<Celkituzes> celkituzesk;
	
	@OneToMany(mappedBy = "penzugyUser", cascade = CascadeType.ALL)
	@Column(nullable = true)
	@JsonManagedReference
	private Set<Penzugy> penzugyek;

	@OneToMany(mappedBy = "celokPenzugyUser", cascade = CascadeType.ALL)
	@Column(nullable = true)
	@JsonManagedReference
	private Set<CelokPenzugy> celokpenzugyek;

	@OneToMany(mappedBy = "strategiaUser", cascade = CascadeType.ALL)
	@Column(nullable = true)
	@JsonManagedReference
	private Set<Strategia> strategiaUser;

	@OneToMany(mappedBy = "stReszvenyUser", cascade = CascadeType.ALL)
	@Column(nullable = true)
	@JsonManagedReference
	private Set<StrategiaReszveny> stReszvenyUser;


	public byte[] getPhoto() {
		return photo;
	}

	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	

	public String getPassw() {
		return passw;
	}

	public void setPassw(String passw) {
		this.passw = passw;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Set<Reszveny> getReszvenyek() {
		return reszvenyek;
	}

	public void setReszvenyek(Set<Reszveny> reszvenyek) {
		this.reszvenyek = reszvenyek;
	}

	public Set<Celkituzes> getCelkituzesk() {
		return celkituzesk;
	}

	public void setCelkituzesk(Set<Celkituzes> celkituzesk) {
		this.celkituzesk = celkituzesk;
	}

	public Set<Penzugy> getPenzugyek() {
		return penzugyek;
	}

	public void setPenzugyek(Set<Penzugy> penzugyek) {
		this.penzugyek = penzugyek;
	}

	public Set<CelokPenzugy> getCelokpenzugyek() {
		return celokpenzugyek;
	}

	public void setCelokpenzugyek(Set<CelokPenzugy> celokpenzugyek) {
		this.celokpenzugyek = celokpenzugyek;
	}

	public Set<Strategia> getStrategiaUser() {
		return strategiaUser;
	}

	public void setStrategiaUser(Set<Strategia> strategiaUser) {
		this.strategiaUser = strategiaUser;
	}

	public Set<StrategiaReszveny> getStReszvenyUser() {
		return stReszvenyUser;
	}

	public void setStReszvenyUser(Set<StrategiaReszveny> stReszvenyUser) {
		this.stReszvenyUser = stReszvenyUser;
	}
}
