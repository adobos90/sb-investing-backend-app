package springsbinvesting.domain.penzugy;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "bevetelpenzugy")
public class BevetelPenzugy {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "bevetel")
	private long bevetel;

	@Column(name = "ev")
	private String ev;

	@Column(name = "ho")
	private int honap;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "honapid")
	@JsonBackReference
	private Honap bevetelHonap;


	public BevetelPenzugy() {
		
	}

	public String getEv() {
		return ev;
	}

	public void setEv(String ev) {
		this.ev = ev;
	}

	public int getHonap() {
		return honap;
	}

	public void setHonap(int honap) {
		this.honap = honap;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public long getBevetel() {
		return this.bevetel;
	}

	public void setBevetel(long bevetel) {
		this.bevetel = bevetel;
	}

	public Honap getBevetelHonap() {
		return this.bevetelHonap;
	}

	public void setBevetelHonap(Honap bevetelHonap) {
		this.bevetelHonap = bevetelHonap;
	}

	
	
}
