package springsbinvesting.domain.penzugy;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name= "honappenzugy")
public class Honap {
    
    @Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
    private Long id;

	@Column(name = "honap")
    private int honap;

	@Column(name = "zarva")
    private int zarva;

    @OneToMany(mappedBy = "bevetelHonap", cascade = CascadeType.ALL)
	@Column(nullable = true)
	@JsonManagedReference
	private Set<BevetelPenzugy> bevetelHonap;

    @OneToMany(mappedBy = "kiadHonap", cascade = CascadeType.ALL)
	@Column(nullable = true)
	@JsonManagedReference
	private Set<KiadasPenzugy> kiadHonap;

    @ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "PENZUGYID")
	@JsonBackReference
	private Penzugy honapPenzugy;


    public Honap(){

    }

    // Getters, setters


    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getHonap() {
        return this.honap;
    }

    public void setHonap(int honap) {
        this.honap = honap;
    }

    public int getZarva() {
        return this.zarva;
    }

    public void setZarva(int zarva) {
        this.zarva = zarva;
    }

    public Set<BevetelPenzugy> getBevetelHonap() {
        return this.bevetelHonap;
    }

    public void setBevetelHonap(Set<BevetelPenzugy> bevetelHonap) {
        this.bevetelHonap = bevetelHonap;
    }

    public Set<KiadasPenzugy> getKiadHonap() {
        return this.kiadHonap;
    }

    public void setKiadHonap(Set<KiadasPenzugy> kiadHonap) {
        this.kiadHonap = kiadHonap;
    }

    public Penzugy getHonapPenzugy() {
        return this.honapPenzugy;
    }

    public void setHonapPenzugy(Penzugy honapPenzugy) {
        this.honapPenzugy = honapPenzugy;
    }
   
}
