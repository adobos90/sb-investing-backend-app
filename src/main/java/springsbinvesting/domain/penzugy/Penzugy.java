package springsbinvesting.domain.penzugy;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import springsbinvesting.domain.User;
import springsbinvesting.domain.reszvenyelemzes.Celar;


@Entity
@Table(name = "penzugy")
public class Penzugy {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	
	@OneToMany(mappedBy = "honapPenzugy", cascade = CascadeType.ALL)
	@Column(nullable = true)
	@JsonManagedReference
	private Set<Honap> honapPenzugy;

	@Column(name = "ev")
	private String ev;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "USER_ID")
	@JsonBackReference
	private User penzugyUser;
	
	public Penzugy() {}
	
	// GETTERS, SETTERS


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEv() {
		return ev;
	}

	public void setEv(String ev) {
		this.ev = ev;
	}

	

	public User getPenzugyUser() {
		return penzugyUser;
	}

	public void setPenzugyUser(User penzugyUser) {
		this.penzugyUser = penzugyUser;
	}

	public void setHonapPenzugy(Set<Honap> honapPenzugy){
		this.honapPenzugy = honapPenzugy;
	}

	public Set<Honap> getHonapPenzugy(){
		return this.honapPenzugy;
	}

	
}
