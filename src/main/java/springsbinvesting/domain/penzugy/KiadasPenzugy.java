package springsbinvesting.domain.penzugy;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;


@Entity
@Table(name = "kiadaspenzugy")
public class KiadasPenzugy {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "nap")
	private int nap;
	
	@Column(name = "rezsi")
	private long rezsi;
	
	@Column(name = "megtakaritas")
	private long megtakaritas;
	
	@Column(name = "elvezeti")
	private long elvezeti;
	
	@Column(name = "uti")
	private long uti;
	
	@Column(name = "fogy")
	private long fogy;
	
	@Column(name = "ruhazkodas")
	private long ruhazkodas;
	
	@Column(name = "egyeb")
	private long egyeb;

	@Column(name = "ev")
	private String ev;

	@Column(name = "ho")
	private int ho;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "HONAPID")
	@JsonBackReference
	private Honap kiadHonap;


	public KiadasPenzugy() {
	
	}


	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getNap() {
		return this.nap;
	}

	public void setNap(int nap) {
		this.nap = nap;
	}

	public long getRezsi() {
		return this.rezsi;
	}

	public void setRezsi(long rezsi) {
		this.rezsi = rezsi;
	}

	public long getMegtakaritas() {
		return this.megtakaritas;
	}

	public void setMegtakaritas(long megtakaritas) {
		this.megtakaritas = megtakaritas;
	}

	public long getElvezeti() {
		return this.elvezeti;
	}

	public void setElvezeti(long elvezeti) {
		this.elvezeti = elvezeti;
	}

	public long getUti() {
		return this.uti;
	}

	public void setUti(long uti) {
		this.uti = uti;
	}

	public long getFogy() {
		return this.fogy;
	}

	public void setFogy(long fogy) {
		this.fogy = fogy;
	}

	public long getRuhazkodas() {
		return this.ruhazkodas;
	}

	public void setRuhazkodas(long ruhazkodas) {
		this.ruhazkodas = ruhazkodas;
	}

	public long getEgyeb() {
		return this.egyeb;
	}

	public void setEgyeb(long egyeb) {
		this.egyeb = egyeb;
	}

	public Honap getKiadHonap() {
		return this.kiadHonap;
	}

	public void setKiadHonap(Honap kiadHonap) {
		this.kiadHonap = kiadHonap;
	}


	public String getEv() {
		return ev;
	}

	public void setEv(String ev) {
		this.ev = ev;
	}

	public int getHo() {
		return ho;
	}

	public void setHo(int ho) {
		this.ho = ho;
	}
}
