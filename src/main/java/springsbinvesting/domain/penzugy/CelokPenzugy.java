package springsbinvesting.domain.penzugy;

import com.fasterxml.jackson.annotation.JsonBackReference;
import springsbinvesting.domain.User;

import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "celokpenzugy")
public class CelokPenzugy {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "megnevezes")
	private String megnevezes;
	
	@Column(name = "osszeg")
	private long osszeg;

	@Column(name = "idotartam")
	private int idotartam;
	
	@Column(name = "kezdet")
	private String kezdet;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "USER_ID")
	@JsonBackReference
	private User celokPenzugyUser;

	public CelokPenzugy() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMegnevezes() {
		return megnevezes;
	}

	public void setMegnevezes(String megnevezes) {
		this.megnevezes = megnevezes;
	}

	public long getOsszeg() {
		return osszeg;
	}

	public void setOsszeg(long osszeg) {
		this.osszeg = osszeg;
	}

	public int getIdotartam() {
		return idotartam;
	}

	public void setIdotartam(int idotartam) {
		this.idotartam = idotartam;
	}

	public String getKezdet() {
		return kezdet;
	}

	public void setKezdet(String kezdet) {
		this.kezdet = kezdet;
	}

	public User getCelokPenzugyUser() {
		return celokPenzugyUser;
	}

	public void setCelokPenzugyUser(User celokPenzugyUser) {
		this.celokPenzugyUser = celokPenzugyUser;
	}
}
