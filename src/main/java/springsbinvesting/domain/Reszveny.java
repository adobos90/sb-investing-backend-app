package springsbinvesting.domain;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import springsbinvesting.domain.reszvenyelemzes.Celar;
import springsbinvesting.domain.reszvenyelemzes.Manageles;
import springsbinvesting.domain.reszvenyelemzes.MentalisElemzes;
import springsbinvesting.domain.reszvenyelemzes.NettoJelenErtek;
import springsbinvesting.domain.reszvenyelemzes.PenzugyiAdatok;
import springsbinvesting.domain.reszvenyelemzes.VallalatKockElemzes;
import springsbinvesting.domain.reszvenyelemzes.VallalatPenzugyiElemzes;

@Entity
public class Reszveny {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String vallalat;
	
	private String ticker;
	
	private Date datum;
	
	private String agazat;
	
	private String strategia;
	
	private String status;

	private int kitolt;

	private String timestamp;
	
	// Foreign key
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "userid")
	@JsonBackReference
	private User reszvenyUser;
	
	@OneToMany(mappedBy = "mentalisReszveny", cascade = CascadeType.ALL)
	@Column(nullable = true)
	@JsonManagedReference
	private Set<MentalisElemzes> mentalisElemzes;
	
	@OneToMany(mappedBy = "vallalatKockElemzesReszveny", cascade = CascadeType.ALL)
	@Column(nullable = true)
	@JsonManagedReference
	private Set<VallalatKockElemzes> vallalatKockElemzes;
	
	@OneToMany(mappedBy = "celarReszveny", cascade = CascadeType.ALL)
	@Column(nullable = true)
	@JsonManagedReference
	private Set<Celar> celar;
	
	@OneToMany(mappedBy = "nettoReszveny", cascade = CascadeType.ALL)
	@Column(nullable = true)
	@JsonManagedReference
	private Set<NettoJelenErtek> nettoJelenertek;
	
	@OneToMany(mappedBy = "managelesReszveny", cascade = CascadeType.ALL)
	@Column(nullable = true)
	@JsonManagedReference
	private Set<Manageles> managelesReszveny;
	
	@OneToMany(mappedBy = "penzugyReszveny", cascade = CascadeType.ALL)
	@Column(nullable = true)
	@JsonManagedReference
	private Set<VallalatPenzugyiElemzes> penzugyReszveny;

	
	@OneToMany(mappedBy = "penzAdatokReszveny", cascade = CascadeType.ALL)
	@Column(nullable = true)
	@JsonManagedReference
	private Set<PenzugyiAdatok> penzAdatokReszveny;
	
	public Set<MentalisElemzes> getMentalisElemzes() {
		return mentalisElemzes;
	}

	public void setMentalisElemzes(Set<MentalisElemzes> mentalisElemzes) {
		this.mentalisElemzes = mentalisElemzes;
	}
	

	public Set<VallalatKockElemzes> getVallalatKockElemzes() {
		return vallalatKockElemzes;
	}

	public void setVallalatKockElemzes(Set<VallalatKockElemzes> vallalatKockElemzes) {
		this.vallalatKockElemzes = vallalatKockElemzes;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public int getKitolt() {
		return kitolt;
	}

	public void setKitolt(int kitolt) {
		this.kitolt = kitolt;
	}

	public Set<Celar> getCelar() {
		return celar;
	}

	public void setCelar(Set<Celar> celar) {
		this.celar = celar;
	}
	
	public Set<NettoJelenErtek> getNettoJelenertek() {
		return nettoJelenertek;
	}

	public void setNettoJelenertek(Set<NettoJelenErtek> nettoJelenertek) {
		this.nettoJelenertek = nettoJelenertek;
	}
	
	public Set<PenzugyiAdatok> getPenzAdatokReszveny() {
		return penzAdatokReszveny;
	}

	public void setPenzAdatokReszveny(Set<PenzugyiAdatok> penzAdatokReszveny) {
		this.penzAdatokReszveny = penzAdatokReszveny;
	}

	public Reszveny() {}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVallalat() {
		return vallalat;
	}

	public void setVallalat(String vallalat) {
		this.vallalat = vallalat;
	}

	public String getTicker() {
		return ticker;
	}

	public void setTicker(String ticker) {
		this.ticker = ticker;
	}

	public Date getDatum() {
		return datum;
	}

	public void setDatum(Date datum) {
		this.datum = datum;
	}

	public String getAgazat() {
		return agazat;
	}

	public void setAgazat(String agazat) {
		this.agazat = agazat;
	}

	public String getStrategia() {
		return strategia;
	}

	public void setStrategia(String strategia) {
		this.strategia = strategia;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public User getReszvenyUser() {
		return reszvenyUser;
	}

	public void setReszvenyUser(User reszvenyUser) {
		this.reszvenyUser = reszvenyUser;
	}

	public Set<Manageles> getManagelesReszveny() {
		return managelesReszveny;
	}

	public void setManagelesReszveny(Set<Manageles> managelesReszveny) {
		this.managelesReszveny = managelesReszveny;
	}

	public Set<VallalatPenzugyiElemzes> getPenzugyReszveny() {
		return penzugyReszveny;
	}

	public void setPenzugyReszveny(Set<VallalatPenzugyiElemzes> penzugyReszveny) {
		this.penzugyReszveny = penzugyReszveny;
	}
	
	
	
	
}
