package springsbinvesting.domain.celkituzes;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "akadaly")
public class Akadaly {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "title")
	private String title;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "CELKITUZES_ID")
	@JsonBackReference
	private Celkituzes akadalyCelkituzes;

	public Akadaly() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Celkituzes getAkadalyCelkituzes() {
		return akadalyCelkituzes;
	}

	public void setAkadalyCelkituzes(Celkituzes akadalyCelkituzes) {
		this.akadalyCelkituzes = akadalyCelkituzes;
	}

}
