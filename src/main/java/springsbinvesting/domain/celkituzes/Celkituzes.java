package springsbinvesting.domain.celkituzes;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import springsbinvesting.domain.Reszveny;
import springsbinvesting.domain.User;

@Entity
@Table(name = "celkituzes")
public class Celkituzes {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "megnevezes")
	private String megnevezes;
	
	@Column(name = "datum")
	private Date datum;
	
	@Column(name = "eleresi_szint")
	private String eleresiSzint;
	
	@Column(name = "elerhetoe")
	private String elerhetoE;
	
	@Column(name = "cel_oka")
	private String celOka;
	
	@Column(name = "cel_igeny")
	private String celIgeny;
	
	@Column(name = "elokeszulet_elso")
	private String elokeszuletElso;
	
	@Column(name = "elokeszulet_masodik")
	private String elokeszuletMasodik;
	
	@Column(name = "elokeszulet_harmadik")
	private String elokeszuletHarmadik;
	
	@Column(name = "elokeszulet_negyedik")
	private String elokeszuletNegyedik;
	
	@Column(name = "szukseges_osszeg")
	private double szuksegesOsszeg;
	
	@Column(name = "lepes1")
	private String lepes1;
	
	@Column(name = "lepes2")
	private String lepes2;
	
	@Column(name = "lepes3")
	private String lepes3;
	
	@Column(name = "lepes4")
	private String lepes4;
	
	@Column(name = "lepes5")
	private String lepes5;
	
	@Column(name = "lepes6")
	private String lepes6;
	
	@Column(name = "lepes7")
	private String lepes7;
	
	@Column(name = "lepes8")
	private String lepes8;
	
	@Column(name = "lepes9")
	private String lepes9;
	
	@Column(name = "lepes10")
	private String lepes10;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "USER_ID")
	@JsonBackReference
	private User celkituzesUser;
	
	@OneToMany(mappedBy = "akadalyCelkituzes", cascade = CascadeType.ALL)
	@Column(nullable = true)
	@JsonManagedReference
	private Set<Akadaly> akadalyok;
	
	@OneToMany(mappedBy = "feladatCelkituzes", cascade = CascadeType.ALL)
	@Column(nullable = true)
	@JsonManagedReference
	private Set<Feladat> feladatok;
	
	public Celkituzes() {}
	
	// Getters , Setters


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMegnevezes() {
		return megnevezes;
	}

	public void setMegnevezes(String megnevezes) {
		this.megnevezes = megnevezes;
	}

	public Date getDatum() {
		return datum;
	}

	public void setDatum(Date datum) {
		this.datum = datum;
	}

	public String getEleresiSzint() {
		return eleresiSzint;
	}

	public void setEleresiSzint(String eleresiSzint) {
		this.eleresiSzint = eleresiSzint;
	}

	public String getElerhetoE() {
		return elerhetoE;
	}

	public void setElerhetoE(String elerhetoE) {
		this.elerhetoE = elerhetoE;
	}

	public String getCelOka() {
		return celOka;
	}

	public void setCelOka(String celOka) {
		this.celOka = celOka;
	}

	public String getCelIgeny() {
		return celIgeny;
	}

	public void setCelIgeny(String celIgeny) {
		this.celIgeny = celIgeny;
	}

	public String getElokeszuletElso() {
		return elokeszuletElso;
	}

	public void setElokeszuletElso(String elokeszuletElso) {
		this.elokeszuletElso = elokeszuletElso;
	}

	public String getElokeszuletMasodik() {
		return elokeszuletMasodik;
	}

	public void setElokeszuletMasodik(String elokeszuletMasodik) {
		this.elokeszuletMasodik = elokeszuletMasodik;
	}

	public String getElokeszuletHarmadik() {
		return elokeszuletHarmadik;
	}

	public void setElokeszuletHarmadik(String elokeszuletHarmadik) {
		this.elokeszuletHarmadik = elokeszuletHarmadik;
	}

	public String getElokeszuletNegyedik() {
		return elokeszuletNegyedik;
	}

	public void setElokeszuletNegyedik(String elokeszuletNegyedik) {
		this.elokeszuletNegyedik = elokeszuletNegyedik;
	}

	public double getSzuksegesOsszeg() {
		return szuksegesOsszeg;
	}

	public void setSzuksegesOsszeg(double szuksegesOsszeg) {
		this.szuksegesOsszeg = szuksegesOsszeg;
	}

	public String getLepes1() {
		return lepes1;
	}

	public void setLepes1(String lepes1) {
		this.lepes1 = lepes1;
	}

	public String getLepes2() {
		return lepes2;
	}

	public void setLepes2(String lepes2) {
		this.lepes2 = lepes2;
	}

	public String getLepes3() {
		return lepes3;
	}

	public void setLepes3(String lepes3) {
		this.lepes3 = lepes3;
	}

	public String getLepes4() {
		return lepes4;
	}

	public void setLepes4(String lepes4) {
		this.lepes4 = lepes4;
	}

	public String getLepes5() {
		return lepes5;
	}

	public void setLepes5(String lepes5) {
		this.lepes5 = lepes5;
	}

	public String getLepes6() {
		return lepes6;
	}

	public void setLepes6(String lepes6) {
		this.lepes6 = lepes6;
	}

	public String getLepes7() {
		return lepes7;
	}

	public void setLepes7(String lepes7) {
		this.lepes7 = lepes7;
	}

	public String getLepes8() {
		return lepes8;
	}

	public void setLepes8(String lepes8) {
		this.lepes8 = lepes8;
	}

	public String getLepes9() {
		return lepes9;
	}

	public void setLepes9(String lepes9) {
		this.lepes9 = lepes9;
	}

	public String getLepes10() {
		return lepes10;
	}

	public void setLepes10(String lepes10) {
		this.lepes10 = lepes10;
	}

	public User getCelkituzesUser() {
		return celkituzesUser;
	}

	public void setCelkituzesUser(User celkituzesUser) {
		this.celkituzesUser = celkituzesUser;
	}

	public Set<Akadaly> getAkadalyok() {
		return akadalyok;
	}

	public void setAkadalyok(Set<Akadaly> akadalyok) {
		this.akadalyok = akadalyok;
	}

	public Set<Feladat> getFeladatok() {
		return feladatok;
	}

	public void setFeladatok(Set<Feladat> feladatok) {
		this.feladatok = feladatok;
	}
	
	
}
