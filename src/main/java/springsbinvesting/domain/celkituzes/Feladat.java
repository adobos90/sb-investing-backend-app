package springsbinvesting.domain.celkituzes;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "feladat")
public class Feladat {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "title")
	private String title;

	@Column(name = "besorolas")
	private String besorolas;

	@Column(name = "hatarido")
	private String hatarido;

	@Column(name = "color")
	private String color;

	@Column(name = "utemezett")
	private int utemezett;

	@Column(name = "feladat_start")
	private String start;

	@Column(name = "feladat_end")
	private String end;

	@Column(name = "feladat_type")
	private String type;

	@Column(name = "sorszam")
	private int sorszam;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "CELKITUZES_ID")
	@JsonBackReference
	private Celkituzes feladatCelkituzes;

	public Feladat() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBesorolas() {
		return besorolas;
	}

	public void setBesorolas(String besorolas) {
		this.besorolas = besorolas;
	}

	public String getHatarido() {
		return hatarido;
	}

	public void setHatarido(String hatarido) {
		this.hatarido = hatarido;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getSorszam() {
		return sorszam;
	}

	public void setSorszam(int sorszam) {
		this.sorszam = sorszam;
	}

	public Celkituzes getFeladatCelkituzes() {
		return feladatCelkituzes;
	}

	public void setFeladatCelkituzes(Celkituzes feladatCelkituzes) {
		this.feladatCelkituzes = feladatCelkituzes;
	}

	public int getUtemezett() {
		return utemezett;
	}

	public void setUtemezett(int utemezett) {
		this.utemezett = utemezett;
	}
	
	

}
