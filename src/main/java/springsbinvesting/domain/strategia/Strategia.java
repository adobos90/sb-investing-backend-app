package springsbinvesting.domain.strategia;

import com.fasterxml.jackson.annotation.JsonBackReference;
import springsbinvesting.domain.User;

import javax.persistence.*;

@Entity
@Table(name = "strategia")
public class Strategia {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "eredmeny")
    private int eredmeny;

    @Column(name = "datum")
    private String datum;

    @Column(name = "flag")
    private String flag;

    @Column(name = "teljesitmeny")
    private int teljesitmeny;

    @Column(name = "pe")
    private int pe;

    @Column(name = "pb")
    private int pb;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "USER_ID")
    @JsonBackReference
    private User strategiaUser;

    public Strategia() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getEredmeny() {
        return eredmeny;
    }

    public void setEredmeny(int eredmeny) {
        this.eredmeny = eredmeny;
    }

    public String getDatum() {
        return datum;
    }

    public void setDatum(String datum) {
        this.datum = datum;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public User getStrategiaUser() {
        return strategiaUser;
    }

    public void setStrategiaUser(User strategiaUser) {
        this.strategiaUser = strategiaUser;
    }

    public int getTeljesitmeny() {
        return teljesitmeny;
    }

    public void setTeljesitmeny(int teljesitmeny) {
        this.teljesitmeny = teljesitmeny;
    }

    public int getPe() {
        return pe;
    }

    public void setPe(int pe) {
        this.pe = pe;
    }

    public int getPb() {
        return pb;
    }

    public void setPb(int pb) {
        this.pb = pb;
    }
}
