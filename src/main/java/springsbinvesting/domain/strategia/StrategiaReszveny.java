package springsbinvesting.domain.strategia;


import com.fasterxml.jackson.annotation.JsonBackReference;
import springsbinvesting.domain.User;

import javax.persistence.*;

@Entity
@Table(name = "streszveny")
public class StrategiaReszveny {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "vallalat")
    private String vallalat;

    @Column(name = "ticker")
    private String ticker;

    @Column(name = "hozam")
    private int hozam;

    @Column(name = "reszveny")
    private int reszveny;

    @Column(name = "eps")
    private int eps;

    @Column(name = "agazat")
    private String agazat;

    @Column(name = "ipar")
    private String ipar;

    @Column(name = "kapitalizacio")
    private String kapitalizacio;

    @Column(name = "orszag")
    private String orszag;

    @Column(name = "ready")
    private int ready;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "USER_ID")
    @JsonBackReference
    private User stReszvenyUser;

    public StrategiaReszveny(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVallalat() {
        return vallalat;
    }

    public void setVallalat(String vallalat) {
        this.vallalat = vallalat;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public int getHozam() {
        return hozam;
    }

    public void setHozam(int hozam) {
        this.hozam = hozam;
    }

    public int getReszveny() {
        return reszveny;
    }

    public void setReszveny(int reszveny) {
        this.reszveny = reszveny;
    }

    public int getEps() {
        return eps;
    }

    public void setEps(int eps) {
        this.eps = eps;
    }

    public String getAgazat() {
        return agazat;
    }

    public void setAgazat(String agazat) {
        this.agazat = agazat;
    }

    public String getIpar() {
        return ipar;
    }

    public void setIpar(String ipar) {
        this.ipar = ipar;
    }

    public String getKapitalizacio() {
        return kapitalizacio;
    }

    public void setKapitalizacio(String kapitalizacio) {
        this.kapitalizacio = kapitalizacio;
    }

    public String getOrszag() {
        return orszag;
    }

    public void setOrszag(String orszag) {
        this.orszag = orszag;
    }

    public int getReady() {
        return ready;
    }

    public void setReady(int ready) {
        this.ready = ready;
    }

    public User getStReszvenyUser() {
        return stReszvenyUser;
    }

    public void setStReszvenyUser(User stReszvenyUser) {
        this.stReszvenyUser = stReszvenyUser;
    }
}
