package springsbinvesting.domain.reszvenyelemzes.manageles;

import javax.persistence.Column;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import springsbinvesting.domain.reszvenyelemzes.Manageles;
import springsbinvesting.domain.reszvenyelemzes.VallalatPenzugyiElemzes;

@Entity
@Table(name = "managelesbef")
public class ManagelesBefAdatok {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column
	private String szektor;

	@Column
	private String piaci_kapitalizacio;
	@Column
	private String menedzselesi_strat;
	@Column
	private int elemzesi_arfolyam;
	@Column
	private String elemzesi_penznem;
	@Column
	private int celar_arfolyam;
	@Column
	private String celar_penznem;
	@Column
	private int eves_megtakaritas;
	
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "MANAGELES_ID")
	@JsonBackReference
	private Manageles befAdatokmanageles;

	
	public ManagelesBefAdatok() {}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}

	public String getSzektor() {
		return szektor;
	}

	public void setSzektor(String szektor) {
		this.szektor = szektor;
	}

	public String getPiaci_kapitalizacio() {
		return piaci_kapitalizacio;
	}


	public void setPiaci_kapitalizacio(String piaci_kapitalizacio) {
		this.piaci_kapitalizacio = piaci_kapitalizacio;
	}


	public String getMenedzselesi_strat() {
		return menedzselesi_strat;
	}


	public void setMenedzselesi_strat(String menedzselesi_strat) {
		this.menedzselesi_strat = menedzselesi_strat;
	}

	public int getElemzesi_arfolyam() {
		return elemzesi_arfolyam;
	}


	public void setElemzesi_arfolyam(int elemzesi_arfolyam) {
		this.elemzesi_arfolyam = elemzesi_arfolyam;
	}

	public String getElemzesi_penznem() {
		return elemzesi_penznem;
	}


	public void setElemzesi_penznem(String elemzesi_penznem) {
		this.elemzesi_penznem = elemzesi_penznem;
	}


	public int getCelar_arfolyam() {
		return celar_arfolyam;
	}


	public void setCelar_arfolyam(int celar_arfolyam) {
		this.celar_arfolyam = celar_arfolyam;
	}


	public String getCelar_penznem() {
		return celar_penznem;
	}


	public void setCelar_penznem(String celar_penznem) {
		this.celar_penznem = celar_penznem;
	}


	public int getEves_megtakaritas() {
		return eves_megtakaritas;
	}


	public void setEves_megtakaritas(int eves_megtakaritas) {
		this.eves_megtakaritas = eves_megtakaritas;
	}


	public Manageles getBefAdatokmanageles() {
		return befAdatokmanageles;
	}


	public void setBefAdatokmanageles(Manageles befAdatokmanageles) {
		this.befAdatokmanageles = befAdatokmanageles;
	}
	
	// GETTERS +  SETTERS
	
	
}
