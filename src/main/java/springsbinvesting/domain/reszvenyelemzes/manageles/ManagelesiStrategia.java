package springsbinvesting.domain.reszvenyelemzes.manageles;

import javax.persistence.Column;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import springsbinvesting.domain.reszvenyelemzes.Manageles;

@Entity
@Table(name = "managelesstrat")
public class ManagelesiStrategia {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column
	private String arfolyam_eses;
	@Column
    private String arfolyam_emelkedes;
	@Column
    private String arfolyam_celar;
	@Column
    private String riziko_faktor_nott;
	@Column
    private String jov_romlott;
	@Column
    private String hitel_romlott;
	@Column
    private String gazd_romlott;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "MANAGELES_ID")
	@JsonBackReference
	private Manageles stratmanageles;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getArfolyam_eses() {
		return arfolyam_eses;
	}

	public void setArfolyam_eses(String arfolyam_eses) {
		this.arfolyam_eses = arfolyam_eses;
	}

	public String getArfolyam_emelkedes() {
		return arfolyam_emelkedes;
	}

	public void setArfolyam_emelkedes(String arfolyam_emelkedes) {
		this.arfolyam_emelkedes = arfolyam_emelkedes;
	}

	public String getArfolyam_celar() {
		return arfolyam_celar;
	}

	public void setArfolyam_celar(String arfolyam_celar) {
		this.arfolyam_celar = arfolyam_celar;
	}

	public String getRiziko_faktor_nott() {
		return riziko_faktor_nott;
	}

	public void setRiziko_faktor_nott(String riziko_faktor_nott) {
		this.riziko_faktor_nott = riziko_faktor_nott;
	}

	public String getJov_romlott() {
		return jov_romlott;
	}

	public void setJov_romlott(String jov_romlott) {
		this.jov_romlott = jov_romlott;
	}

	public String getHitel_romlott() {
		return hitel_romlott;
	}

	public void setHitel_romlott(String hitel_romlott) {
		this.hitel_romlott = hitel_romlott;
	}

	public String getGazd_romlott() {
		return gazd_romlott;
	}

	public void setGazd_romlott(String gazd_romlott) {
		this.gazd_romlott = gazd_romlott;
	}

	public Manageles getStratmanageles() {
		return stratmanageles;
	}

	public void setStratmanageles(Manageles stratmanageles) {
		this.stratmanageles = stratmanageles;
	}
		
}
