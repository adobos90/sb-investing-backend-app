package springsbinvesting.domain.reszvenyelemzes.manageles;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;

import springsbinvesting.domain.reszvenyelemzes.Manageles;

@Entity
@Table(name = "managelesreszveny")
public class ManagelesReszveny {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column
	private String vasarlas;
	@Column
	private String eszkoz;
	@Column
	private String stop_szint;
	@Column
	private String riziko_faktor;
	@Column
	private String bef_toke;
	@Column
	private String mikor_vasarolni;

	@Column
	private String gazdasagi_ingadozas_magyarazat;

	@Column
	private String valallati_riziko_magyarazat;

	@Column
	private String vallalati_rossz_penz_mutato_magy;

	@Column
	private String vallalat_rossz_hat_mut_magyarazat;

	@Column
	private String vallalat_hosszu_eladosodas_magyarazat;

	@Column
	private String vallalat_rovid_eladosodas_magyarazat;

	@Column
	private String vallalat_piaci_mut_magyarazat;

	@Column
	private String vallalat_magas_reszveny_magyarazat;

	@Column
	private String bef_netto_jelen_magyarazat;

	@Column
	private String egyeb_magyarazat;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "MANAGELES_ID")
	@JsonBackReference
	private Manageles reszvenymanageles;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVasarlas() {
		return vasarlas;
	}

	public void setVasarlas(String vasarlas) {
		this.vasarlas = vasarlas;
	}

	public String getEszkoz() {
		return eszkoz;
	}

	public void setEszkoz(String eszkoz) {
		this.eszkoz = eszkoz;
	}

	public String getStop_szint() {
		return stop_szint;
	}

	public void setStop_szint(String stop_szint) {
		this.stop_szint = stop_szint;
	}

	public String getRiziko_faktor() {
		return riziko_faktor;
	}

	public void setRiziko_faktor(String riziko_faktor) {
		this.riziko_faktor = riziko_faktor;
	}

	public String getBef_toke() {
		return bef_toke;
	}

	public void setBef_toke(String bef_toke) {
		this.bef_toke = bef_toke;
	}

	public String getMikor_vasarolni() {
		return mikor_vasarolni;
	}

	public void setMikor_vasarolni(String mikor_vasarolni) {
		this.mikor_vasarolni = mikor_vasarolni;
	}



	public String getGazdasagi_ingadozas_magyarazat() {
		return gazdasagi_ingadozas_magyarazat;
	}

	public void setGazdasagi_ingadozas_magyarazat(String gazdasagi_ingadozas_magyarazat) {
		this.gazdasagi_ingadozas_magyarazat = gazdasagi_ingadozas_magyarazat;
	}



	public String getValallati_riziko_magyarazat() {
		return valallati_riziko_magyarazat;
	}

	public void setValallati_riziko_magyarazat(String valallati_riziko_magyarazat) {
		this.valallati_riziko_magyarazat = valallati_riziko_magyarazat;
	}



	public String getVallalati_rossz_penz_mutato_magy() {
		return vallalati_rossz_penz_mutato_magy;
	}

	public void setVallalati_rossz_penz_mutato_magy(String vallalati_rossz_penz_mutato_magy) {
		this.vallalati_rossz_penz_mutato_magy = vallalati_rossz_penz_mutato_magy;
	}


	public String getVallalat_rossz_hat_mut_magyarazat() {
		return vallalat_rossz_hat_mut_magyarazat;
	}

	public void setVallalat_rossz_hat_mut_magyarazat(String vallalat_rossz_hat_hut_magyarazat) {
		this.vallalat_rossz_hat_mut_magyarazat = vallalat_rossz_hat_hut_magyarazat;
	}



	public String getVallalat_hosszu_eladosodas_magyarazat() {
		return vallalat_hosszu_eladosodas_magyarazat;
	}

	public void setVallalat_hosszu_eladosodas_magyarazat(String vallalat_hosszu_eladosodas_magyarazat) {
		this.vallalat_hosszu_eladosodas_magyarazat = vallalat_hosszu_eladosodas_magyarazat;
	}


	public String getVallalat_rovid_eladosodas_magyarazat() {
		return vallalat_rovid_eladosodas_magyarazat;
	}

	public void setVallalat_rovid_eladosodas_magyarazat(String vallalat_rovid_eladosodas_magyarazat) {
		this.vallalat_rovid_eladosodas_magyarazat = vallalat_rovid_eladosodas_magyarazat;
	}



	public String getVallalat_piaci_mut_magyarazat() {
		return vallalat_piaci_mut_magyarazat;
	}

	public void setVallalat_piaci_mut_magyarazat(String vallalat_piaci_mut_magyarazat) {
		this.vallalat_piaci_mut_magyarazat = vallalat_piaci_mut_magyarazat;
	}



	public String getVallalat_magas_reszveny_magyarazat() {
		return vallalat_magas_reszveny_magyarazat;
	}

	public void setVallalat_magas_reszveny_magyarazat(String vallalat_magas_reszveny_magyarazat) {
		this.vallalat_magas_reszveny_magyarazat = vallalat_magas_reszveny_magyarazat;
	}


	public String getBef_netto_jelen_magyarazat() {
		return bef_netto_jelen_magyarazat;
	}

	public void setBef_netto_jelen_magyarazat(String bef_netto_jelen_magyarazat) {
		this.bef_netto_jelen_magyarazat = bef_netto_jelen_magyarazat;
	}

	public String getEgyeb_magyarazat() {
		return egyeb_magyarazat;
	}

	public void setEgyeb_magyarazat(String egyeb_magyarazat) {
		this.egyeb_magyarazat = egyeb_magyarazat;
	}

	public Manageles getReszvenymanageles() {
		return reszvenymanageles;
	}

	public void setReszvenymanageles(Manageles reszvenymanageles) {
		this.reszvenymanageles = reszvenymanageles;
	}
	
	
}
