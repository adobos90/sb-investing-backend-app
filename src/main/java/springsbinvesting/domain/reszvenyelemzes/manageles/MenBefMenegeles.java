package springsbinvesting.domain.reszvenyelemzes.manageles;

import javax.persistence.Column;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import springsbinvesting.domain.reszvenyelemzes.Manageles;

@Entity
@Table(name = "managelesbefmen")
public class MenBefMenegeles {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column
	private String kov_datum;
	@Column
	private int arfolyam;
	@Column
	private String bek_valtozasok;
	@Column
	private String intezkedeseim;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "MANAGELES_ID")
	@JsonBackReference
	private Manageles befMenmanageles;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKov_datum() {
		return kov_datum;
	}

	public void setKov_datum(String kov_datum) {
		this.kov_datum = kov_datum;
	}

	public int getArfolyam() {
		return arfolyam;
	}

	public void setArfolyam(int arfolyam) {
		this.arfolyam = arfolyam;
	}

	public String getBek_valtozasok() {
		return bek_valtozasok;
	}

	public void setBek_valtozasok(String bek_valtozasok) {
		this.bek_valtozasok = bek_valtozasok;
	}

	public String getIntezkedeseim() {
		return intezkedeseim;
	}

	public void setIntezkedeseim(String intezkedeseim) {
		this.intezkedeseim = intezkedeseim;
	}

	public Manageles getBefMenmanageles() {
		return befMenmanageles;
	}

	public void setBefMenmanageles(Manageles befMenmanageles) {
		this.befMenmanageles = befMenmanageles;
	}
	
	
}
