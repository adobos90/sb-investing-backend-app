package springsbinvesting.domain.reszvenyelemzes;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import springsbinvesting.domain.Reszveny;
import springsbinvesting.domain.reszvenyelemzes.manageles.ManagelesBefAdatok;
import springsbinvesting.domain.reszvenyelemzes.manageles.ManagelesReszveny;
import springsbinvesting.domain.reszvenyelemzes.manageles.ManagelesiStrategia;
import springsbinvesting.domain.reszvenyelemzes.manageles.MenBefMenegeles;

@Entity
@Table(name = "manageles")
public class Manageles {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "RESZVENY_ID")
	@JsonBackReference
	private Reszveny managelesReszveny;
	
	// Manageles elemek
	// Bef Adatok
	@OneToMany(mappedBy = "befAdatokmanageles", cascade = CascadeType.ALL)
	@Column(nullable = true)
	@JsonManagedReference
	private Set<ManagelesBefAdatok> befAdatokmanageles;
	
	// Strategia
	@OneToMany(mappedBy = "stratmanageles", cascade = CascadeType.ALL)
	@Column(nullable = true)
	@JsonManagedReference
	private Set<ManagelesiStrategia> stratmanageles;
	
	// Reszveny
	@OneToMany(mappedBy = "reszvenymanageles", cascade = CascadeType.ALL)
	@Column(nullable = true)
	@JsonManagedReference
	private Set<ManagelesReszveny> reszvenymanageles;
	
	// MenBef
	@OneToMany(mappedBy = "befMenmanageles", cascade = CascadeType.ALL)
	@Column(nullable = true)
	@JsonManagedReference
	private Set<MenBefMenegeles> befMenmanageles;
	
	public Manageles() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Reszveny getManagelesReszveny() {
		return managelesReszveny;
	}

	public void setManagelesReszveny(Reszveny managelesReszveny) {
		this.managelesReszveny = managelesReszveny;
	}

	public Set<ManagelesBefAdatok> getBefAdatokmanageles() {
		return befAdatokmanageles;
	}

	public void setBefAdatokmanageles(Set<ManagelesBefAdatok> befAdatokmanageles) {
		this.befAdatokmanageles = befAdatokmanageles;
	}

	public Set<ManagelesiStrategia> getStratmanageles() {
		return stratmanageles;
	}

	public void setStratmanageles(Set<ManagelesiStrategia> stratmanageles) {
		this.stratmanageles = stratmanageles;
	}

	public Set<ManagelesReszveny> getReszvenymanageles() {
		return reszvenymanageles;
	}

	public void setReszvenymanageles(Set<ManagelesReszveny> reszvenymanageles) {
		this.reszvenymanageles = reszvenymanageles;
	}

	public Set<MenBefMenegeles> getBefMenmanageles() {
		return befMenmanageles;
	}

	public void setBefMenmanageles(Set<MenBefMenegeles> befMenmanageles) {
		this.befMenmanageles = befMenmanageles;
	}

	
	
}
