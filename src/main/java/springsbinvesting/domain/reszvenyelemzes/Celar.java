package springsbinvesting.domain.reszvenyelemzes;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import springsbinvesting.domain.Reszveny;

@Entity
@Table(name = "celar")
public class Celar {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column
	private Integer konkurencia_mutato;
	
	@Column
	private Integer konyv_szerinti_ertek;
	
	@Column
	private Integer elvart_hozam;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "RESZVENY_ID")
	@JsonBackReference
	private Reszveny celarReszveny;
	
	public Celar() {}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getKonkurencia_mutato() {
		return konkurencia_mutato;
	}

	public void setKonkurencia_mutato(Integer konkurencia_mutato) {
		this.konkurencia_mutato = konkurencia_mutato;
	}

	public Integer getKonyv_szerinti_ertek() {
		return konyv_szerinti_ertek;
	}

	public void setKonyv_szerinti_ertek(Integer konyv_szerIntegeri_ertek) {
		this.konyv_szerinti_ertek = konyv_szerIntegeri_ertek;
	}

	public Integer getElvart_hozam() {
		return elvart_hozam;
	}

	public void setElvart_hozam(Integer elvart_hozam) {
		this.elvart_hozam = elvart_hozam;
	}

	public Reszveny getCelarReszveny() {
		return celarReszveny;
	}

	public void setCelarReszveny(Reszveny celarReszveny) {
		this.celarReszveny = celarReszveny;
	}
	
	
   
}
