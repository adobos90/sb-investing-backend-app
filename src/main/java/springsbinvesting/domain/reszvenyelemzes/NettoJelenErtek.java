package springsbinvesting.domain.reszvenyelemzes;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import springsbinvesting.domain.Reszveny;

@Entity
@Table(name = "netto_jelenertek")
public class NettoJelenErtek {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	private Integer bef_osszeg;
	private String penznem;
	private Integer arfolyam;
	private Integer csucs_kimenet;
	private Integer csucs_val;
	private Integer celar_kimenet;
	private Integer celar_val;
	private Integer celar_fel_kimenet;
	private Integer celar_fel_val ;
	private Integer alj_kimenet;
	private Integer alj_val;
	private Integer mely_kimenet;
	private Integer mely_val;
	private Integer csod_kimenet;
	private Integer csod_val;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "RESZVENY_ID")
	@JsonBackReference
	private Reszveny nettoReszveny;
	
	public NettoJelenErtek() {}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getBef_osszeg() {
		return bef_osszeg;
	}

	public void setBef_osszeg(Integer bef_osszeg) {
		this.bef_osszeg = bef_osszeg;
	}

	public String getPenznem() {
		return penznem;
	}

	public void setPenznem(String penznem) {
		this.penznem = penznem;
	}

	public Integer getArfolyam() {
		return arfolyam;
	}

	public void setArfolyam(Integer arfolyam) {
		this.arfolyam = arfolyam;
	}

	public Integer getCsucs_kimenet() {
		return csucs_kimenet;
	}

	public void setCsucs_kimenet(Integer csucs_kimenet) {
		this.csucs_kimenet = csucs_kimenet;
	}

	public Integer getCsucs_val() {
		return csucs_val;
	}

	public void setCsucs_val(Integer csucs_val) {
		this.csucs_val = csucs_val;
	}

	public Integer getCelar_kimenet() {
		return celar_kimenet;
	}

	public void setCelar_kimenet(Integer celar_kimenet) {
		this.celar_kimenet = celar_kimenet;
	}

	public Integer getCelar_val() {
		return celar_val;
	}

	public void setCelar_val(Integer celar_val) {
		this.celar_val = celar_val;
	}

	public Integer getCelar_fel_kimenet() {
		return celar_fel_kimenet;
	}

	public void setCelar_fel_kimenet(Integer celar_fel_kimenet) {
		this.celar_fel_kimenet = celar_fel_kimenet;
	}

	public Integer getCelar_fel_val() {
		return celar_fel_val;
	}

	public void setCelar_fel_val(Integer celar_fel_val) {
		this.celar_fel_val = celar_fel_val;
	}

	public Integer getAlj_kimenet() {
		return alj_kimenet;
	}

	public void setAlj_kimenet(Integer alj_kimenet) {
		this.alj_kimenet = alj_kimenet;
	}

	public Integer getAlj_val() {
		return alj_val;
	}

	public void setAlj_val(Integer alj_val) {
		this.alj_val = alj_val;
	}

	public Integer getMely_kimenet() {
		return mely_kimenet;
	}

	public void setMely_kimenet(Integer mely_kimenet) {
		this.mely_kimenet = mely_kimenet;
	}

	public Integer getMely_val() {
		return mely_val;
	}

	public void setMely_val(Integer mely_val) {
		this.mely_val = mely_val;
	}

	public Integer getCsod_kimenet() {
		return csod_kimenet;
	}

	public void setCsod_kimenet(Integer csod_kimenet) {
		this.csod_kimenet = csod_kimenet;
	}

	public Reszveny getNettoReszveny() {
		return nettoReszveny;
	}

	public void setNettoReszveny(Reszveny nettoReszveny) {
		this.nettoReszveny = nettoReszveny;
	}

	public Integer getCsod_val() {
		return csod_val;
	}

	public void setCsod_val(Integer csod_val) {
		this.csod_val = csod_val;
	}
	
	
	
	
}
