package springsbinvesting.domain.reszvenyelemzes;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import springsbinvesting.domain.Reszveny;

@Entity
@Table(name = "vallalatkockazatelemzes")
public class VallalatKockElemzes {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String reszveny_eredmeny;
	private Integer reszveny_riziko;
	private String konyv_eredmeny;
	private Integer konyv_riziko;
	private String eps_eredmeny;
	private Integer eps_riziko;
	private String piaci_kap_eredmeny;
	private Integer piaci_kap_riziko;
    private String hitel_besorolas_eredmeny;
    private Integer hitel_besorolas_riziko;
    private String tobbsegi_tulaj_eredmeny;
    private Integer tobbsegi_tulaj_riziko;
    private String ceg_info_eredmeny;
    private Integer ceg_info_riziko;
    private String termek_info_eredmeny;
    private Integer termek_info_riziko;
    private String kutatas_fejlesztes_eredmeny;
    private Integer kutatas_fejlesztes_riziko;
    private String immaterialis_eredmeny;
    private Integer immaterialis_riziko;
    private String egyeb_eredmeny;
    private Integer egyeb_riziko;
    private String szukseges_allomany_eredmeny;
    private Integer szukseges_allomany_riziko;
    private String elerheto_info_eredmeny;
    private Integer elerheto_info_riziko;
    private String osztalek_eredmeny;
    private Integer osztalek_riziko;
    private String vallalat_tozsde_eredmeny;
    private Integer vallalat_tozsde_riziko;
    private String fontos_tech_eredmeny;
    private Integer fontos_tech_riziko;
    private String volt_csucson_eredmeny;
    private Integer volt_csucson_riziko;
    private String fuzio_eredmeny;
    private Integer fuzio_riziko;
    private String beta_eredmeny;
    private Integer beta_riziko;
    private String vez_me_eredmeny;
    private Integer vez_me_riziko;
    private String uj_reszveny_ki_eredmeny;
    private Integer uj_reszveny_ki_riziko;
    
    @ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "RESZVENY_ID")
	@JsonBackReference
	private Reszveny vallalatKockElemzesReszveny;
    
    public VallalatKockElemzes() {}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getReszveny_eredmeny() {
		return reszveny_eredmeny;
	}

	public void setReszveny_eredmeny(String reszveny_eredmeny) {
		this.reszveny_eredmeny = reszveny_eredmeny;
	}

	public Integer getReszveny_riziko() {
		return reszveny_riziko;
	}

	public void setReszveny_riziko(Integer reszveny_riziko) {
		this.reszveny_riziko = reszveny_riziko;
	}

	public String getKonyv_eredmeny() {
		return konyv_eredmeny;
	}

	public void setKonyv_eredmeny(String konyv_eredmeny) {
		this.konyv_eredmeny = konyv_eredmeny;
	}

	public Integer getKonyv_riziko() {
		return konyv_riziko;
	}

	public void setKonyv_riziko(Integer konyv_riziko) {
		this.konyv_riziko = konyv_riziko;
	}

	public String getEps_eredmeny() {
		return eps_eredmeny;
	}

	public void setEps_eredmeny(String eps_eredmeny) {
		this.eps_eredmeny = eps_eredmeny;
	}

	public Integer getEps_riziko() {
		return eps_riziko;
	}

	public void setEps_riziko(Integer eps_riziko) {
		this.eps_riziko = eps_riziko;
	}

	public String getPiaci_kap_eredmeny() {
		return piaci_kap_eredmeny;
	}

	public void setPiaci_kap_eredmeny(String piaci_kap_eredmeny) {
		this.piaci_kap_eredmeny = piaci_kap_eredmeny;
	}

	public Integer getPiaci_kap_riziko() {
		return piaci_kap_riziko;
	}

	public void setPiaci_kap_riziko(Integer piaci_kap_riziko) {
		this.piaci_kap_riziko = piaci_kap_riziko;
	}

	public String getHitel_besorolas_eredmeny() {
		return hitel_besorolas_eredmeny;
	}

	public void setHitel_besorolas_eredmeny(String hitel_besorolas_eredmeny) {
		this.hitel_besorolas_eredmeny = hitel_besorolas_eredmeny;
	}

	public Integer getHitel_besorolas_riziko() {
		return hitel_besorolas_riziko;
	}

	public void setHitel_besorolas_riziko(Integer hitel_besorolas_riziko) {
		this.hitel_besorolas_riziko = hitel_besorolas_riziko;
	}

	public String getTobbsegi_tulaj_eredmeny() {
		return tobbsegi_tulaj_eredmeny;
	}

	public void setTobbsegi_tulaj_eredmeny(String tobbsegi_tulaj_eredmeny) {
		this.tobbsegi_tulaj_eredmeny = tobbsegi_tulaj_eredmeny;
	}

	public Integer getTobbsegi_tulaj_riziko() {
		return tobbsegi_tulaj_riziko;
	}

	public void setTobbsegi_tulaj_riziko(Integer tobbsegi_tulaj_riziko) {
		this.tobbsegi_tulaj_riziko = tobbsegi_tulaj_riziko;
	}

	public String getCeg_info_eredmeny() {
		return ceg_info_eredmeny;
	}

	public void setCeg_info_eredmeny(String ceg_info_eredmeny) {
		this.ceg_info_eredmeny = ceg_info_eredmeny;
	}

	public Integer getCeg_info_riziko() {
		return ceg_info_riziko;
	}

	public void setCeg_info_riziko(Integer ceg_info_riziko) {
		this.ceg_info_riziko = ceg_info_riziko;
	}

	public String getTermek_info_eredmeny() {
		return termek_info_eredmeny;
	}

	public void setTermek_info_eredmeny(String termek_info_eredmeny) {
		this.termek_info_eredmeny = termek_info_eredmeny;
	}

	public Integer getTermek_info_riziko() {
		return termek_info_riziko;
	}

	public void setTermek_info_riziko(Integer termek_info_riziko) {
		this.termek_info_riziko = termek_info_riziko;
	}

	public String getKutatas_fejlesztes_eredmeny() {
		return kutatas_fejlesztes_eredmeny;
	}

	public void setKutatas_fejlesztes_eredmeny(String kutatas_fejlesztes_eredmeny) {
		this.kutatas_fejlesztes_eredmeny = kutatas_fejlesztes_eredmeny;
	}

	public Integer getKutatas_fejlesztes_riziko() {
		return kutatas_fejlesztes_riziko;
	}

	public void setKutatas_fejlesztes_riziko(Integer kutatas_fejlesztes_riziko) {
		this.kutatas_fejlesztes_riziko = kutatas_fejlesztes_riziko;
	}

	public String getImmaterialis_eredmeny() {
		return immaterialis_eredmeny;
	}

	public void setImmaterialis_eredmeny(String immaterialis_eredmeny) {
		this.immaterialis_eredmeny = immaterialis_eredmeny;
	}

	public Integer getImmaterialis_riziko() {
		return immaterialis_riziko;
	}

	public void setImmaterialis_riziko(Integer immaterialis_riziko) {
		this.immaterialis_riziko = immaterialis_riziko;
	}

	public String getEgyeb_eredmeny() {
		return egyeb_eredmeny;
	}

	public void setEgyeb_eredmeny(String egyeb_eredmeny) {
		this.egyeb_eredmeny = egyeb_eredmeny;
	}

	public Integer getEgyeb_riziko() {
		return egyeb_riziko;
	}

	public void setEgyeb_riziko(Integer egyeb_riziko) {
		this.egyeb_riziko = egyeb_riziko;
	}

	public String getSzukseges_allomany_eredmeny() {
		return szukseges_allomany_eredmeny;
	}

	public void setSzukseges_allomany_eredmeny(String szukseges_allomany_eredmeny) {
		this.szukseges_allomany_eredmeny = szukseges_allomany_eredmeny;
	}

	public Integer getSzukseges_allomany_riziko() {
		return szukseges_allomany_riziko;
	}

	public void setSzukseges_allomany_riziko(Integer szukseges_allomany_riziko) {
		this.szukseges_allomany_riziko = szukseges_allomany_riziko;
	}

	public String getElerheto_info_eredmeny() {
		return elerheto_info_eredmeny;
	}

	public void setElerheto_info_eredmeny(String elerheto_info_eredmeny) {
		this.elerheto_info_eredmeny = elerheto_info_eredmeny;
	}

	public Integer getElerheto_info_riziko() {
		return elerheto_info_riziko;
	}

	public void setElerheto_info_riziko(Integer elerheto_info_riziko) {
		this.elerheto_info_riziko = elerheto_info_riziko;
	}

	public String getOsztalek_eredmeny() {
		return osztalek_eredmeny;
	}

	public void setOsztalek_eredmeny(String osztalek_eredmeny) {
		this.osztalek_eredmeny = osztalek_eredmeny;
	}

	public Integer getOsztalek_riziko() {
		return osztalek_riziko;
	}

	public void setOsztalek_riziko(Integer osztalek_riziko) {
		this.osztalek_riziko = osztalek_riziko;
	}

	public String getVallalat_tozsde_eredmeny() {
		return vallalat_tozsde_eredmeny;
	}

	public void setVallalat_tozsde_eredmeny(String vallalat_tozsde_eredmeny) {
		this.vallalat_tozsde_eredmeny = vallalat_tozsde_eredmeny;
	}

	public Integer getVallalat_tozsde_riziko() {
		return vallalat_tozsde_riziko;
	}

	public void setVallalat_tozsde_riziko(Integer vallalat_tozsde_riziko) {
		this.vallalat_tozsde_riziko = vallalat_tozsde_riziko;
	}

	public String getFontos_tech_eredmeny() {
		return fontos_tech_eredmeny;
	}

	public void setFontos_tech_eredmeny(String fontos_tech_eredmeny) {
		this.fontos_tech_eredmeny = fontos_tech_eredmeny;
	}

	public Integer getFontos_tech_riziko() {
		return fontos_tech_riziko;
	}

	public void setFontos_tech_riziko(Integer fontos_tech_riziko) {
		this.fontos_tech_riziko = fontos_tech_riziko;
	}

	public String getVolt_csucson_eredmeny() {
		return volt_csucson_eredmeny;
	}

	public void setVolt_csucson_eredmeny(String volt_csucson_eredmeny) {
		this.volt_csucson_eredmeny = volt_csucson_eredmeny;
	}

	public Integer getVolt_csucson_riziko() {
		return volt_csucson_riziko;
	}

	public void setVolt_csucson_riziko(Integer volt_csucson_riziko) {
		this.volt_csucson_riziko = volt_csucson_riziko;
	}

	public String getFuzio_eredmeny() {
		return fuzio_eredmeny;
	}

	public void setFuzio_eredmeny(String fuzio_eredmeny) {
		this.fuzio_eredmeny = fuzio_eredmeny;
	}

	public Integer getFuzio_riziko() {
		return fuzio_riziko;
	}

	public void setFuzio_riziko(Integer fuzio_riziko) {
		this.fuzio_riziko = fuzio_riziko;
	}

	public String getBeta_eredmeny() {
		return beta_eredmeny;
	}

	public void setBeta_eredmeny(String beta_eredmeny) {
		this.beta_eredmeny = beta_eredmeny;
	}

	public Integer getBeta_riziko() {
		return beta_riziko;
	}

	public void setBeta_riziko(Integer beta_riziko) {
		this.beta_riziko = beta_riziko;
	}

	public String getVez_me_eredmeny() {
		return vez_me_eredmeny;
	}

	public void setVez_me_eredmeny(String vez_me_eredmeny) {
		this.vez_me_eredmeny = vez_me_eredmeny;
	}

	public Integer getVez_me_riziko() {
		return vez_me_riziko;
	}

	public void setVez_me_riziko(Integer vez_me_riziko) {
		this.vez_me_riziko = vez_me_riziko;
	}

	public String getUj_reszveny_ki_eredmeny() {
		return uj_reszveny_ki_eredmeny;
	}

	public void setUj_reszveny_ki_eredmeny(String uj_reszveny_ki_eredmeny) {
		this.uj_reszveny_ki_eredmeny = uj_reszveny_ki_eredmeny;
	}

	public Integer getUj_reszveny_ki_riziko() {
		return uj_reszveny_ki_riziko;
	}

	public void setUj_reszveny_ki_riziko(Integer uj_reszveny_ki_riziko) {
		this.uj_reszveny_ki_riziko = uj_reszveny_ki_riziko;
	}

	public Reszveny getVallalatKockElemzesReszveny() {
		return vallalatKockElemzesReszveny;
	}

	public void setVallalatKockElemzesReszveny(Reszveny vallalatKockElemzesReszveny) {
		this.vallalatKockElemzesReszveny = vallalatKockElemzesReszveny;
	}
    
    
}
