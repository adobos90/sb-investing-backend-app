package springsbinvesting.domain.reszvenyelemzes;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import springsbinvesting.domain.Reszveny;
import springsbinvesting.domain.reszvenyelemzes.manageles.ManagelesBefAdatok;
import springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.PenzugyiElemzesHatekonysag;
import springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.PenzugyiElemzesHitel;
import springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.PenzugyiElemzesJovedelem;
import springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.PenzugyiElemzesPiaci;

@Entity
@Table(name = "vallalatpenzugyielemzes")
public class VallalatPenzugyiElemzes {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "RESZVENY_ID")
	@JsonBackReference
	private Reszveny penzugyReszveny;
	
	// Penz elemzes elemek
	@OneToMany(mappedBy = "jovPenzElemzes", cascade = CascadeType.ALL)
	@Column(nullable = true)
	@JsonManagedReference
	private Set<PenzugyiElemzesJovedelem> jovPenzElemzes;
	
	@OneToMany(mappedBy = "hatPenzElemzes", cascade = CascadeType.ALL)
	@Column(nullable = true)
	@JsonManagedReference
	private Set<PenzugyiElemzesHatekonysag> hatPenzElemzes;
	
	@OneToMany(mappedBy = "hitelPenzElemzes", cascade = CascadeType.ALL)
	@Column(nullable = true)
	@JsonManagedReference
	private Set<PenzugyiElemzesHitel> hitelPenzElemzes;
	
	@OneToMany(mappedBy = "piaciPenzElemzes", cascade = CascadeType.ALL)
	@Column(nullable = true)
	@JsonManagedReference
	private Set<PenzugyiElemzesPiaci> piaciPenzElemzes;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Reszveny getPenzugyReszveny() {
		return penzugyReszveny;
	}

	public void setPenzugyReszveny(Reszveny penzugyReszveny) {
		this.penzugyReszveny = penzugyReszveny;
	}

	public Set<PenzugyiElemzesJovedelem> getJovPenzElemzes() {
		return jovPenzElemzes;
	}

	public void setJovPenzElemzes(Set<PenzugyiElemzesJovedelem> jovPenzElemzes) {
		this.jovPenzElemzes = jovPenzElemzes;
	}

	public Set<PenzugyiElemzesHatekonysag> getHatPenzElemzes() {
		return hatPenzElemzes;
	}

	public void setHatPenzElemzes(Set<PenzugyiElemzesHatekonysag> hatPenzElemzes) {
		this.hatPenzElemzes = hatPenzElemzes;
	}

	public Set<PenzugyiElemzesHitel> getHitelPenzElemzes() {
		return hitelPenzElemzes;
	}

	public void setHitelPenzElemzes(Set<PenzugyiElemzesHitel> hitelPenzElemzes) {
		this.hitelPenzElemzes = hitelPenzElemzes;
	}

	public Set<PenzugyiElemzesPiaci> getPiaciPenzElemzes() {
		return piaciPenzElemzes;
	}

	public void setPiaciPenzElemzes(Set<PenzugyiElemzesPiaci> piaciPenzElemzes) {
		this.piaciPenzElemzes = piaciPenzElemzes;
	}
	
	
	
}
