package springsbinvesting.domain.reszvenyelemzes.penzugyielemzes;

import javax.persistence.Column;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import springsbinvesting.domain.reszvenyelemzes.VallalatPenzugyiElemzes;

@Entity
@Table(name = "vallalatpenzugyhat")
public class PenzugyiElemzesHatekonysag {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column
	private String hat_megjegyzes;
	@Column
	private Integer netto_hat;
	@Column
	private Integer keszlet_hat;
	@Column
	private Integer targyev_keszlet_hat;
	@Column
	private Integer elozoev_keszlet_hat;
	@Column
	private Integer konkurencia_keszlet_hat;
	@Column
	private Integer ert_hat;
	@Column
	private Integer hat_egyeb_mukodesi;
	@Column
	private Integer hat_netto_arbevetel;
	@Column
	private Integer targyev_koltseg_hat;
	@Column
	private Integer elozoev_koltseg_hat;
	@Column
	private Integer konkurencia_koltseg_hat;
	@Column
	private Integer naptar_hat;
	@Column
	private Integer hat_atlagos_netto_arbevetel;
	@Column
	private Integer hat_atlagos_koveteles;
	@Column
	private Integer targyev_atlagos_hat;
	@Column
	private Integer elozoev_atlagos_hat;
	@Column
	private Integer konkurencia_atlagos_hat;
	@Column
	private Integer naptar_hat_szallito;
	@Column
	private Integer hat_szallito_netto_arbevetel;
	@Column
	private Integer hat_szallito_kotelezettseg;
	@Column
	private Integer targyev_szallito_hat;
	@Column
	private Integer elozoev_szallito_hat;
	@Column
	private Integer konkurencia_szallito_hat;
	@Column
	private Integer fore_juto_netto_hat;
	@Column
	private Integer fore_juto_letszam_hat;
	@Column
	private Integer targyev_fore_juto_hat;
	@Column
	private Integer elozoev_fore_juto_hat;
	@Column
	private Integer konkurencia_fore_juto_hat;
	@Column
	private Integer forgo_toke_hat_netto_arbevetel;
	@Column
	private Integer forgo_toke_hat_forgo_eszkoz;
	@Column
	private Integer forgo_toke_hat_rovid_lej_kot;
	@Column
	private Integer targyev_forgo_toke_hat;
	@Column
	private Integer elozoev_forgo_toke_hat;

	@Column
	private Integer konkurencia_forgo_toke_hat;

	public String getHat_megjegyzes() {
		return hat_megjegyzes;
	}

	public void setHat_megjegyzes(String hat_megjegyzes) {
		this.hat_megjegyzes = hat_megjegyzes;
	}

	public Integer getNetto_hat() {
		return netto_hat;
	}

	public void setNetto_hat(Integer netto_hat) {
		this.netto_hat = netto_hat;
	}

	public Integer getKeszlet_hat() {
		return keszlet_hat;
	}

	public void setKeszlet_hat(Integer keszlet_hat) {
		this.keszlet_hat = keszlet_hat;
	}

	public Integer getTargyev_keszlet_hat() {
		return targyev_keszlet_hat;
	}

	public void setTargyev_keszlet_hat(Integer targyev_keszlet_hat) {
		this.targyev_keszlet_hat = targyev_keszlet_hat;
	}

	public Integer getElozoev_keszlet_hat() {
		return elozoev_keszlet_hat;
	}

	public void setElozoev_keszlet_hat(Integer elozoev_keszlet_hat) {
		this.elozoev_keszlet_hat = elozoev_keszlet_hat;
	}

	public Integer getKonkurencia_keszlet_hat() {
		return konkurencia_keszlet_hat;
	}

	public void setKonkurencia_keszlet_hat(Integer konkurencia_keszlet_hat) {
		this.konkurencia_keszlet_hat = konkurencia_keszlet_hat;
	}

	public Integer getErt_hat() {
		return ert_hat;
	}

	public void setErt_hat(Integer ert_hat) {
		this.ert_hat = ert_hat;
	}

	public Integer getHat_egyeb_mukodesi() {
		return hat_egyeb_mukodesi;
	}

	public void setHat_egyeb_mukodesi(Integer hat_egyeb_mukodesi) {
		this.hat_egyeb_mukodesi = hat_egyeb_mukodesi;
	}

	public Integer getHat_netto_arbevetel() {
		return hat_netto_arbevetel;
	}

	public void setHat_netto_arbevetel(Integer hat_netto_arbevetel) {
		this.hat_netto_arbevetel = hat_netto_arbevetel;
	}

	public Integer getTargyev_koltseg_hat() {
		return targyev_koltseg_hat;
	}

	public void setTargyev_koltseg_hat(Integer targyev_koltseg_hat) {
		this.targyev_koltseg_hat = targyev_koltseg_hat;
	}

	public Integer getElozoev_koltseg_hat() {
		return elozoev_koltseg_hat;
	}

	public void setElozoev_koltseg_hat(Integer elozoev_koltseg_hat) {
		this.elozoev_koltseg_hat = elozoev_koltseg_hat;
	}

	public Integer getKonkurencia_koltseg_hat() {
		return konkurencia_koltseg_hat;
	}

	public void setKonkurencia_koltseg_hat(Integer konkurencia_koltseg_hat) {
		this.konkurencia_koltseg_hat = konkurencia_koltseg_hat;
	}

	public Integer getNaptar_hat() {
		return naptar_hat;
	}

	public void setNaptar_hat(Integer naptar_hat) {
		this.naptar_hat = naptar_hat;
	}

	public Integer getHat_atlagos_netto_arbevetel() {
		return hat_atlagos_netto_arbevetel;
	}

	public void setHat_atlagos_netto_arbevetel(Integer hat_atlagos_netto_arbevetel) {
		this.hat_atlagos_netto_arbevetel = hat_atlagos_netto_arbevetel;
	}

	public Integer getHat_atlagos_koveteles() {
		return hat_atlagos_koveteles;
	}

	public void setHat_atlagos_koveteles(Integer hat_atlagos_koveteles) {
		this.hat_atlagos_koveteles = hat_atlagos_koveteles;
	}

	public Integer getTargyev_atlagos_hat() {
		return targyev_atlagos_hat;
	}

	public void setTargyev_atlagos_hat(Integer targyev_atlagos_hat) {
		this.targyev_atlagos_hat = targyev_atlagos_hat;
	}

	public Integer getElozoev_atlagos_hat() {
		return elozoev_atlagos_hat;
	}

	public void setElozoev_atlagos_hat(Integer elozoev_atlagos_hat) {
		this.elozoev_atlagos_hat = elozoev_atlagos_hat;
	}

	public Integer getKonkurencia_atlagos_hat() {
		return konkurencia_atlagos_hat;
	}

	public void setKonkurencia_atlagos_hat(Integer konkurencia_atlagos_hat) {
		this.konkurencia_atlagos_hat = konkurencia_atlagos_hat;
	}

	public Integer getNaptar_hat_szallito() {
		return naptar_hat_szallito;
	}

	public void setNaptar_hat_szallito(Integer naptar_hat_szallito) {
		this.naptar_hat_szallito = naptar_hat_szallito;
	}

	public Integer getHat_szallito_netto_arbevetel() {
		return hat_szallito_netto_arbevetel;
	}

	public void setHat_szallito_netto_arbevetel(Integer hat_szallito_netto_arbevetel) {
		this.hat_szallito_netto_arbevetel = hat_szallito_netto_arbevetel;
	}

	public Integer getHat_szallito_kotelezettseg() {
		return hat_szallito_kotelezettseg;
	}

	public void setHat_szallito_kotelezettseg(Integer hat_szallito_kotelezettseg) {
		this.hat_szallito_kotelezettseg = hat_szallito_kotelezettseg;
	}

	public Integer getTargyev_szallito_hat() {
		return targyev_szallito_hat;
	}

	public void setTargyev_szallito_hat(Integer targyev_szallito_hat) {
		this.targyev_szallito_hat = targyev_szallito_hat;
	}

	public Integer getElozoev_szallito_hat() {
		return elozoev_szallito_hat;
	}

	public void setElozoev_szallito_hat(Integer elozoev_szallito_hat) {
		this.elozoev_szallito_hat = elozoev_szallito_hat;
	}

	public Integer getKonkurencia_szallito_hat() {
		return konkurencia_szallito_hat;
	}

	public void setKonkurencia_szallito_hat(Integer konkurencia_szallito_hat) {
		this.konkurencia_szallito_hat = konkurencia_szallito_hat;
	}

	public Integer getFore_juto_netto_hat() {
		return fore_juto_netto_hat;
	}

	public void setFore_juto_netto_hat(Integer fore_juto_netto_hat) {
		this.fore_juto_netto_hat = fore_juto_netto_hat;
	}

	public Integer getFore_juto_letszam_hat() {
		return fore_juto_letszam_hat;
	}

	public void setFore_jutoLetszam_hat(Integer fore_juto_letszam_hat) {
		this.fore_juto_letszam_hat = fore_juto_letszam_hat;
	}

	public Integer getTargyev_fore_juto_hat() {
		return targyev_fore_juto_hat;
	}

	public void setTargyev_fore_juto_hat(Integer targyev_fore_juto_hat) {
		this.targyev_fore_juto_hat = targyev_fore_juto_hat;
	}

	public Integer getElozoev_fore_juto_hat() {
		return elozoev_fore_juto_hat;
	}

	public void setElozoev_fore_juto_hat(Integer elozoev_fore_juto_hat) {
		this.elozoev_fore_juto_hat = elozoev_fore_juto_hat;
	}

	public Integer getKonkurencia_fore_juto_hat() {
		return konkurencia_fore_juto_hat;
	}

	public void setKonkurencia_fore_juto_hat(Integer konkurencia_fore_juto_hat) {
		this.konkurencia_fore_juto_hat = konkurencia_fore_juto_hat;
	}

	public Integer getForgo_toke_hat_netto_arbevetel() {
		return forgo_toke_hat_netto_arbevetel;
	}

	public void setForgo_toke_hat_netto_arbevetel(Integer forgo_toke_hat_netto_arbevetel) {
		this.forgo_toke_hat_netto_arbevetel = forgo_toke_hat_netto_arbevetel;
	}

	public Integer getForgo_toke_hat_forgo_eszkoz() {
		return forgo_toke_hat_forgo_eszkoz;
	}

	public void setForgo_toke_hat_forgo_eszkoz(Integer forgo_toke_hat_forgo_eszkoz) {
		this.forgo_toke_hat_forgo_eszkoz = forgo_toke_hat_forgo_eszkoz;
	}

	public Integer getForgo_toke_hat_rovid_lej_kot() {
		return forgo_toke_hat_rovid_lej_kot;
	}

	public void setForgo_toke_hat_rovid_lej_kot(Integer forgo_toke_hat_rovid_lej_kot) {
		this.forgo_toke_hat_rovid_lej_kot = forgo_toke_hat_rovid_lej_kot;
	}

	public Integer getTargyev_forgo_toke_hat() {
		return targyev_forgo_toke_hat;
	}

	public void setTargyev_forgo_toke_hat(Integer targyev_forgo_toke_hat) {
		this.targyev_forgo_toke_hat = targyev_forgo_toke_hat;
	}

	public Integer getElozoev_forgo_toke_hat() {
		return elozoev_forgo_toke_hat;
	}

	public void setElozoev_forgo_toke_hat(Integer elozoev_forgo_toke_hat) {
		this.elozoev_forgo_toke_hat = elozoev_forgo_toke_hat;
	}

	public Integer getKonkurencia_forgo_toke_hat() {
		return konkurencia_forgo_toke_hat;
	}

	public void setKonkurencia_forgo_toke_hat(Integer konkurencia_forgo_toke_hat) {
		this.konkurencia_forgo_toke_hat = konkurencia_forgo_toke_hat;
	}


	// Vall.PenzugyiElemzes
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "PENZELEMZES_ID")
	@JsonBackReference
	private VallalatPenzugyiElemzes hatPenzElemzes;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public VallalatPenzugyiElemzes getHatPenzElemzes() {
		return hatPenzElemzes;
	}

	public void setHatPenzElemzes(VallalatPenzugyiElemzes hatPenzElemzes) {
		this.hatPenzElemzes = hatPenzElemzes;
	}

	
}
