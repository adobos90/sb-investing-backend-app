package springsbinvesting.domain.reszvenyelemzes.penzugyielemzes;

import javax.persistence.Column;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import springsbinvesting.domain.reszvenyelemzes.Manageles;
import springsbinvesting.domain.reszvenyelemzes.VallalatPenzugyiElemzes;

@Entity
@Table(name = "vallalatpenzugyjov")
public class PenzugyiElemzesJovedelem {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column
	private String jov_megjegyzes;
	@Column
	  private Integer netto_eszkoz ;
	@Column
	  private Integer eszkozok_eszkoz ;
	@Column  
	private Integer targyev_eszkoz ;
	@Column  
	private Integer elozoev_eszkoz ;
	@Column
	  private Integer konkurencia_eszkoz ;
	@Column
	  private Integer netto_sajat ;
	@Column
	  private Integer ossz_sajat_toke ;
	@Column
	  private Integer targyev_sajat ;
	@Column
	  private Integer elozoev_sajat ;
	  @Column
	  private Integer konkurencia_sajat ;
	  @Column
	  private Integer netto_reszveny ;
	  @Column
	  private Integer reszveny_db ;
	  @Column
	  private Integer targyev_reszveny ;
	  @Column
	  private Integer elozoev_reszveny ;
	  @Column
	  private Integer konkurencia_reszveny ;
	  @Column
	  private Integer netto_arbevetel ;
	  @Column
	  private Integer arbevetel ;
	  @Column
	  private Integer targyev_arbevetel ;
	  @Column
	  private Integer elozoev_arbevetel ;
	  @Column
	  private Integer konkurencia_arbevetel ;
	  @Column
	  private Integer brutto_fedezet ;
	  @Column
	  private Integer eladott_aruk_ktg ;
	  @Column
	  private Integer netto_fedezet ;
	  @Column
	  private Integer targyev_fedezet ;
	  @Column
	  private Integer elozoev_fedezet ;

	@Column
	private Integer konkurencia_fedezet ;

	public String getJov_megjegyzes() {
		return jov_megjegyzes;
	}

	public void setJov_megjegyzes(String jov_megjegyzes) {
		this.jov_megjegyzes = jov_megjegyzes;
	}

	public Integer getNetto_eszkoz() {
		return netto_eszkoz;
	}

	public void setNetto_eszkoz(Integer netto_eszkoz) {
		this.netto_eszkoz = netto_eszkoz;
	}

	public Integer getEszkozok_eszkoz() {
		return eszkozok_eszkoz;
	}

	public void setEszkozok_eszkoz(Integer eszkozok_eszkoz) {
		this.eszkozok_eszkoz = eszkozok_eszkoz;
	}

	public Integer getTargyev_eszkoz() {
		return targyev_eszkoz;
	}

	public void setTargyev_eszkoz(Integer targyev_eszkoz) {
		this.targyev_eszkoz = targyev_eszkoz;
	}

	public Integer getElozoev_eszkoz() {
		return elozoev_eszkoz;
	}

	public void setElozoev_eszkoz(Integer elozoev_eszkoz) {
		this.elozoev_eszkoz = elozoev_eszkoz;
	}

	public Integer getKonkurencia_eszkoz() {
		return konkurencia_eszkoz;
	}

	public void setKonkurencia_eszkoz(Integer konkurencia_eszkoz) {
		this.konkurencia_eszkoz = konkurencia_eszkoz;
	}

	public Integer getNetto_sajat() {
		return netto_sajat;
	}

	public void setNetto_sajat(Integer netto_sajat) {
		this.netto_sajat = netto_sajat;
	}

	public Integer getOssz_sajat_toke() {
		return ossz_sajat_toke;
	}

	public void setOssz_sajatToke(Integer ossz_sajat_toke) {
		this.ossz_sajat_toke = ossz_sajat_toke;
	}

	public Integer getTargyev_sajat() {
		return targyev_sajat;
	}

	public void setTargyev_sajat(Integer targyev_sajat) {
		this.targyev_sajat = targyev_sajat;
	}

	public Integer getElozoev_sajat() {
		return elozoev_sajat;
	}

	public void setElozoev_sajat(Integer elozoev_sajat) {
		this.elozoev_sajat = elozoev_sajat;
	}

	public Integer getKonkurencia_sajat() {
		return konkurencia_sajat;
	}

	public void setKonkurencia_sajat(Integer konkurencia_sajat) {
		this.konkurencia_sajat = konkurencia_sajat;
	}

	public Integer getNetto_reszveny() {
		return netto_reszveny;
	}

	public void setNetto_reszveny(Integer netto_reszveny) {
		this.netto_reszveny = netto_reszveny;
	}

	public Integer getReszveny_db() {
		return reszveny_db;
	}

	public void setReszveny_db(Integer reszveny_db) {
		this.reszveny_db = reszveny_db;
	}

	public Integer getTargyev_reszveny() {
		return targyev_reszveny;
	}

	public void setTargyev_reszveny(Integer targyev_reszveny) {
		this.targyev_reszveny = targyev_reszveny;
	}

	public Integer getElozoev_reszveny() {
		return elozoev_reszveny;
	}

	public void setElozoev_reszveny(Integer elozoev_reszveny) {
		this.elozoev_reszveny = elozoev_reszveny;
	}

	public Integer getKonkurencia_reszveny() {
		return konkurencia_reszveny;
	}

	public void setKonkurencia_reszveny(Integer konkurencia_reszveny) {
		this.konkurencia_reszveny = konkurencia_reszveny;
	}

	public Integer getNetto_arbevetel() {
		return netto_arbevetel;
	}

	public void setNetto_arbevetel(Integer netto_arbevetel) {
		this.netto_arbevetel = netto_arbevetel;
	}

	public Integer getArbevetel() {
		return arbevetel;
	}

	public void setArbevetel(Integer arbevetel) {
		this.arbevetel = arbevetel;
	}

	public Integer getTargyev_arbevetel() {
		return targyev_arbevetel;
	}

	public void setTargyev_arbevetel(Integer targyev_arbevetel) {
		this.targyev_arbevetel = targyev_arbevetel;
	}

	public Integer getElozoev_arbevetel() {
		return elozoev_arbevetel;
	}

	public void setElozoev_arbevetel(Integer elozoev_arbevetel) {
		this.elozoev_arbevetel = elozoev_arbevetel;
	}

	public Integer getKonkurencia_arbevetel() {
		return konkurencia_arbevetel;
	}

	public void setKonkurencia_arbevetel(Integer konkurencia_arbevetel) {
		this.konkurencia_arbevetel = konkurencia_arbevetel;
	}

	public Integer getBrutto_fedezet() {
		return brutto_fedezet;
	}

	public void setBrutto_fedezet(Integer brutto_fedezet) {
		this.brutto_fedezet = brutto_fedezet;
	}

	public Integer getEladott_aruk_ktg() {
		return eladott_aruk_ktg;
	}

	public void setEladott_aruk_ktg(Integer eladott_aruk_ktg) {
		this.eladott_aruk_ktg = eladott_aruk_ktg;
	}

	public Integer getNetto_fedezet() {
		return netto_fedezet;
	}

	public void setNetto_fedezet(Integer netto_fedezet) {
		this.netto_fedezet = netto_fedezet;
	}

	public Integer getTargyev_fedezet() {
		return targyev_fedezet;
	}

	public void setTargyev_fedezet(Integer targyev_fedezet) {
		this.targyev_fedezet = targyev_fedezet;
	}

	public Integer getElozoev_fedezet() {
		return elozoev_fedezet;
	}

	public void setElozoev_fedezet(Integer elozoev_fedezet) {
		this.elozoev_fedezet = elozoev_fedezet;
	}

	public Integer getKonkurencia_fedezet() {
		return konkurencia_fedezet;
	}

	public void setKonkurencia_fedezet(Integer konkurencia_fedezet) {
		this.konkurencia_fedezet = konkurencia_fedezet;
	}


	  
	  // Vall.PenzugyiElemzes
	  @ManyToOne(cascade = CascadeType.ALL)
		@JoinColumn(name = "PENZELEMZES_ID")
		@JsonBackReference
		private VallalatPenzugyiElemzes jovPenzElemzes;
	  
	  public PenzugyiElemzesJovedelem() {}

	public VallalatPenzugyiElemzes getJovPenzElemzes() {
		return jovPenzElemzes;
	}

	public void setJovPenzElemzes(VallalatPenzugyiElemzes jovPenzElemzes) {
		this.jovPenzElemzes = jovPenzElemzes;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	  
	  
}
