package springsbinvesting.domain.reszvenyelemzes.penzugyielemzes;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import springsbinvesting.domain.reszvenyelemzes.VallalatPenzugyiElemzes;

@Entity
@Table(name = "vallalatpenzugypiaci")
public class PenzugyiElemzesPiaci {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	  private String piaci_megjegyzes;
	  private Integer piaci_arfolyam_nyereseg;
	  private Integer piaci_tars_netto_eredmeny;
	  private Integer piaci_reszvenyek_db_szama;
	  private Integer targyev_piaci_arfolyam_nyereseg;
	  private Integer elozoev_piaci_arfolyam_nyereseg;
	  private Integer konkurencia_piaci_arfolyam_nyereseg;
	  private Integer piaci_bef_kam_netto_eredmeny;
	  private Integer piaci_bef_kam_reszv_db_szama;
	  private Integer piaci_bef_kam_reszveny_arfolyama;
	  private Integer targyev_piaci_bef_kamatlab;
	  private Integer elozoev_piaci_bef_kamatlab;
	  private Integer konkurencia_piaci_bef_kamatlab;
	  private Integer piaci_konyv_reszveny_arfolyam;
	  private Integer piaci_konyv_sajat_toke;
	  private Integer piaci_konyv_goodwill;
	  private Integer piaci_konyv_imm_javak;
	  private Integer piaci_konyv_reszvenyek_db_szama;
	  private Integer targyev_piaci_konyv;
	  private Integer elozoev_piaci_konyv;
	private Integer konkurencia_piaci_konyv;


	public String getPiaci_megjegyzes() {
		return piaci_megjegyzes;
	}

	public void setPiaci_megjegyzes(String piaci_megjegyzes) {
		this.piaci_megjegyzes = piaci_megjegyzes;
	}

	public Integer getPiaci_arfolyam_nyereseg() {
		return piaci_arfolyam_nyereseg;
	}

	public void setPiaci_arfolyam_nyereseg(Integer piaci_arfolyam_nyereseg) {
		this.piaci_arfolyam_nyereseg = piaci_arfolyam_nyereseg;
	}

	public Integer getPiaci_tars_netto_eredmeny() {
		return piaci_tars_netto_eredmeny;
	}

	public void setPiaci_tars_netto_eredmeny(Integer piaci_tars_netto_eredmeny) {
		this.piaci_tars_netto_eredmeny = piaci_tars_netto_eredmeny;
	}

	public Integer getPiaci_reszvenyek_db_szama() {
		return piaci_reszvenyek_db_szama;
	}

	public void setPiaci_reszvenyek_db_szama(Integer piaci_reszvenyek_db_szama) {
		this.piaci_reszvenyek_db_szama = piaci_reszvenyek_db_szama;
	}

	public Integer getTargyev_piaci_arfolyam_nyereseg() {
		return targyev_piaci_arfolyam_nyereseg;
	}

	public void setTargyev_piaci_arfolyam_nyereseg(Integer targyev_piaci_arfolyam_nyereseg) {
		this.targyev_piaci_arfolyam_nyereseg = targyev_piaci_arfolyam_nyereseg;
	}

	public Integer getElozoev_piaci_arfolyam_nyereseg() {
		return elozoev_piaci_arfolyam_nyereseg;
	}

	public void setElozoev_piaci_arfolyam_nyereseg(Integer elozoev_piaci_arfolyam_nyereseg) {
		this.elozoev_piaci_arfolyam_nyereseg = elozoev_piaci_arfolyam_nyereseg;
	}

	public Integer getKonkurencia_piaci_arfolyam_nyereseg() {
		return konkurencia_piaci_arfolyam_nyereseg;
	}

	public void setKonkurencia_piaci_arfolyam_nyereseg(Integer konkurencia_piaci_arfolyam_nyereseg) {
		this.konkurencia_piaci_arfolyam_nyereseg = konkurencia_piaci_arfolyam_nyereseg;
	}

	public Integer getPiaci_bef_kam_netto_eredmeny() {
		return piaci_bef_kam_netto_eredmeny;
	}

	public void setPiaci_bef_kam_netto_eredmeny(Integer piaci_bef_kam_netto_eredmeny) {
		this.piaci_bef_kam_netto_eredmeny = piaci_bef_kam_netto_eredmeny;
	}

	public Integer getPiaci_bef_kam_reszv_db_szama() {
		return piaci_bef_kam_reszv_db_szama;
	}

	public void setPiaci_bef_kam_reszv_db_szama(Integer piaci_bef_kam_reszv_db_szama) {
		this.piaci_bef_kam_reszv_db_szama = piaci_bef_kam_reszv_db_szama;
	}

	public Integer getPiaci_bef_kam_reszveny_arfolyama() {
		return piaci_bef_kam_reszveny_arfolyama;
	}

	public void setPiaci_bef_kam_reszveny_arfolyama(Integer piaci_bef_kam_reszveny_arfolyama) {
		this.piaci_bef_kam_reszveny_arfolyama = piaci_bef_kam_reszveny_arfolyama;
	}

	public Integer getTargyev_piaci_bef_kamatlab() {
		return targyev_piaci_bef_kamatlab;
	}

	public void setTargyev_piaci_bef_kamatlab(Integer targyev_piaci_bef_kamatlab) {
		this.targyev_piaci_bef_kamatlab = targyev_piaci_bef_kamatlab;
	}

	public Integer getElozoev_piaci_bef_kamatlab() {
		return elozoev_piaci_bef_kamatlab;
	}

	public void setElozoev_piaci_bef_kamatlab(Integer elozoev_piaci_bef_kamatlab) {
		this.elozoev_piaci_bef_kamatlab = elozoev_piaci_bef_kamatlab;
	}

	public Integer getKonkurencia_piaci_bef_kamatlab() {
		return konkurencia_piaci_bef_kamatlab;
	}

	public void setKonkurencia_piaci_bef_kamatlab(Integer konkurencia_piaci_bef_kamatlab) {
		this.konkurencia_piaci_bef_kamatlab = konkurencia_piaci_bef_kamatlab;
	}

	public Integer getPiaci_konyv_reszveny_arfolyam() {
		return piaci_konyv_reszveny_arfolyam;
	}

	public void setPiaci_konyv_reszveny_arfolyam(Integer piaci_konyv_reszveny_arfolyam) {
		this.piaci_konyv_reszveny_arfolyam = piaci_konyv_reszveny_arfolyam;
	}

	public Integer getPiaci_konyv_sajat_toke() {
		return piaci_konyv_sajat_toke;
	}

	public void setPiaci_konyv_sajat_toke(Integer piaci_konyv_sajat_toke) {
		this.piaci_konyv_sajat_toke = piaci_konyv_sajat_toke;
	}

	public Integer getPiaci_konyv_goodwill() {
		return piaci_konyv_goodwill;
	}

	public void setPiaci_konyv_goodwill(Integer piaci_konyv_goodwill) {
		this.piaci_konyv_goodwill = piaci_konyv_goodwill;
	}

	public Integer getPiaci_konyv_imm_javak() {
		return piaci_konyv_imm_javak;
	}

	public void setPiaci_konyv_imm_javak(Integer piaci_konyv_imm_javak) {
		this.piaci_konyv_imm_javak = piaci_konyv_imm_javak;
	}

	public Integer getPiaci_konyv_reszvenyek_db_szama() {
		return piaci_konyv_reszvenyek_db_szama;
	}

	public void setPiaci_konyv_reszvenyek_db_szama(Integer piaci_konyv_reszvenyek_db_szama) {
		this.piaci_konyv_reszvenyek_db_szama = piaci_konyv_reszvenyek_db_szama;
	}

	public Integer getTargyev_piaci_konyv() {
		return targyev_piaci_konyv;
	}

	public void setTargyev_piaci_konyv(Integer targyev_piaci_konyv) {
		this.targyev_piaci_konyv = targyev_piaci_konyv;
	}

	public Integer getElozoev_piaci_konyv() {
		return elozoev_piaci_konyv;
	}

	public void setElozoev_piaci_konyv(Integer elozoev_piaci_konyv) {
		this.elozoev_piaci_konyv = elozoev_piaci_konyv;
	}

	public Integer getKonkurencia_piaci_konyv() {
		return konkurencia_piaci_konyv;
	}

	public void setKonkurencia_piaci_konyv(Integer konkurencia_piaci_konyv) {
		this.konkurencia_piaci_konyv = konkurencia_piaci_konyv;
	}


	// Vall.PenzugyiElemzes
		@ManyToOne(cascade = CascadeType.ALL)
		@JoinColumn(name = "PENZELEMZES_ID")
		@JsonBackReference
		private VallalatPenzugyiElemzes piaciPenzElemzes;

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}



		public VallalatPenzugyiElemzes getPiaciPenzElemzes() {
			return piaciPenzElemzes;
		}

		public void setPiaciPenzElemzes(VallalatPenzugyiElemzes piaciPenzElemzes) {
			this.piaciPenzElemzes = piaciPenzElemzes;
		}
}
