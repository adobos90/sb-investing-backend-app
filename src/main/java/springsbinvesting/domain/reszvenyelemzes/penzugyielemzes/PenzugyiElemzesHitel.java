package springsbinvesting.domain.reszvenyelemzes.penzugyielemzes;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import springsbinvesting.domain.reszvenyelemzes.VallalatPenzugyiElemzes;

@Entity
@Table(name = "vallalatpenzugyhitel")
public class PenzugyiElemzesHitel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String hitel_megjegyzes;
	private Integer hitel_sajat_toke;
	private Integer hitel_imm_javak;
	private Integer hitel_jovairas;
	private Integer hitel_ossz_hossz_lej_kot;
	private Integer targyev_sajat_toke_hitel;
	private Integer elozoev_sajat_toke_hitel;
	private Integer konkurencia_sajat_toke_hitel;
	private Integer hitel_kamat_jov_ado_elotti_eredmeny;
	private Integer hitel_elszamolt_amortizacio;
	private Integer hitel_kamat;
	private Integer hitel_kifizetett_osztalek;
	private Integer targyev_kamat_hitel;
	private Integer elozoev_kamat_hitel;
	private Integer konkurencia_kamat_hitel;
	private Integer hitel_rovid_likv_osszforgo_eszkoz;
	private Integer hitel_rovid_likv_kotelezettsegek;
	private Integer targyev_rovid_likv_hitel;
	private Integer elozoev_rovid_likv_hitel;
	private Integer konkurencia_rovid_likv_hitel;
	private Integer hitel_likv_osszforgo_eszkoz;
	private Integer hitel_likv_keszletek;

	private Integer hitel_likv_rovid_lej_kot;
	private Integer targyev_likv_hitel;
	private Integer elozoev_likv_hitel;
	private Integer konkurencia_likv_hitel;

	public String getHitel_megjegyzes() {
		return hitel_megjegyzes;
	}

	public void setHitel_megjegyzes(String hitel_megjegyzes) {
		this.hitel_megjegyzes = hitel_megjegyzes;
	}

	public Integer getHitel_sajat_toke() {
		return hitel_sajat_toke;
	}

	public void setHitel_sajat_toke(Integer hitel_sajat_toke) {
		this.hitel_sajat_toke = hitel_sajat_toke;
	}

	public Integer getHitel_imm_javak() {
		return hitel_imm_javak;
	}

	public void setHitel_imm_javak(Integer hitel_imm_javak) {
		this.hitel_imm_javak = hitel_imm_javak;
	}

	public Integer getHitel_jovairas() {
		return hitel_jovairas;
	}

	public void setHitel_jovairas(Integer hitel_jovairas) {
		this.hitel_jovairas = hitel_jovairas;
	}

	public Integer getHitel_ossz_hossz_lej_kot() {
		return hitel_ossz_hossz_lej_kot;
	}

	public void setHitel_ossz_hossz_lej_kot(Integer hitel_ossz_hossz_lej_kot) {
		this.hitel_ossz_hossz_lej_kot = hitel_ossz_hossz_lej_kot;
	}

	public Integer getTargyev_sajat_toke_hitel() {
		return targyev_sajat_toke_hitel;
	}

	public void setTargyev_sajat_toke_hitel(Integer targyev_sajat_toke_hitel) {
		this.targyev_sajat_toke_hitel = targyev_sajat_toke_hitel;
	}

	public Integer getElozoev_sajat_toke_hitel() {
		return elozoev_sajat_toke_hitel;
	}

	public void setElozoev_sajat_toke_hitel(Integer elozoev_sajat_toke_hitel) {
		this.elozoev_sajat_toke_hitel = elozoev_sajat_toke_hitel;
	}

	public Integer getKonkurencia_sajat_toke_hitel() {
		return konkurencia_sajat_toke_hitel;
	}

	public void setKonkurencia_sajat_toke_hitel(Integer konkurencia_sajat_toke_hitel) {
		this.konkurencia_sajat_toke_hitel = konkurencia_sajat_toke_hitel;
	}

	public Integer getHitel_kamat_jov_ado_elotti_eredmeny() {
		return hitel_kamat_jov_ado_elotti_eredmeny;
	}

	public void setHitel_kamat_jov_ado_elotti_eredmeny(Integer hitel_kamat_jov_ado_elotti_eredmeny) {
		this.hitel_kamat_jov_ado_elotti_eredmeny = hitel_kamat_jov_ado_elotti_eredmeny;
	}

	public Integer getHitel_elszamolt_amortizacio() {
		return hitel_elszamolt_amortizacio;
	}

	public void setHitel_elszamolt_amortizacio(Integer hitel_elszamolt_amortizacio) {
		this.hitel_elszamolt_amortizacio = hitel_elszamolt_amortizacio;
	}

	public Integer getHitel_kamat() {
		return hitel_kamat;
	}

	public void setHitel_kamat(Integer hitel_kamat) {
		this.hitel_kamat = hitel_kamat;
	}

	public Integer getHitel_kifizetett_osztalek() {
		return hitel_kifizetett_osztalek;
	}

	public void setHitel_kifizetett_osztalek(Integer hitel_kifizetett_osztalek) {
		this.hitel_kifizetett_osztalek = hitel_kifizetett_osztalek;
	}

	public Integer getTargyev_kamat_hitel() {
		return targyev_kamat_hitel;
	}

	public void setTargyev_kamat_hitel(Integer targyev_kamat_hitel) {
		this.targyev_kamat_hitel = targyev_kamat_hitel;
	}

	public Integer getElozoev_kamat_hitel() {
		return elozoev_kamat_hitel;
	}

	public void setElozoev_kamat_hitel(Integer elozoev_kamat_hitel) {
		this.elozoev_kamat_hitel = elozoev_kamat_hitel;
	}

	public Integer getKonkurencia_kamat_hitel() {
		return konkurencia_kamat_hitel;
	}

	public void setKonkurencia_kamat_hitel(Integer konkurencia_kamat_hitel) {
		this.konkurencia_kamat_hitel = konkurencia_kamat_hitel;
	}

	public Integer getHitel_rovid_likv_osszforgo_eszkoz() {
		return hitel_rovid_likv_osszforgo_eszkoz;
	}

	public void setHitel_rovid_likv_osszforgo_eszkoz(Integer hitel_rovid_likv_osszforgo_eszkoz) {
		this.hitel_rovid_likv_osszforgo_eszkoz = hitel_rovid_likv_osszforgo_eszkoz;
	}

	public Integer getHitel_rovid_likv_kotelezettsegek() {
		return hitel_rovid_likv_kotelezettsegek;
	}

	public void setHitel_rovid_likv_kotelezettsegek(Integer hitel_rovid_likv_kotelezettsegek) {
		this.hitel_rovid_likv_kotelezettsegek = hitel_rovid_likv_kotelezettsegek;
	}

	public Integer getTargyev_rovid_likv_hitel() {
		return targyev_rovid_likv_hitel;
	}

	public void setTargyev_rovid_likv_hitel(Integer targyev_rovid_likv_hitel) {
		this.targyev_rovid_likv_hitel = targyev_rovid_likv_hitel;
	}

	public Integer getElozoev_rovid_likv_hitel() {
		return elozoev_rovid_likv_hitel;
	}

	public void setElozoev_rovid_likv_hitel(Integer elozoev_rovid_likv_hitel) {
		this.elozoev_rovid_likv_hitel = elozoev_rovid_likv_hitel;
	}

	public Integer getKonkurencia_rovid_likv_hitel() {
		return konkurencia_rovid_likv_hitel;
	}

	public void setKonkurencia_rovid_likv_hitel(Integer konkurencia_rovid_likv_hitel) {
		this.konkurencia_rovid_likv_hitel = konkurencia_rovid_likv_hitel;
	}

	public Integer getHitel_likv_osszforgo_eszkoz() {
		return hitel_likv_osszforgo_eszkoz;
	}

	public void setHitel_likv_osszforgo_eszkoz(Integer hitel_likv_osszforgo_eszkoz) {
		this.hitel_likv_osszforgo_eszkoz = hitel_likv_osszforgo_eszkoz;
	}

	public Integer getHitel_likv_keszletek() {
		return hitel_likv_keszletek;
	}

	public void setHitel_likv_keszletek(Integer hitel_likv_keszletek) {
		this.hitel_likv_keszletek = hitel_likv_keszletek;
	}

	public Integer getHitel_likv_rovid_lej_kot() {
		return hitel_likv_rovid_lej_kot;
	}

	public void setHitel_likv_rovid_lej_kot(Integer hitel_likv_rovid_lej_kot) {
		this.hitel_likv_rovid_lej_kot = hitel_likv_rovid_lej_kot;
	}

	public Integer getTargyev_likv_hitel() {
		return targyev_likv_hitel;
	}

	public void setTargyev_likv_hitel(Integer targyev_likv_hitel) {
		this.targyev_likv_hitel = targyev_likv_hitel;
	}

	public Integer getElozoev_likv_hitel() {
		return elozoev_likv_hitel;
	}

	public void setElozoev_likv_hitel(Integer elozoev_likv_hitel) {
		this.elozoev_likv_hitel = elozoev_likv_hitel;
	}

	public Integer getKonkurencia_likv_hitel() {
		return konkurencia_likv_hitel;
	}

	public void setKonkurencia_likv_hitel(Integer konkurencia_likv_hitel) {
		this.konkurencia_likv_hitel = konkurencia_likv_hitel;
	}


	// Vall.PenzugyiElemzes
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "PENZELEMZES_ID")
	@JsonBackReference
	private VallalatPenzugyiElemzes hitelPenzElemzes;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}



	public VallalatPenzugyiElemzes getHitelPenzElemzes() {
		return hitelPenzElemzes;
	}

	public void setHitelPenzElemzes(VallalatPenzugyiElemzes hitelPenzElemzes) {
		this.hitelPenzElemzes = hitelPenzElemzes;
	}
	
	
}
