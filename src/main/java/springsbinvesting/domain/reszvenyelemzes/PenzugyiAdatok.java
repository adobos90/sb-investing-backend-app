package springsbinvesting.domain.reszvenyelemzes;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import springsbinvesting.domain.Reszveny;

@Entity
@Table(name = "penzugyi_adatok")
public class PenzugyiAdatok {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private Integer netto_targyev;
	private Integer netto_elozo_ev;
	private Integer netto_konkurencia;

	private Integer netto_q2;
	private Integer netto_q3;
	private Integer eladott_aru_targyev;
	private Integer eladott_aru_elozo_ev;
	private Integer eladott_aru_konkurencia;
	private Integer eladott_aru_q2;
	private Integer eladott_aru_q3;
	private Integer tarsasag_targyev;
	private Integer tarsasag_elozo_ev;
	private Integer tarsasag_konkurencia;
	private Integer tarsasag_q2;
	private Integer tarsasag_q3;
	private Integer kamat_targyev;
	private Integer kamat_elozo_ev;
	private Integer kamat_konkurencia;
	private Integer kamat_q2;
	private Integer kamat_q3;
	private Integer fizetett_kamatok_targyev;
	private Integer fizetett_kamatok_elozo_ev;
	private Integer fizetett_kamatok_konkurencia;
	private Integer fizetett_kamatok_q2;
	private Integer fizetett_kamatok_q3;
	private Integer adm_targyev;
	private Integer adm_elozo_ev;
	private Integer adm_konkurencia;
	private Integer adm_q2;
	private Integer adm_q3;
	private Integer egyeb_targyev;
	private Integer egyeb_elozo_ev;
	private Integer egyeb_konkurencia;
	private Integer egyeb_q2;
	private Integer egyeb_q3;
	private Integer reszveny_targyev;
	private Integer reszveny_elozo_ev;
	private Integer reszveny_konkurencia;
	private Integer reszveny_q2;
	private Integer reszveny_q3;
	private Integer osztalek_targyev;
	private Integer osztalek_elozo_ev;
	private Integer osztalek_konkurencia;
	private Integer osztalek_q2;
	private Integer osztalek_q3;
	private Integer elszamolt_targyev;
	private Integer elszamolt_elozo_ev;
	private Integer elszamolt_konkurencia;
	private Integer elszamolt_q2;
	private Integer elszamolt_q3;
	private Integer keszpenz_targyev;
	private Integer keszpenz_elozo_ev;
	private Integer keszpenz_konkurencia;
	private Integer keszpenz_q2;
	private Integer keszpenz_q3;
	private Integer koveteles_vevo_targyev;
	private Integer koveteles_vevo_elozo_ev;
	private Integer koveteles_vevo_konkurencia;
	private Integer koveteles_vevo_q2;
	private Integer koveteles_vevo_q3;
	private Integer keszlet_targyev;
	private Integer keszlet_elozo_ev;
	private Integer keszlet_konkurencia;
	private Integer keszlet_q2;
	private Integer keszlet_q3;
	private Integer jovairas_targyev;
	private Integer jovairas_elozo_ev;
	private Integer jovairas_konkurencia;
	private Integer jovairas_q2;
	private Integer jovairas_q3;
	private Integer imm_targyev;
	private Integer imm_elozo_ev;
	private Integer imm_konkurencia;
	private Integer imm_q2;
	private Integer imm_q3;
	private Integer forgo_targyev;
	private Integer forgo_elozo_ev;
	private Integer forgo_konkurencia;
	private Integer forgo_q2;
	private Integer forgo_q3;
	private Integer eszkoz_targyev;
	private Integer eszkoz_elozo_ev;
	private Integer eszkoz_konkurencia;
	private Integer eszkoz_q2;
	private Integer eszkoz_q3;
	private Integer kot_szallito_targyev;
	private Integer kot_szallito_elozo_ev;
	private Integer kot_szallito_konkurencia;
	private Integer kot_szallito_q2;
	private Integer kot_szallito_q3;
	private Integer toke_targyev;
	private Integer toke_elozo_ev;
	private Integer toke_konkurencia;
	private Integer toke_q2;
	private Integer toke_q3;
	private Integer rovid_lejaratu_kot_targyev;
	private Integer rovid_lejaratu_kot_elozo_ev;
	private Integer rovid_lejaratu_kot_konkurencia;
	private Integer rovid_lejaratu_kot_q2;
	private Integer rovid_lejaratu_kot_q3;
	private Integer ossz_kot_targyev;
	private Integer ossz_kot_elozo_ev;
	private Integer ossz_kot_konkurencia;
	private Integer ossz_kot_q2;
	private Integer ossz_kot_q3;
	private Integer alkalmazottak_szama_targyev;
	private Integer alkalmazottak_szama_elozo_ev;
	private Integer alkalmazottak_szama_konkurencia;
	private Integer alkalmazottak_szama_q2;
	private Integer alkalmazottak_szama_q3;
	private Integer reszveny_arfolyam_targyev;
	private Integer reszveny_arfolyam_elozo_ev;
	private Integer reszveny_arfolyam_konkurencia;
	private Integer reszveny_arfolyam_q2;
	private Integer reszveny_arfolyam_q3;
	private Integer naptar_targyev;
	private Integer naptar_elozo_ev;
	private Integer naptar_konkurencia;
	private Integer naptar_q2;
	private Integer naptar_q3;
	private Integer hozam_targyev;
	private Integer hozam_elozo_ev;
	private Integer hozam_konkurencia;
	private Integer hozam_q2;
	private Integer hozam_q3;

	public Integer getNetto_targyev() {
		return netto_targyev;
	}

	public void setNetto_targyev(Integer netto_targyev) {
		this.netto_targyev = netto_targyev;
	}

	public Integer getNetto_elozo_ev() {
		return netto_elozo_ev;
	}

	public void setNetto_elozo_ev(Integer netto_elozo_ev) {
		this.netto_elozo_ev = netto_elozo_ev;
	}

	public Integer getNetto_konkurencia() {
		return netto_konkurencia;
	}

	public void setNetto_konkurencia(Integer netto_konkurencia) {
		this.netto_konkurencia = netto_konkurencia;
	}

	public Integer getNetto_q2() {
		return netto_q2;
	}

	public void setNetto_q2(Integer netto_q2) {
		this.netto_q2 = netto_q2;
	}

	public Integer getNetto_q3() {
		return netto_q3;
	}

	public void setNetto_q3(Integer netto_q3) {
		this.netto_q3 = netto_q3;
	}

	public Integer getEladott_aru_targyev() {
		return eladott_aru_targyev;
	}

	public void setEladott_aru_targyev(Integer eladott_aru_targyev) {
		this.eladott_aru_targyev = eladott_aru_targyev;
	}

	public Integer getEladott_aru_elozo_ev() {
		return eladott_aru_elozo_ev;
	}

	public void setEladott_aru_elozo_ev(Integer eladott_aru_elozo_ev) {
		this.eladott_aru_elozo_ev = eladott_aru_elozo_ev;
	}

	public Integer getEladott_aru_konkurencia() {
		return eladott_aru_konkurencia;
	}

	public void setEladott_aru_konkurencia(Integer eladott_aru_konkurencia) {
		this.eladott_aru_konkurencia = eladott_aru_konkurencia;
	}

	public Integer getEladott_aru_q2() {
		return eladott_aru_q2;
	}

	public void setEladott_aru_q2(Integer eladott_aru_q2) {
		this.eladott_aru_q2 = eladott_aru_q2;
	}

	public Integer getEladott_aru_q3() {
		return eladott_aru_q3;
	}

	public void setEladott_aru_q3(Integer eladott_aru_q3) {
		this.eladott_aru_q3 = eladott_aru_q3;
	}

	public Integer getTarsasag_targyev() {
		return tarsasag_targyev;
	}

	public void setTarsasag_targyev(Integer tarsasag_targyev) {
		this.tarsasag_targyev = tarsasag_targyev;
	}

	public Integer getTarsasag_elozo_ev() {
		return tarsasag_elozo_ev;
	}

	public void setTarsasag_elozo_ev(Integer tarsasag_elozo_ev) {
		this.tarsasag_elozo_ev = tarsasag_elozo_ev;
	}

	public Integer getTarsasag_konkurencia() {
		return tarsasag_konkurencia;
	}

	public void setTarsasag_konkurencia(Integer tarsasag_konkurencia) {
		this.tarsasag_konkurencia = tarsasag_konkurencia;
	}

	public Integer getTarsasag_q2() {
		return tarsasag_q2;
	}

	public void setTarsasag_q2(Integer tarsasag_q2) {
		this.tarsasag_q2 = tarsasag_q2;
	}

	public Integer getTarsasag_q3() {
		return tarsasag_q3;
	}

	public void setTarsasag_q3(Integer tarsasag_q3) {
		this.tarsasag_q3 = tarsasag_q3;
	}

	public Integer getKamat_targyev() {
		return kamat_targyev;
	}

	public void setKamat_targyev(Integer kamat_targyev) {
		this.kamat_targyev = kamat_targyev;
	}

	public Integer getKamat_elozo_ev() {
		return kamat_elozo_ev;
	}

	public void setKamat_elozo_ev(Integer kamat_elozo_ev) {
		this.kamat_elozo_ev = kamat_elozo_ev;
	}

	public Integer getKamat_konkurencia() {
		return kamat_konkurencia;
	}

	public void setKamat_konkurencia(Integer kamat_konkurencia) {
		this.kamat_konkurencia = kamat_konkurencia;
	}

	public Integer getKamat_q2() {
		return kamat_q2;
	}

	public void setKamat_q2(Integer kamat_q2) {
		this.kamat_q2 = kamat_q2;
	}

	public Integer getKamat_q3() {
		return kamat_q3;
	}

	public void setKamat_q3(Integer kamat_q3) {
		this.kamat_q3 = kamat_q3;
	}

	public Integer getFizetett_kamatok_targyev() {
		return fizetett_kamatok_targyev;
	}

	public void setFizetett_kamatok_targyev(Integer fizetett_kamatok_targyev) {
		this.fizetett_kamatok_targyev = fizetett_kamatok_targyev;
	}

	public Integer getFizetett_kamatok_elozo_ev() {
		return fizetett_kamatok_elozo_ev;
	}

	public void setFizetett_kamatok_elozo_ev(Integer fizetett_kamatok_elozo_ev) {
		this.fizetett_kamatok_elozo_ev = fizetett_kamatok_elozo_ev;
	}

	public Integer getFizetett_kamatok_konkurencia() {
		return fizetett_kamatok_konkurencia;
	}

	public void setFizetett_kamatok_konkurencia(Integer fizetett_kamatok_konkurencia) {
		this.fizetett_kamatok_konkurencia = fizetett_kamatok_konkurencia;
	}

	public Integer getFizetett_kamatok_q2() {
		return fizetett_kamatok_q2;
	}

	public void setFizetett_kamatok_q2(Integer fizetett_kamatok_q2) {
		this.fizetett_kamatok_q2 = fizetett_kamatok_q2;
	}

	public Integer getFizetett_kamatok_q3() {
		return fizetett_kamatok_q3;
	}

	public void setFizetett_kamatok_q3(Integer fizetett_kamatok_q3) {
		this.fizetett_kamatok_q3 = fizetett_kamatok_q3;
	}

	public Integer getAdm_targyev() {
		return adm_targyev;
	}

	public void setAdm_targyev(Integer adm_targyev) {
		this.adm_targyev = adm_targyev;
	}

	public Integer getAdm_elozo_ev() {
		return adm_elozo_ev;
	}

	public void setAdm_elozo_ev(Integer adm_elozo_ev) {
		this.adm_elozo_ev = adm_elozo_ev;
	}

	public Integer getAdm_konkurencia() {
		return adm_konkurencia;
	}

	public void setAdm_konkurencia(Integer adm_konkurencia) {
		this.adm_konkurencia = adm_konkurencia;
	}

	public Integer getAdm_q2() {
		return adm_q2;
	}

	public void setAdm_q2(Integer adm_q2) {
		this.adm_q2 = adm_q2;
	}

	public Integer getAdm_q3() {
		return adm_q3;
	}

	public void setAdm_q3(Integer adm_q3) {
		this.adm_q3 = adm_q3;
	}

	public Integer getEgyeb_targyev() {
		return egyeb_targyev;
	}

	public void setEgyeb_targyev(Integer egyeb_targyev) {
		this.egyeb_targyev = egyeb_targyev;
	}

	public Integer getEgyeb_elozo_ev() {
		return egyeb_elozo_ev;
	}

	public void setEgyeb_elozo_ev(Integer egyeb_elozo_ev) {
		this.egyeb_elozo_ev = egyeb_elozo_ev;
	}

	public Integer getEgyeb_konkurencia() {
		return egyeb_konkurencia;
	}

	public void setEgyeb_konkurencia(Integer egyeb_konkurencia) {
		this.egyeb_konkurencia = egyeb_konkurencia;
	}

	public Integer getEgyeb_q2() {
		return egyeb_q2;
	}

	public void setEgyeb_q2(Integer egyeb_q2) {
		this.egyeb_q2 = egyeb_q2;
	}

	public Integer getEgyeb_q3() {
		return egyeb_q3;
	}

	public void setEgyeb_q3(Integer egyeb_q3) {
		this.egyeb_q3 = egyeb_q3;
	}

	public Integer getReszveny_targyev() {
		return reszveny_targyev;
	}

	public void setReszveny_targyev(Integer reszveny_targyev) {
		this.reszveny_targyev = reszveny_targyev;
	}

	public Integer getReszveny_elozo_ev() {
		return reszveny_elozo_ev;
	}

	public void setReszveny_elozo_ev(Integer reszveny_elozo_ev) {
		this.reszveny_elozo_ev = reszveny_elozo_ev;
	}

	public Integer getReszveny_konkurencia() {
		return reszveny_konkurencia;
	}

	public void setReszveny_konkurencia(Integer reszveny_konkurencia) {
		this.reszveny_konkurencia = reszveny_konkurencia;
	}

	public Integer getReszveny_q2() {
		return reszveny_q2;
	}

	public void setReszveny_q2(Integer reszveny_q2) {
		this.reszveny_q2 = reszveny_q2;
	}

	public Integer getReszveny_q3() {
		return reszveny_q3;
	}

	public void setReszveny_q3(Integer reszveny_q3) {
		this.reszveny_q3 = reszveny_q3;
	}

	public Integer getOsztalek_targyev() {
		return osztalek_targyev;
	}

	public void setOsztalek_targyev(Integer osztalek_targyev) {
		this.osztalek_targyev = osztalek_targyev;
	}

	public Integer getOsztalek_elozo_ev() {
		return osztalek_elozo_ev;
	}

	public void setOsztalek_elozo_ev(Integer osztalek_elozo_ev) {
		this.osztalek_elozo_ev = osztalek_elozo_ev;
	}

	public Integer getOsztalek_konkurencia() {
		return osztalek_konkurencia;
	}

	public void setOsztalek_konkurencia(Integer osztalek_konkurencia) {
		this.osztalek_konkurencia = osztalek_konkurencia;
	}

	public Integer getOsztalek_q2() {
		return osztalek_q2;
	}

	public void setOsztalek_q2(Integer osztalek_q2) {
		this.osztalek_q2 = osztalek_q2;
	}

	public Integer getOsztalek_q3() {
		return osztalek_q3;
	}

	public void setOsztalek_q3(Integer osztalek_q3) {
		this.osztalek_q3 = osztalek_q3;
	}

	public Integer getElszamolt_targyev() {
		return elszamolt_targyev;
	}

	public void setElszamolt_targyev(Integer elszamolt_targyev) {
		this.elszamolt_targyev = elszamolt_targyev;
	}

	public Integer getElszamolt_elozo_ev() {
		return elszamolt_elozo_ev;
	}

	public void setElszamolt_elozo_ev(Integer elszamolt_elozo_ev) {
		this.elszamolt_elozo_ev = elszamolt_elozo_ev;
	}

	public Integer getElszamolt_konkurencia() {
		return elszamolt_konkurencia;
	}

	public void setElszamolt_konkurencia(Integer elszamolt_konkurencia) {
		this.elszamolt_konkurencia = elszamolt_konkurencia;
	}

	public Integer getElszamolt_q2() {
		return elszamolt_q2;
	}

	public void setElszamolt_q2(Integer elszamolt_q2) {
		this.elszamolt_q2 = elszamolt_q2;
	}

	public Integer getElszamolt_q3() {
		return elszamolt_q3;
	}

	public void setElszamolt_q3(Integer elszamolt_q3) {
		this.elszamolt_q3 = elszamolt_q3;
	}

	public Integer getKeszpenz_targyev() {
		return keszpenz_targyev;
	}

	public void setKeszpenz_targyev(Integer keszpenz_targyev) {
		this.keszpenz_targyev = keszpenz_targyev;
	}

	public Integer getKeszpenz_elozo_ev() {
		return keszpenz_elozo_ev;
	}

	public void setKeszpenz_elozo_ev(Integer keszpenz_elozo_ev) {
		this.keszpenz_elozo_ev = keszpenz_elozo_ev;
	}

	public Integer getKeszpenz_konkurencia() {
		return keszpenz_konkurencia;
	}

	public void setKeszpenz_konkurencia(Integer keszpenz_konkurencia) {
		this.keszpenz_konkurencia = keszpenz_konkurencia;
	}

	public Integer getKeszpenz_q2() {
		return keszpenz_q2;
	}

	public void setKeszpenz_q2(Integer keszpenz_q2) {
		this.keszpenz_q2 = keszpenz_q2;
	}

	public Integer getKeszpenz_q3() {
		return keszpenz_q3;
	}

	public void setKeszpenz_q3(Integer keszpenz_q3) {
		this.keszpenz_q3 = keszpenz_q3;
	}

	public Integer getKoveteles_vevo_targyev() {
		return koveteles_vevo_targyev;
	}

	public void setKoveteles_vevo_targyev(Integer koveteles_vevo_targyev) {
		this.koveteles_vevo_targyev = koveteles_vevo_targyev;
	}

	public Integer getKoveteles_vevo_elozo_ev() {
		return koveteles_vevo_elozo_ev;
	}

	public void setKoveteles_vevo_elozo_ev(Integer koveteles_vevo_elozo_ev) {
		this.koveteles_vevo_elozo_ev = koveteles_vevo_elozo_ev;
	}

	public Integer getKoveteles_vevo_konkurencia() {
		return koveteles_vevo_konkurencia;
	}

	public void setKoveteles_vevo_konkurencia(Integer koveteles_vevo_konkurencia) {
		this.koveteles_vevo_konkurencia = koveteles_vevo_konkurencia;
	}

	public Integer getKoveteles_vevo_q2() {
		return koveteles_vevo_q2;
	}

	public void setKoveteles_vevo_q2(Integer koveteles_vevo_q2) {
		this.koveteles_vevo_q2 = koveteles_vevo_q2;
	}

	public Integer getKoveteles_vevo_q3() {
		return koveteles_vevo_q3;
	}

	public void setKoveteles_vevo_q3(Integer koveteles_vevo_q3) {
		this.koveteles_vevo_q3 = koveteles_vevo_q3;
	}

	public Integer getKeszlet_targyev() {
		return keszlet_targyev;
	}

	public void setKeszlet_targyev(Integer keszlet_targyev) {
		this.keszlet_targyev = keszlet_targyev;
	}

	public Integer getKeszlet_elozo_ev() {
		return keszlet_elozo_ev;
	}

	public void setKeszlet_elozo_ev(Integer keszlet_elozo_ev) {
		this.keszlet_elozo_ev = keszlet_elozo_ev;
	}

	public Integer getKeszlet_konkurencia() {
		return keszlet_konkurencia;
	}

	public void setKeszlet_konkurencia(Integer keszlet_konkurencia) {
		this.keszlet_konkurencia = keszlet_konkurencia;
	}

	public Integer getKeszlet_q2() {
		return keszlet_q2;
	}

	public void setKeszlet_q2(Integer keszlet_q2) {
		this.keszlet_q2 = keszlet_q2;
	}

	public Integer getKeszlet_q3() {
		return keszlet_q3;
	}

	public void setKeszlet_q3(Integer keszlet_q3) {
		this.keszlet_q3 = keszlet_q3;
	}

	public Integer getJovairas_targyev() {
		return jovairas_targyev;
	}

	public void setJovairas_targyev(Integer jovairas_targyev) {
		this.jovairas_targyev = jovairas_targyev;
	}

	public Integer getJovairas_elozo_ev() {
		return jovairas_elozo_ev;
	}

	public void setJovairas_elozo_ev(Integer jovairas_elozo_ev) {
		this.jovairas_elozo_ev = jovairas_elozo_ev;
	}

	public Integer getJovairas_konkurencia() {
		return jovairas_konkurencia;
	}

	public void setJovairas_konkurencia(Integer jovairas_konkurencia) {
		this.jovairas_konkurencia = jovairas_konkurencia;
	}

	public Integer getJovairas_q2() {
		return jovairas_q2;
	}

	public void setJovairas_q2(Integer jovairas_q2) {
		this.jovairas_q2 = jovairas_q2;
	}

	public Integer getJovairas_q3() {
		return jovairas_q3;
	}

	public void setJovairas_q3(Integer jovairas_q3) {
		this.jovairas_q3 = jovairas_q3;
	}

	public Integer getImm_targyev() {
		return imm_targyev;
	}

	public void setImm_targyev(Integer imm_targyev) {
		this.imm_targyev = imm_targyev;
	}

	public Integer getImm_elozo_ev() {
		return imm_elozo_ev;
	}

	public void setImm_elozo_ev(Integer imm_elozo_ev) {
		this.imm_elozo_ev = imm_elozo_ev;
	}

	public Integer getImm_konkurencia() {
		return imm_konkurencia;
	}

	public void setImm_konkurencia(Integer imm_konkurencia) {
		this.imm_konkurencia = imm_konkurencia;
	}

	public Integer getImm_q2() {
		return imm_q2;
	}

	public void setImm_q2(Integer imm_q2) {
		this.imm_q2 = imm_q2;
	}

	public Integer getImm_q3() {
		return imm_q3;
	}

	public void setImm_q3(Integer imm_q3) {
		this.imm_q3 = imm_q3;
	}

	public Integer getForgo_targyev() {
		return forgo_targyev;
	}

	public void setForgo_targyev(Integer forgo_targyev) {
		this.forgo_targyev = forgo_targyev;
	}

	public Integer getForgo_elozo_ev() {
		return forgo_elozo_ev;
	}

	public void setForgo_elozo_ev(Integer forgo_elozo_ev) {
		this.forgo_elozo_ev = forgo_elozo_ev;
	}

	public Integer getForgo_konkurencia() {
		return forgo_konkurencia;
	}

	public void setForgo_konkurencia(Integer forgo_konkurencia) {
		this.forgo_konkurencia = forgo_konkurencia;
	}

	public Integer getForgo_q2() {
		return forgo_q2;
	}

	public void setForgo_q2(Integer forgo_q2) {
		this.forgo_q2 = forgo_q2;
	}

	public Integer getForgo_q3() {
		return forgo_q3;
	}

	public void setForgo_q3(Integer forgo_q3) {
		this.forgo_q3 = forgo_q3;
	}

	public Integer getEszkoz_targyev() {
		return eszkoz_targyev;
	}

	public void setEszkoz_targyev(Integer eszkoz_targyev) {
		this.eszkoz_targyev = eszkoz_targyev;
	}

	public Integer getEszkoz_elozo_ev() {
		return eszkoz_elozo_ev;
	}

	public void setEszkoz_elozo_ev(Integer eszkoz_elozo_ev) {
		this.eszkoz_elozo_ev = eszkoz_elozo_ev;
	}

	public Integer getEszkoz_konkurencia() {
		return eszkoz_konkurencia;
	}

	public void setEszkoz_konkurencia(Integer eszkoz_konkurencia) {
		this.eszkoz_konkurencia = eszkoz_konkurencia;
	}

	public Integer getEszkoz_q2() {
		return eszkoz_q2;
	}

	public void setEszkoz_q2(Integer eszkoz_q2) {
		this.eszkoz_q2 = eszkoz_q2;
	}

	public Integer getEszkoz_q3() {
		return eszkoz_q3;
	}

	public void setEszkoz_q3(Integer eszkoz_q3) {
		this.eszkoz_q3 = eszkoz_q3;
	}

	public Integer getKot_szallito_targyev() {
		return kot_szallito_targyev;
	}

	public void setKot_szallito_targyev(Integer kot_szallito_targyev) {
		this.kot_szallito_targyev = kot_szallito_targyev;
	}

	public Integer getKot_szallito_elozo_ev() {
		return kot_szallito_elozo_ev;
	}

	public void setKot_szallito_elozo_ev(Integer kot_szallito_elozo_ev) {
		this.kot_szallito_elozo_ev = kot_szallito_elozo_ev;
	}

	public Integer getKot_szallito_konkurencia() {
		return kot_szallito_konkurencia;
	}

	public void setKot_szallito_konkurencia(Integer kot_szallito_konkurencia) {
		this.kot_szallito_konkurencia = kot_szallito_konkurencia;
	}

	public Integer getKot_szallito_q2() {
		return kot_szallito_q2;
	}

	public void setKot_szallito_q2(Integer kot_szallito_q2) {
		this.kot_szallito_q2 = kot_szallito_q2;
	}

	public Integer getKot_szallito_q3() {
		return kot_szallito_q3;
	}

	public void setKot_szallito_q3(Integer kot_szallito_q3) {
		this.kot_szallito_q3 = kot_szallito_q3;
	}

	public Integer getToke_targyev() {
		return toke_targyev;
	}

	public void setToke_targyev(Integer toke_targyev) {
		this.toke_targyev = toke_targyev;
	}

	public Integer getToke_elozo_ev() {
		return toke_elozo_ev;
	}

	public void setToke_elozo_ev(Integer toke_elozo_ev) {
		this.toke_elozo_ev = toke_elozo_ev;
	}

	public Integer getToke_konkurencia() {
		return toke_konkurencia;
	}

	public void setToke_konkurencia(Integer toke_konkurencia) {
		this.toke_konkurencia = toke_konkurencia;
	}

	public Integer getToke_q2() {
		return toke_q2;
	}

	public void setToke_q2(Integer toke_q2) {
		this.toke_q2 = toke_q2;
	}

	public Integer getToke_q3() {
		return toke_q3;
	}

	public void setToke_q3(Integer toke_q3) {
		this.toke_q3 = toke_q3;
	}

	public Integer getRovid_lejaratu_kot_targyev() {
		return rovid_lejaratu_kot_targyev;
	}

	public void setRovid_lejaratu_kot_targyev(Integer rovid_lejaratu_kot_targyev) {
		this.rovid_lejaratu_kot_targyev = rovid_lejaratu_kot_targyev;
	}

	public Integer getRovid_lejaratu_kot_elozo_ev() {
		return rovid_lejaratu_kot_elozo_ev;
	}

	public void setRovid_lejaratu_kot_elozo_ev(Integer rovid_lejaratu_kot_elozo_ev) {
		this.rovid_lejaratu_kot_elozo_ev = rovid_lejaratu_kot_elozo_ev;
	}

	public Integer getRovid_lejaratu_kot_konkurencia() {
		return rovid_lejaratu_kot_konkurencia;
	}

	public void setRovid_lejaratu_kot_konkurencia(Integer rovid_lejaratu_kot_konkurencia) {
		this.rovid_lejaratu_kot_konkurencia = rovid_lejaratu_kot_konkurencia;
	}

	public Integer getRovid_lejaratu_kot_q2() {
		return rovid_lejaratu_kot_q2;
	}

	public void setRovid_lejaratu_kot_q2(Integer rovid_lejaratu_kot_q2) {
		this.rovid_lejaratu_kot_q2 = rovid_lejaratu_kot_q2;
	}

	public Integer getRovid_lejaratu_kot_q3() {
		return rovid_lejaratu_kot_q3;
	}

	public void setRovid_lejaratu_kot_q3(Integer rovid_lejaratu_kot_q3) {
		this.rovid_lejaratu_kot_q3 = rovid_lejaratu_kot_q3;
	}

	public Integer getOssz_kot_targyev() {
		return ossz_kot_targyev;
	}

	public void setOssz_kot_targyev(Integer ossz_kot_targyev) {
		this.ossz_kot_targyev = ossz_kot_targyev;
	}

	public Integer getOssz_kot_elozo_ev() {
		return ossz_kot_elozo_ev;
	}

	public void setOssz_kot_elozo_ev(Integer ossz_kot_elozo_ev) {
		this.ossz_kot_elozo_ev = ossz_kot_elozo_ev;
	}

	public Integer getOssz_kot_konkurencia() {
		return ossz_kot_konkurencia;
	}

	public void setOssz_kot_konkurencia(Integer ossz_kot_konkurencia) {
		this.ossz_kot_konkurencia = ossz_kot_konkurencia;
	}

	public Integer getOssz_kot_q2() {
		return ossz_kot_q2;
	}

	public void setOssz_kot_q2(Integer ossz_kot_q2) {
		this.ossz_kot_q2 = ossz_kot_q2;
	}

	public Integer getOssz_kot_q3() {
		return ossz_kot_q3;
	}

	public void setOssz_kot_q3(Integer ossz_kot_q3) {
		this.ossz_kot_q3 = ossz_kot_q3;
	}

	public Integer getAlkalmazottak_szama_targyev() {
		return alkalmazottak_szama_targyev;
	}

	public void setAlkalmazottak_szama_targyev(Integer alkalmazottak_szama_targyev) {
		this.alkalmazottak_szama_targyev = alkalmazottak_szama_targyev;
	}

	public Integer getAlkalmazottak_szama_elozo_ev() {
		return alkalmazottak_szama_elozo_ev;
	}

	public void setAlkalmazottak_szama_elozo_ev(Integer alkalmazottak_szama_elozo_ev) {
		this.alkalmazottak_szama_elozo_ev = alkalmazottak_szama_elozo_ev;
	}

	public Integer getAlkalmazottak_szama_konkurencia() {
		return alkalmazottak_szama_konkurencia;
	}

	public void setAlkalmazottak_szama_konkurencia(Integer alkalmazottak_szama_konkurencia) {
		this.alkalmazottak_szama_konkurencia = alkalmazottak_szama_konkurencia;
	}

	public Integer getAlkalmazottak_szama_q2() {
		return alkalmazottak_szama_q2;
	}

	public void setAlkalmazottak_szama_q2(Integer alkalmazottak_szama_q2) {
		this.alkalmazottak_szama_q2 = alkalmazottak_szama_q2;
	}

	public Integer getAlkalmazottak_szama_q3() {
		return alkalmazottak_szama_q3;
	}

	public void setAlkalmazottak_szama_q3(Integer alkalmazottak_szama_q3) {
		this.alkalmazottak_szama_q3 = alkalmazottak_szama_q3;
	}

	public Integer getReszveny_arfolyam_targyev() {
		return reszveny_arfolyam_targyev;
	}

	public void setReszveny_arfolyam_targyev(Integer reszveny_arfolyam_targyev) {
		this.reszveny_arfolyam_targyev = reszveny_arfolyam_targyev;
	}

	public Integer getReszveny_arfolyam_elozo_ev() {
		return reszveny_arfolyam_elozo_ev;
	}

	public void setReszveny_arfolyam_elozo_ev(Integer reszveny_arfolyam_elozo_ev) {
		this.reszveny_arfolyam_elozo_ev = reszveny_arfolyam_elozo_ev;
	}

	public Integer getReszveny_arfolyam_konkurencia() {
		return reszveny_arfolyam_konkurencia;
	}

	public void setReszveny_arfolyam_konkurencia(Integer reszveny_arfolyam_konkurencia) {
		this.reszveny_arfolyam_konkurencia = reszveny_arfolyam_konkurencia;
	}

	public Integer getReszveny_arfolyam_q2() {
		return reszveny_arfolyam_q2;
	}

	public void setReszveny_arfolyam_q2(Integer reszveny_arfolyam_q2) {
		this.reszveny_arfolyam_q2 = reszveny_arfolyam_q2;
	}

	public Integer getReszveny_arfolyam_q3() {
		return reszveny_arfolyam_q3;
	}

	public void setReszveny_arfolyam_q3(Integer reszveny_arfolyam_q3) {
		this.reszveny_arfolyam_q3 = reszveny_arfolyam_q3;
	}

	public Integer getNaptar_targyev() {
		return naptar_targyev;
	}

	public void setNaptar_targyev(Integer naptar_targyev) {
		this.naptar_targyev = naptar_targyev;
	}

	public Integer getNaptar_elozo_ev() {
		return naptar_elozo_ev;
	}

	public void setNaptar_elozo_ev(Integer naptar_elozo_ev) {
		this.naptar_elozo_ev = naptar_elozo_ev;
	}

	public Integer getNaptar_konkurencia() {
		return naptar_konkurencia;
	}

	public void setNaptar_konkurencia(Integer naptar_konkurencia) {
		this.naptar_konkurencia = naptar_konkurencia;
	}

	public Integer getNaptar_q2() {
		return naptar_q2;
	}

	public void setNaptar_q2(Integer naptar_q2) {
		this.naptar_q2 = naptar_q2;
	}

	public Integer getNaptar_q3() {
		return naptar_q3;
	}

	public void setNaptar_q3(Integer naptar_q3) {
		this.naptar_q3 = naptar_q3;
	}

	public Integer getHozam_targyev() {
		return hozam_targyev;
	}

	public void setHozam_targyev(Integer hozam_targyev) {
		this.hozam_targyev = hozam_targyev;
	}

	public Integer getHozam_elozo_ev() {
		return hozam_elozo_ev;
	}

	public void setHozam_elozo_ev(Integer hozam_elozo_ev) {
		this.hozam_elozo_ev = hozam_elozo_ev;
	}

	public Integer getHozam_konkurencia() {
		return hozam_konkurencia;
	}

	public void setHozam_konkurencia(Integer hozam_konkurencia) {
		this.hozam_konkurencia = hozam_konkurencia;
	}

	public Integer getHozam_q2() {
		return hozam_q2;
	}

	public void setHozam_q2(Integer hozam_q2) {
		this.hozam_q2 = hozam_q2;
	}

	public Integer getHozam_q3() {
		return hozam_q3;
	}

	public void setHozam_q3(Integer hozam_q3) {
		this.hozam_q3 = hozam_q3;
	}


	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "RESZVENY_ID")
	@JsonBackReference
	private Reszveny penzAdatokReszveny;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}



	public Reszveny getPenzAdatokReszveny() {
		return penzAdatokReszveny;
	}

	public void setPenzAdatokReszveny(Reszveny penzAdatokReszveny) {
		this.penzAdatokReszveny = penzAdatokReszveny;
	}

}
