package springsbinvesting.domain.reszvenyelemzes;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import springsbinvesting.domain.Reszveny;
import springsbinvesting.domain.User;

@Entity
@Table(name = "mentalis")
public class MentalisElemzes {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	
	private String ellensegesseg;
	
	private String ellensegesseg_elv;
	
	private String felelem;
	
	private String felelem_elv;
	
	private String fokozott;
	
	private String fokozott_elv;
	
	private String indokolatlan;
	
	private String indokolatlan_elv;
	
	private String introvertalt;
	
	private String introvertalt_elv;
	
	private String kavargo;
	
	private String kavargo_elv;
	
	private String kellemetlen;
	
	private String kellemetlen_elv ;
	
	private String kimerultseg;
	private String kimerultseg_elv;
	
	private String onbizalom;
	
	private String onbizalom_elv;
	
	private String szomorusag;
	
	private String szomorusag_elv;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "RESZVENY_ID")
	@JsonBackReference
	private Reszveny mentalisReszveny;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEllensegesseg() {
		return ellensegesseg;
	}

	public void setEllensegesseg(String ellensegesseg) {
		this.ellensegesseg = ellensegesseg;
	}

	public String getEllensegesseg_elv() {
		return ellensegesseg_elv;
	}

	public void setEllensegesseg_elv(String ellensegesseg_elv) {
		this.ellensegesseg_elv = ellensegesseg_elv;
	}

	public String getFelelem() {
		return felelem;
	}

	public void setFelelem(String felelem) {
		this.felelem = felelem;
	}

	public String getFelelem_elv() {
		return felelem_elv;
	}

	public void setFelelem_elv(String felelem_elv) {
		this.felelem_elv = felelem_elv;
	}

	public String getFokozott() {
		return fokozott;
	}

	public void setFokozott(String fokozott) {
		this.fokozott = fokozott;
	}

	public String getFokozott_elv() {
		return fokozott_elv;
	}

	public void setFokozott_elv(String fokozott_elv) {
		this.fokozott_elv = fokozott_elv;
	}

	public String getIndokolatlan() {
		return indokolatlan;
	}

	public void setIndokolatlan(String indokolatlan) {
		this.indokolatlan = indokolatlan;
	}

	public String getIndokolatlan_elv() {
		return indokolatlan_elv;
	}

	public void setIndokolatlan_elv(String indokolatlan_elv) {
		this.indokolatlan_elv = indokolatlan_elv;
	}

	public String getIntrovertalt() {
		return introvertalt;
	}

	public void setIntrovertalt(String introvertalt) {
		this.introvertalt = introvertalt;
	}

	public String getIntrovertalt_elv() {
		return introvertalt_elv;
	}

	public void setIntrovertalt_elv(String introvertalt_elv) {
		this.introvertalt_elv = introvertalt_elv;
	}

	public String getKavargo() {
		return kavargo;
	}

	public void setKavargo(String kavargo) {
		this.kavargo = kavargo;
	}

	public String getKavargo_elv() {
		return kavargo_elv;
	}

	public void setKavargo_elv(String kavargo_elv) {
		this.kavargo_elv = kavargo_elv;
	}

	public String getKellemetlen() {
		return kellemetlen;
	}

	public void setKellemetlen(String kellemetlen) {
		this.kellemetlen = kellemetlen;
	}

	public String getKellemetlen_elv() {
		return kellemetlen_elv;
	}

	public void setKellemetlen_elv(String kellemetlen_elv) {
		this.kellemetlen_elv = kellemetlen_elv;
	}

	public String getKimerultseg() {
		return kimerultseg;
	}

	public void setKimerultseg(String kimerultseg) {
		this.kimerultseg = kimerultseg;
	}

	public String getKimerultseg_elv() {
		return kimerultseg_elv;
	}

	public void setKimerultseg_elv(String kimerultseg_elv) {
		this.kimerultseg_elv = kimerultseg_elv;
	}

	public String getOnbizalom() {
		return onbizalom;
	}

	public void setOnbizalom(String onbizalom) {
		this.onbizalom = onbizalom;
	}

	public String getOnbizalom_elv() {
		return onbizalom_elv;
	}

	public void setOnbizalom_elv(String onbizalom_elv) {
		this.onbizalom_elv = onbizalom_elv;
	}

	public String getSzomorusag() {
		return szomorusag;
	}

	public void setSzomorusag(String szomorusag) {
		this.szomorusag = szomorusag;
	}

	public String getSzomorusag_elv() {
		return szomorusag_elv;
	}

	public void setSzomorusag_elv(String szomorusag_elv) {
		this.szomorusag_elv = szomorusag_elv;
	}

	public Reszveny getMentalisReszveny() {
		return mentalisReszveny;
	}

	public void setMentalisReszveny(Reszveny mentalisReszveny) {
		this.mentalisReszveny = mentalisReszveny;
	}

	
	
}