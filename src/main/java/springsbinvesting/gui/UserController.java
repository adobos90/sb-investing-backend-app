package springsbinvesting.gui;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.context.annotation.RequestScope;
import springsbinvesting.domain.Reszveny;
import springsbinvesting.domain.User;
import springsbinvesting.domain.celkituzes.Akadaly;
import springsbinvesting.domain.celkituzes.Celkituzes;
import springsbinvesting.domain.celkituzes.Feladat;
import springsbinvesting.domain.penzugy.*;
import springsbinvesting.domain.reszvenyelemzes.Celar;
import springsbinvesting.domain.reszvenyelemzes.Manageles;
import springsbinvesting.domain.reszvenyelemzes.MentalisElemzes;
import springsbinvesting.domain.reszvenyelemzes.NettoJelenErtek;
import springsbinvesting.domain.reszvenyelemzes.PenzugyiAdatok;
import springsbinvesting.domain.reszvenyelemzes.VallalatKockElemzes;
import springsbinvesting.domain.reszvenyelemzes.VallalatPenzugyiElemzes;
import springsbinvesting.domain.reszvenyelemzes.manageles.ManagelesBefAdatok;
import springsbinvesting.domain.reszvenyelemzes.manageles.ManagelesReszveny;
import springsbinvesting.domain.reszvenyelemzes.manageles.ManagelesiStrategia;
import springsbinvesting.domain.reszvenyelemzes.manageles.MenBefMenegeles;
import springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.PenzugyiElemzesHatekonysag;
import springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.PenzugyiElemzesHitel;
import springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.PenzugyiElemzesJovedelem;
import springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.PenzugyiElemzesPiaci;
import springsbinvesting.domain.strategia.Strategia;
import springsbinvesting.domain.strategia.StrategiaReszveny;
import springsbinvesting.model.services.impl.UserServiceImpl;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserServiceImpl userService;

	@GetMapping("")
	public List<User> findAll() {
		return userService.findAll();
	}

	@GetMapping(value = "", params = {"id"})
	public User findUserById(@RequestParam Long id) {
		return userService.findUserById(id);
	}

	@GetMapping(value = "", params = {"email"})
	public User findUserByEmail(@RequestParam String email) {
		return userService.findUserByEmail(email);
	}

	// Adatok
	@PostMapping("/addUser")
	public void saveUser(@RequestBody User user) {
		userService.saveUser(user);
	}

	@PutMapping(value = "/updateUser", params = {"userId"})
	public User updateUser(@RequestParam Long userId, @RequestBody User user){
		return userService.updateUser(userId, user);
	}

}
