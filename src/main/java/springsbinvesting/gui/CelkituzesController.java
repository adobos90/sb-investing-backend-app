package springsbinvesting.gui;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springsbinvesting.domain.celkituzes.Akadaly;
import springsbinvesting.domain.celkituzes.Celkituzes;
import springsbinvesting.domain.celkituzes.Feladat;
import springsbinvesting.model.services.impl.UserServiceImpl;

import java.util.List;

@RestController
@RequestMapping("/users/celkituzes")
public class CelkituzesController {

    @Autowired
    private UserServiceImpl userService;

    @PostMapping(value ="/addcelkituzes", params = {"userId"})
    public void addCelkituzes(@RequestParam Long userId, @RequestBody Celkituzes celkituzes) {
        userService.addCelkituzes(userId, celkituzes);

    }

    @PostMapping(value ="/addfeladat", params = {"celkituzesId"})
    public void addFeladToCelkituzes(@RequestParam Long celkituzesId, @RequestBody Feladat feladat) {
        userService.addFeladatToCelkituzes(celkituzesId, feladat);
    }

    @PostMapping(value ="/addakadaly", params = {"celkituzesId"})
    public void addAkadalyToCelkituzes(@RequestParam Long celkituzesId, @RequestBody Akadaly akadaly) {
        userService.addAkadalyToCelkituzes(celkituzesId, akadaly);
    }

    @PutMapping(value ="/updatecelkituzes", params = {"userId"})
    public void updateCelkituzes(@RequestParam Long userId, @RequestBody Celkituzes celkituzes) {
        userService.updateCelkituzes(userId, celkituzes);
    }

    @PutMapping(value ="/updatefeladat", params = {"celkituzesId"})
    public void updateFeladat(@RequestParam Long celkituzesId, @RequestBody Feladat feladat) {
        userService.updateFeladat(celkituzesId, feladat);
    }

    @GetMapping(value ="/selected")
    public Long getCelkituzesId(@RequestParam Long userId) {

        return userService.getCelkituzesId(userId);
    }

    @GetMapping(value ="/celkituzesek")
    public Celkituzes getCelkituzes(@RequestParam Long userId) {

        return userService.getCelkituzes(userId);
    }

    @DeleteMapping(value ="delete", params = {"userId"})
    public ResponseEntity<Long> deleteCelkituzes(@RequestParam Long userId) {
        userService.deleteCelkituzes(userId);

        return new ResponseEntity<Long>(userId, HttpStatus.OK);
    }

    @GetMapping(value ="/feladatok")
    public List<Feladat> getFeladatok(@RequestParam Long celkituzesId) {
        return userService.getFeladatok(celkituzesId);
    }

    @DeleteMapping(value ="/feladatok/delete", params = {"feladatId"})
    public ResponseEntity<Long> deleteFeladat(@RequestParam Long feladatId) {
        int numberOfDeleted = userService.deleteFeladat(feladatId);

        return numberOfDeleted > 0 ? new ResponseEntity<>(feladatId, HttpStatus.OK)
                : new ResponseEntity<>(feladatId, HttpStatus.NOT_FOUND);

    }
}
