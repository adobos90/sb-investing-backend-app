package springsbinvesting.gui;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import springsbinvesting.domain.Reszveny;
import springsbinvesting.domain.User;
import springsbinvesting.domain.reszvenyelemzes.*;
import springsbinvesting.domain.reszvenyelemzes.manageles.ManagelesBefAdatok;
import springsbinvesting.domain.reszvenyelemzes.manageles.ManagelesReszveny;
import springsbinvesting.domain.reszvenyelemzes.manageles.ManagelesiStrategia;
import springsbinvesting.domain.reszvenyelemzes.manageles.MenBefMenegeles;
import springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.PenzugyiElemzesHatekonysag;
import springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.PenzugyiElemzesHitel;
import springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.PenzugyiElemzesJovedelem;
import springsbinvesting.domain.reszvenyelemzes.penzugyielemzes.PenzugyiElemzesPiaci;
import springsbinvesting.model.services.impl.UserServiceImpl;

import java.util.List;

@RestController
@RequestMapping("/users")
public class ReszvenyekController {

    @Autowired
    private UserServiceImpl userService;


    // ******* GET REQUESTS *********


    @GetMapping(value = "/reszvenyek")
    public List<Reszveny> getReszvenyek(@RequestParam("userId") Long userId) {
        return userService.getReszvenyek((userId));
    }

    @GetMapping(value = "/reszvenyek/{reszvenyId}")
    public Reszveny getReszvenyById(@PathVariable  Long reszvenyId) {
        return userService.getReszvenyById(reszvenyId);
    }

    @GetMapping(value = "/reszvenyek/mentalis", params = "reszvenyId")
    public MentalisElemzes getMentalisByReszvenyId(@RequestParam("reszvenyId") Long reszvenyId) {
            return userService.getMentalisByReszvenyId(reszvenyId);
    }

    @GetMapping(value = "/reszvenyek/kockaztelemzes")
    public VallalatKockElemzes getVallKockElemzes(@RequestParam("reszvenyId") Long reszvenyId){
        return userService.getVallKockElemzes(reszvenyId);

    }

    @GetMapping(value = "/reszvenyek/penzugyiadatok")
    public PenzugyiAdatok getPenzugyiAdatok(@RequestParam("reszvenyId") Long reszvenyId){
        return userService.getPenzugyiAdatok(reszvenyId);

    }

    @GetMapping(value = "/reszvenyek/penzugyielemzes")
    public VallalatPenzugyiElemzes getVallPenzElemzes(@RequestParam("reszvenyId") Long reszvenyId){
        return userService.getVallPenzElemzes(reszvenyId);

    }

    @GetMapping(value = "/reszvenyek/celar")
    public Celar getCelar(@RequestParam("reszvenyId") Long reszvenyId){
        return userService.getCelar(reszvenyId);

    }

    @GetMapping(value = "/reszvenyek/nettojelenertek")
    public NettoJelenErtek getNettoJelenertek(@RequestParam("reszvenyId") Long reszvenyId){
        return userService.getNettoJelenertek(reszvenyId);

    }

    @GetMapping(value = "/reszvenyek/manageles")
    public Manageles getManageles(@RequestParam("reszvenyId") Long reszvenyId){
        return userService.getManageles(reszvenyId);
    }

    // ********* DELETE REQUESTS ******


    @DeleteMapping(value = "/reszvenyek", params = "id")
    public void deleteReszveny(@RequestParam("id") Long id) {
         userService.deleteReszveny(id);
    }




        //********  POST REQUESTS  ************

    @PostMapping(value = "/addReszveny", params = {"userId"})
    public Reszveny addReszveny(@RequestParam Long userId, @RequestBody Reszveny reszveny) {
        return userService.addReszveny(userId, reszveny);
    }

    // Mentalis elemzes

  @PostMapping(value = "/reszveny/mentalis", params={"reszvenyId"})
    public void addMentalisElemzes(@RequestParam Long reszvenyId, @RequestBody
            MentalisElemzes mentalis) {
        userService.addMentalis(reszvenyId, mentalis);
    }

    // Váll. kock. elemzés

    @PostMapping(value = "/reszveny/kockazatelemzes", params = {"reszvenyId"})
    public void addVallKockElemzes(@RequestParam Long reszvenyId, @RequestBody VallalatKockElemzes vallalatKockElemzes) {
        userService.addVallKockElemzes(reszvenyId, vallalatKockElemzes);
    }

    // Pénzügyi adatok

    @PostMapping(value = "/reszveny/penzugyiadatok", params = {"reszvenyId"})
    public void addPenzugyiAdatok(@RequestParam Long reszvenyId, @RequestBody PenzugyiAdatok penzugyiAdatok) {
        userService.addPenzugyiAdatok(reszvenyId, penzugyiAdatok);
    }

    // PenzugyiElemzes
    @PostMapping(value = "/addPenzugyiElemzes", params = "reszvenyId")
    public void addPenzugyiElemzes(@RequestParam Long reszvenyId, @RequestBody
            VallalatPenzugyiElemzes penzugyiElemzes) {
        userService.addPenzugyiElemzes(reszvenyId, penzugyiElemzes);
    }

    @PostMapping(value = "/addJovPenzElemzes", params = "penzelemzesId")
    public void addJovToPenzugyiElemzes(@RequestParam Long penzelemzesId, @RequestBody
            PenzugyiElemzesJovedelem jovPenzugyElemzes) {
        userService.addJovToPenzugyiElemzes(penzelemzesId, jovPenzugyElemzes);
    }

    @PostMapping(value = "/addHatPenzElemzes", params = "penzelemzesId")
    public void addHatToPenzugyiElemzes(@RequestParam Long penzelemzesId, @RequestBody
            PenzugyiElemzesHatekonysag hatPenzugyElemzes) {
        userService.addHatToPenzugyiElemzes(penzelemzesId, hatPenzugyElemzes);
    }

    @PostMapping(value = "/addHitelPenzElemzes", params = "penzelemzesId")
    public void addHitelToPenzugyiElemzes(@RequestParam Long penzelemzesId, @RequestBody
            PenzugyiElemzesHitel hitelPenzugyElemzes) {
        userService.addHitelToPenzugyiElemzes(penzelemzesId, hitelPenzugyElemzes);
    }

    @PostMapping(value = "/addPiaciPenzElemzes", params = "penzelemzesId")
    public void addPiaciToPenzugyiElemzes(@RequestParam Long penzelemzesId, @RequestBody
            PenzugyiElemzesPiaci piaciPenzugyElemzes) {
        userService.addPiaciToPenzugyiElemzes(penzelemzesId, piaciPenzugyElemzes);
    }

    // Célár

    @PostMapping(value = "/reszveny/celar", params = {"reszvenyId"})
    public void addCelar(@RequestParam Long reszvenyId, @RequestBody Celar celar) {
        userService.addCelar(reszvenyId, celar);
    }

    // Netto Jelenérték

    @PostMapping(value = "/reszveny/nettojelenertek", params = {"reszvenyId"})
    public void addNettoJelenertek(@RequestParam Long reszvenyId, @RequestBody NettoJelenErtek netto) {
        userService.addNettoJelenErtek(reszvenyId, netto);
    }

    // Manageles

    @PostMapping(value = "/addManageles", params = {"reszvenyId"})
    public void addManageles(@RequestParam Long reszvenyId, @RequestBody Manageles manageles) {
        userService.addManageles(reszvenyId, manageles);
    }

    @PostMapping(value = "/reszveny/manageles/adatok", params = {"managelesId"})
    public void addBefAdatokToManageles(@RequestParam Long managelesId, @RequestBody
            ManagelesBefAdatok menBefadatok) {
        userService.addBefAdatokToManageles(managelesId, menBefadatok);

    }

    @PostMapping(value = "/reszveny/manageles/reszveny", params = {"managelesId"})
    public void addReszvenyToManageles(@RequestParam Long managelesId, @RequestBody
            ManagelesReszveny menReszveny) {
        userService.addReszvenyToManageles(managelesId, menReszveny);


    }

    @PostMapping(value = "/reszveny/manageles/strategia", params = {"managelesId"})
    public void addStrategiaToManageles(@RequestParam Long managelesId, @RequestBody
            ManagelesiStrategia menStrat) {
         userService.addStrategiaToManageles(managelesId, menStrat);

    }

    @PostMapping(value = "/reszveny/manageles/befmen", params = {"managelesId"})
    public void addMenBefMenToManageles(@RequestParam Long managelesId, @RequestBody
            MenBefMenegeles menBefMen) {
        userService.addMenBefMenToManageles(managelesId,
                menBefMen);


    }


    // ********* PUT REQUESTS ********
    @PutMapping(value = "/updateReszveny", params = {"reszvenyId"})
    public Reszveny updateReszveny(@RequestParam Long reszvenyId, @RequestBody  Reszveny newReszveny){
        return userService.updateReszveny(reszvenyId, newReszveny);
    }

    @PutMapping(value = "/updateMentalis", params = {"reszvenyId"})
    public MentalisElemzes updateMentalisElemzes(@RequestParam Long reszvenyId, @RequestBody MentalisElemzes mentalisElemzes) {
        return userService.updateMentalisElemzes(reszvenyId, mentalisElemzes);
    }

    @PutMapping(value = "/updateKockazatElemzes", params = {"reszvenyId"})
    public VallalatKockElemzes updateVallKockElemzes(@RequestParam Long reszvenyId,  @RequestBody VallalatKockElemzes kockElemzes){
        return userService.updateVallKockElemzes(reszvenyId, kockElemzes);
    }

    @PutMapping(value = "/updatePenzugyiAdatok", params = {"reszvenyId"})
    public PenzugyiAdatok updatePenzugyiAdatok(@RequestParam Long reszvenyId, @RequestBody PenzugyiAdatok penzugyiAdatok){
        return userService.updatePenzugyiAdatok(reszvenyId, penzugyiAdatok);
    }

    /*public VallalatPenzugyiElemzes updatePenzugyielemzes(@RequestParam Long reszvenyId, @RequestBody VallalatPenzugyiElemzes vallalatPenzugyiElemzes){
    }*/

    @PutMapping(value = "/updateJovPenzugyiElemzes", params = {"penzelemzesId"})
    public void updateJovToPenzugyiElemzes(@RequestParam Long penzelemzesId, @RequestBody PenzugyiElemzesJovedelem jovPenzugyElemzes){
        userService.updateJovToPenzugyiElemzes(penzelemzesId, jovPenzugyElemzes);
    }

    @PutMapping(value = "/updateHitelPenzugyiElemzes", params = {"penzelemzesId"})
    public void updateHitelToPenzugyiElemzes(@RequestParam Long penzelemzesId, @RequestBody PenzugyiElemzesHitel hitelPenzugyElemzes){
        userService.updateHitelToPenzugyiElemzes(penzelemzesId, hitelPenzugyElemzes);
    }

    @PutMapping(value = "/updateHatPenzugyiElemzes", params = {"penzelemzesId"})
    public void updateHatToPenzugyiElemzes(@RequestParam Long penzelemzesId, @RequestBody PenzugyiElemzesHatekonysag hatPenzugyElemzes){
        userService.updateHatToPenzugyiElemzes(penzelemzesId, hatPenzugyElemzes);
    }

    @PutMapping(value = "/updatePiaciPenzugyiElemzes", params = {"penzelemzesId"})
    public void updatePiaciToPenzugyiElemzes(@RequestParam Long penzelemzesId, @RequestBody PenzugyiElemzesPiaci piaciPenzugyElemzes){
        userService.updatePiaciToPenzugyiElemzes(penzelemzesId, piaciPenzugyElemzes);
    }

    @PutMapping(value = "/updateCelar", params = {"reszvenyId"})
    public Celar updateCelar(@RequestParam Long reszvenyId, @RequestBody Celar celar){
        return userService.updateCelar(reszvenyId, celar);
    }

    @PutMapping(value = "/updateNettoJelenertek", params = {"reszvenyId"})
    public NettoJelenErtek updateNettoJelenertek(@RequestParam Long reszvenyId, @RequestBody NettoJelenErtek nettoJelenErtek){
        return userService.updateNettoJelenertek(reszvenyId, nettoJelenErtek);
    }

    @PutMapping(value = "/updateManagelesAdatok", params = {"managelesId"})
    public ManagelesBefAdatok updateManagelesAdatok(@RequestParam Long managelesId, @RequestBody ManagelesBefAdatok managelesBefAdatok){
        return userService.updateManagelesAdatok(managelesId, managelesBefAdatok);

    }

    @PutMapping(value = "/updateManagelesReszveny", params = {"managelesId"})
    public ManagelesReszveny updateManagelesReszveny(@RequestParam Long managelesId,@RequestBody  ManagelesReszveny managelesReszveny){
        return userService.updateManagelesReszveny(managelesId, managelesReszveny);

    }

    @PutMapping(value = "/updateManagelesStrat", params = {"managelesId"})
    public ManagelesiStrategia updateManagelesStrat( @RequestParam Long managelesId, @RequestBody ManagelesiStrategia managelesiStrategia){
        return userService.updateManagelesStrat(managelesId, managelesiStrategia);
    }

    @PutMapping(value = "/updateManagelesBef", params = {"managelesId", "id"})
    public MenBefMenegeles updateBefMenManageles(@RequestParam Long managelesId, @RequestParam Long id, @RequestBody MenBefMenegeles menBefMenegeles){
       return  userService.updateBefMenManageles(managelesId, id,menBefMenegeles);
    }

    @DeleteMapping(value = "/manageles/delete", params = {"id", "managelesId"})
    public void deleteMenBefMen(@RequestParam Long id, @RequestParam Long managelesId){
        userService.deleteMenBefMen(id, managelesId);
    }

}
