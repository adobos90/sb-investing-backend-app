package springsbinvesting.gui;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springsbinvesting.domain.strategia.Strategia;
import springsbinvesting.domain.strategia.StrategiaReszveny;
import springsbinvesting.model.services.impl.UserServiceImpl;

import java.util.List;

@RestController
@RequestMapping("/users/strategia")
public class StrategiaController {

    @Autowired
    private UserServiceImpl userService;

    @PostMapping(value = "/addStrategia" , params = "userId")
    public void addStrategiaItem(@RequestParam Long userId, @RequestBody Strategia strategia){
        this.userService.addStrategiaItem(userId, strategia);
    }

    @GetMapping(value = "/strategiaByFlag")
    public List<Strategia> findAllStrategiaItemByFlag(@RequestParam Long userId, @RequestParam String flag) {
        return userService.findAllStrategiaItemByFlag(userId, flag);
    }

    @PostMapping(value = "/addStrategiaReszveny", params = "userId")
    public void addStrategiaReszveny(@RequestParam Long userId, @RequestBody StrategiaReszveny strategiaReszveny) {
        userService.addStrategiaReszveny(userId, strategiaReszveny);
    }

    @GetMapping(value = "/strategiaReszvenyek")
    public List<StrategiaReszveny> getAllStratReszveny(@RequestParam Long userId) {
        return userService.getAllStratReszveny(userId);
    }

}
