package springsbinvesting.gui;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springsbinvesting.domain.penzugy.*;
import springsbinvesting.model.services.impl.UserServiceImpl;

import java.util.List;

@RestController
@RequestMapping("/users/penzugyek")
public class PenzugyekController {

    @Autowired
    private UserServiceImpl userService;


    @GetMapping(value = "")
    public List<Penzugy> getPenzugyek(@RequestParam String userId) {
        return userService.findAllPenzugy(userId);
    }

    @PostMapping(value = "/addPenzugy", params = "{userId}")
    public Long addPenzugy(@RequestParam Long userId,@RequestBody Penzugy penzugy) {
        return  userService.addPenzugy(userId,penzugy);
    }

    @PostMapping(value = "/addBevetelekToHonap", params = "{honapId}")
    public void addBevetelekToHonap(@RequestParam Long honapId, @RequestBody BevetelPenzugy bevKiadasPenzugy) {
        userService.addBevetelekToHonap(honapId, bevKiadasPenzugy);
    }

    @PostMapping(value = "/addKiadasokToHonap", params = "{honapId}")
    public void addKiadasokToHonap(@RequestParam Long honapId,@RequestBody KiadasPenzugy kiadasPenzugy) {
        userService.addKiadasokToHonap(honapId, kiadasPenzugy);
    }

    // id keresése év alapján
    @GetMapping(value = "/penzugyByEv")
    public Long findPenzugyIdByEv(@RequestParam String ev) {
        return userService.findPenzugyIdByEv(ev);
    }

    // bevkiad keresese penzugyid + honap
	@GetMapping(value = "/bevKiadByIdandHonap")
	public Long findBevKiadIbByPenzugyId(@RequestParam Long penzugyId, @RequestParam String honap) {
		return userService.findBevKiadIbByPenzugyId(penzugyId, honap);
	}

    @DeleteMapping(value = "/deleteBevetel", params = "{ honapId}")
    public void deleteBevetelInPenzugy( @RequestParam Long honapId) {
        userService.deleteBevetelInPenzugy(honapId);
    }

    @DeleteMapping(value = "/deleteKiadas", params = "{ honapId,nap}")
    public void deleteKiadasPenzugy( @RequestParam Long honapId, @RequestParam int nap) {
        userService.deleteKiadasInPenzugy(honapId, nap);
    }

    @PutMapping(value = "/updateBevetel", params = "{honapId}/{bevetel}")
    public void updateBevetelInPenzugy(@RequestParam Long honapId, @RequestParam Long bevetel) {
        userService.updateBevetelInPenzugy(honapId, bevetel);
    }

    @PostMapping(value = "/addHonap", params = "{penzugyId}")
    public void addHonap(@RequestParam Long penzugyId, @RequestBody Honap honap){
        userService.addHonap(penzugyId, honap);
    }

    @GetMapping(value = "/honapByPenzugyiEv")
    public List<Honap> findHonapByPenzugyiEv(@RequestParam String penzugyiEv) {
        return userService.findHonapByPenzugyiEv(penzugyiEv);
    }

    // HOZZAADVA
    @GetMapping(value = "/bevetelekByHonapId")
    public BevetelPenzugy findBevetelekByHonapId(@RequestParam String year, @RequestParam String ho){
        return userService.findBevetelekByHonapId(year, ho);
    }

    @GetMapping(value = "/kiadasokByYearsAndMonths")
    public List<KiadasPenzugy> findKiadasokByYearsAndMonths(@RequestParam String year, @RequestParam String ho){
        return userService.findKiadasokByYearsAndMonths(year, ho);
    }


    @GetMapping(value = "/kiadasokByHonapIdAndNap")
    public KiadasPenzugy findKiadasokByHonapIdandNap(@RequestParam Long honapId, @RequestParam int nap){
        return userService.findKiadasokByHonapIdandNap(honapId, nap);
    }

    // HOZZÁADVA
    @GetMapping(value = "/osszBevetel")
    public List<BevetelPenzugy> getOsszesBevetel(){
        return userService.getOsszesBevetel();
    }

    // HOZZAADVA
    @GetMapping(value = "/osszKiadas")
    public List<KiadasPenzugy> getOsszesKiadas(){
        return userService.getOsszesKiadas();
    }

    @PutMapping(value = "/updateHonapZaras", params = "{yearId, honap, zarva}")
    public void updateHonapZarasa(@RequestParam Long yearId, @RequestParam int honap, @RequestParam int zarva) {
        userService.updateHonapZarasa(yearId, honap, zarva);
    }

    @PostMapping(value = "/addCelToPenzugy", params = "{userId}")
    public void addCelToPenzugy(@RequestParam Long userId, @RequestBody CelokPenzugy celokPenzugy) {
        userService.addCelToPenzugy(userId, celokPenzugy);
    }

    @GetMapping(value = "/celokPenzugyById")
    public CelokPenzugy findCelokPenzugyById(@RequestParam Long userId) {
        return userService.findCelokPenzugyById(userId);
    }

    @DeleteMapping(value = "/deleteCelokPenzugy", params = "{userId}")
    public void deleteCelokPenzugy(@RequestParam Long userId) {
        this.userService.deleteCelokPenzugy(userId);
    }



}
