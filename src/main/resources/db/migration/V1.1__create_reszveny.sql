create table reszveny(
	id int primary key auto_increment not null,
	vallalat_neve varchar(100) not null,
	agazat varchar(100) not null,
	datum date not null,
	status varchar(100) not null,
	strategia varchar(100) not null,
	ticker varchar(100) not null,
	user_id int,
	foreign key(user_id) references user(id)
);